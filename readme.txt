3DLOOK API Template project.

This template consists of three main parts:
Input params (gender and height)
Photo state (two steps - front and side photo)
Result state (if response has been received, app shows 3d model link and saves it locally)

Basic API script is APICoordinator, where all credentials initialized and API-interaction flow is implemented.

Credential block is   
 // API 3DLOOK PARAMS
      private string apiv2Url = "https://saia.3dlook.me/api/v2/persons/";
      private string apiv2Key = "YOUR API KEY";    

InfoInputState and PhotoMakingLogic - basic logic for first two major states. There, actually, should run "from a box", so in basic case you need to set credentials and handle received mesh only.

main steps in API are:
- sending metadata (gender|height|APIKey), receiving url for photo upload
- sending photos, receiving url of processing
- checking processing url, wait for success or unsuccess answer
- when success, writing results json

For debug purposes, in Unity Editor run mode, some test photos used, so flow able to be checked in editor mode.

This project is 2017.1.1.f1 unity version - of course, basic logic will run in any version.
Scenes in Unity project are here : Assets/basic_logic/Scenes/
Sripts in Unity project are here : Assets/basic_logic/scripts/

 