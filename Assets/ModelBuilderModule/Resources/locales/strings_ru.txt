﻿{
"PHOTO_STATE_HINT_UP_1" : "FRONT VIEW: 1/3",
"PHOTO_STATE_HINT_UP_2" : "PROFILE VIEW: 2/3",
"PHOTO_STATE_HINT_UP_3" : "TAKE A SELFIE: 3/3",
"PHOTO_STATE_HINT_UP_1_1RAW" : "Keep your silhouette inside the frame. If you are using a mirror, hold one arm along the body.",

"PHOTO_STATE_HINT_UP_2_1RAW" : "Keep your silhouette inside the frame. If you are using a mirror, hold one arm along the body.",

"PHOTO_STATE_HINT_UP_3_1RAW" : "Please remove the hair and glasses from your face.",


"PHOTO_STATE_FRONT_HINT" : "FRONT VIEW: 1/3",
"PHOTO_STATE_PROFILE_HINT" : "PROFILE VIEW: 2/3",
"PHOTO_STATE_SELFIE_HINT" : "TAKE A SELFIE: 3/3",
"PHOTO_STATE_SAVING" : "Saving data",
"PHOTO_STATE_CUSTOM_MENU_BTN_TEXT" : "Manual creation",
"PHOTO_STATE_CUSTOM_INPUT_TITLE" : "Problem with photo",
"PHOTO_STATE_CUSTOM_INPUT_MAIN_DESCRIPTION" : "For some reasons we can't process the pictures. No worries, we will handle it.",
"PHOTO_STATE_CUSTOM_INPUT_CM_MEASURE" : "CM",
"PHOTO_STATE_CUSTOM_INPUT_IN_MEASURE" : "IN",
"PHOTO_STATE_CUSTOM_INPUT_BREAST" : "BREAST",
"PHOTO_STATE_CUSTOM_INPUT_VOLUME_WAIST" : "WAIST",
"PHOTO_STATE_CUSTOM_INPUT_VOLUME_PELVIS" : "HIPS",
"PHOTO_STATE_CUSTOM_INPUT_CONTINUE_BTN_TEXT" : "Continue",
"PHOTO_STATE_CUSTOM_INPUT_EMPTY_FIELD_TITLE" : "Don't leave it empty",
"PHOTO_STATE_CUSTOM_INPUT_EMPTY_FIELD_TEXT" : "The field (template) is empty",
"PHOTO_STATE_CUSTOM_INPUT_WRONG_FIELD_TITLE" : "Enter valid data",
"PHOTO_STATE_CUSTOM_INPUT_WRONG_FIELD_BREAST_TEXT" : "Input data is not valid. We expect your cheast volume between {0} and {1} ",
"PHOTO_STATE_CUSTOM_INPUT_WRONG_FIELD_PELVIST_TEXT" : "Input data is not valid. We expect your hips volume between {0} and {1} ",
"PHOTO_STATE_CUSTOM_INPUT_WRONG_FIELD_WAIST_TEXT" : "Input data is not valid. We expect your waist volume between {0} and {1} ",

"NOT_CONNECTION_DIALOG_WINDOW_TITLE" : "NO CONNECTION",
"NOT_CONNECTION_DIALOG_WINDOW_TEXT" : "Stable connection is required for a model creation.",

"INCORRECT_PHOTO_DIALOG_WINDOW_TITLE" : "INCORRECT PHOTO",
"INCORRECT_PHOTO_DIALOG_WINDOW_TEXT" : "A person should be inside borders on screen.",

"SOCIAL_ETWORK_LOGIN_ERROR_TITLE" : "Attention",
"SOCIAL_ETWORK_LOGIN_ERROR_TEXT" : "Unable to login via Facebook",

"AUTHORIZATION_STATE_CONDITIONS_CHECKBOX_NOT_SWITCHED_TITLE" : "Attention",
"AUTHORIZATION_STATE_CONDITIONS_CHECKBOX_NOT_SWITCHED_TEXT" : "You have to accept Terms & Conditions in order to continue.",

"NOT_HANDLED_ERROR_MSG_TITLE" : "Attention",
"NOT_HANDLED_ERROR_MSG_TEXT" : "Unexpected error. Please try again.",

"UPLOAD_ERROR_MSG_TITLE" : "Attention",
"UPLOAD_ERROR_MSG_TEXT" : "Server connection error. Please try again",

"EMAIL_FORMAT_ERROR_TITLE" : "Attention",
"EMAIL_FORMAT_ERROR_TEXT" : "Incorrect email format",

"USER_FIELD_EMPTY_ERROR_TITLE" : "Attention",
"USER_FIELD_EMPTY_ERROR_TEXT" : "Name is required",

"USER_FIELD_EMPTY_PASSWORD_TITLE" : "Attention",
"USER_FIELD_EMPTY_PASSWORD_TEXT" : "Password is required",

"WRONG_USERNAME_OR_PASSWORD_ERROR_TITLE" : "Attention",
"WRONG_USERNAME_OR_PASSWORD_ERROR_TEXT" : "Input valid Email or Password",

"CREATE_ACCOUNT_STATE_PASSWORDS_NOT_MATCH_TITLE" : "Attention",
"CREATE_ACCOUNT_STATE_PASSWORDS_NOT_MATCH_TEXT" : "The passwords don't match",


"LOW_INTERNET_CONNECTION_DIALOG_TITLE" : "Low speed of internet connection",
"LOW_INTERNET_CONNECTION_DIALOG_TEXT" : "Do you want to try again ?",


"CUSTOM_PARAMS_INPUT_MENU_SUGGESTION_TITLE" : "Problem with photo",
"CUSTOM_PARAMS_INPUT_MENU_SUGGESTION_TEXT" : "For some reasons we can't process the pictures. No worries, we will handle it",

"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FREE_DIALOG_TITLE" : "Attention",
"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FREE_DIALOG_TEXT" : "When you skip the training, it affects your final results.",

"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FULL_DIALOG_TITLE" : "Attention",
"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FULL_DIALOG_TEXT" : "When you skip the training, it affects your final results.",

"AUTHORIZATION_STATE_EMAIL_INPUT_SUB_STATE_WRONG_EMAIL_OR_PASSORD" : "Wrong email or password",
"AUTHORIZATION_STATE_EMAIL_CHANGEING_SUB_STATE_WRONG_EMAIL" : "Wrong email",

"YES_NO_DIALOG_YES_TEXT" : "Yes",
"YES_NO_DIALOG_NO_TEXT" : "No",
"YES_NO_DIALOG_CANCEL_TEXT" : "Cancel",
"YES_NO_DIALOG_UNLOCK_TEXT" : "Unlock",
"YES_NO_DIALOG_MANUALY_TEXT" : "Manualy",

"INFO_INPUT_STATE_DESCRIPTION" : "We only ask you for information that helps us create a training program tailored for you. It won't be shared with anyone else.",
"INFO_INPUT_STATE_AGE_DESCRIPTION" : "Age",
"INFO_INPUT_STATE_AGE_PLACEHOLDER_TEXT" : "AGE",
"INFO_INPUT_STATE_GENDER_TOGGLE_MAN" : "MAN",
"INFO_INPUT_STATE_GENDER_TOGGLE_WOMAN" : "WOMAN",
"INFO_INPUT_STATE_HEIGHT_DESCRIPTION" : "Height",
"INFO_INPUT_STATE_HEIGHT_PLACEHOLDER" : "HEIGHT",
"INFO_INPUT_STATE_WEIGHT_DESCRIPTION" : "Weight",
"INFO_INPUT_STATE_WEIGHT_PLACEHOLDER" : "SET",
"INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_CM_KG" : "cm",
"INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_IN_LBS" : "inches",
"INFO_INPUT_STATE_DONE_BUTTON" : "DONE",
"INFO_INPUT_STATE_WRONG_AGE_FIELD_TITLE" : "ERROR",
"INFO_INPUT_STATE_WRONG_AGE_FIELD_TEXT" : "Age input is not valid",
"INFO_INPUT_STATE_WRONG_HEIGHT_FIELD_TITLE" : "ERROR",
"INFO_INPUT_STATE_WRONG_HEIGHT_FIELD_TEXT" : "Height input is not valid",

"INFO_INPUT_STATE_HINT_DESCRIPTION_ONE" : "You will be proposed a workout program that will cause additional load to your muscles and heart. We recommend you to consult with your therapist before start of training.",
"INFO_INPUT_STATE_HINT_DESCRIPTION_TWO" : "You can find a lot of useful information about healthy meal and lifestyle in our Help section.",
"INFO_INPUT_STATE_HINT_BUTTON_TEXT" : "CREATE A 3D MODEL",
"INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TITLE" : "Height is out of range",
"INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TEXT" : "It should be between 90 and 240 cm",

"INFO_INPUT_STATE_AGE_OUT_OF_RANGE_TITLE" : "Age is out of range",
"INFO_INPUT_STATE_AGE_OUT_OF_RANGE_TEXT" : "It should be between 13 and 90 years old",


"DESCRIPTION_STATE_FIRST_PAGE_TITLE" : "YOUR VISION",
"DESCRIPTION_STATE_FIRST_PAGE_DESCRIPTION" : "Hello! You are seeing this note because you have decided to get fitter and healthier. Allow us to show the right direction and entertain you all along the way. We will collect some information about you to figure out our starting point. This will remain strictly confidential.",
"DESCRIPTION_STATE_SECOND_PAGE_TITLE" : "YOUR BODY",
"DESCRIPTION_STATE_SECOND_PAGE_DESCRIPTION" : "Based on your selfie pictures and your personal parameters we will build a 3D model of your body. You can change the model how you want it to look like. You will be able to change your body this way too. Get a workout course based on your current condition, your goal and the speed of changes. If you can dream it, you can do it.",


"AUTHORIZATION_LOGIN_SWITCHER_FB_BUTTON_TEXT" : "Sign in with Facebook",
"AUTHORIZATION_LOGIN_SWITCHER_OR_TEXT_FIELD" : "or",
"AUTHORIZATION_LOGIN_SWITCHER_LOGIN_WITH_EMAIL_BTN_TEXT" : "Sign in with e-mail",
"AUTHORIZATION_LOGIN_SWITCHER_CONDITIONS_BTN_TEXT" : "I Accept Terms & Conditions",
"AUTHORIZATION_LOGIN_SWITCHER_TITLE" : "START NOW",

"AUTHORIZATION_EMAIL_INPUT_TITLE" : "START NOW",
"AUTHORIZATION_EMAIL_INPUT_FORGOT_PASSWORD_BTN_TEXT" : "Forgot Password",
"AUTHORIZATION_EMAIL_INPUT_CREATE_ACCOUNT_BTN_TEXT" : "Create Account",
"AUTHORIZATION_EMAIL_INPUT_LOG_IN_BTN_TEXT" : "Login",
"AUTHORIZATION_EMAIL_INPUT_EMAIL_PLACEHOLDER_TEXT" : "Email",
"AUTHORIZATION_EMAIL_INPUT_PASSWORD_PLACEHOLDER_TEXT" : "Password",

"AUTHORIZATION_CHANGE_PASSWORD_TITLE" : "CHANGE PASSWORD",
"AUTHORIZATION_CHANGE_PASSWORD_DESCRIPTION" : "Enter your email address below and we will send you a new password.",
"AUTHORIZATION_CHANGE_PASSWORD_EMAIL_PLACEHOLDER" : "Email",
"AUTHORIZATION_CHANGE_PASSWORD_CHANGE_BTN_TEXT" : "Reset",
"AUTHORIZATION_CHANGE_PASSWORD_BACK_TO_LOGIN_SWITCHER_BTN_TEXT" : "or go back to Sign In page",

"AUTHORIZATION_PASSWORD_CHANGED_SUBMENU_DESCRIPTION" : "We have sent you an email with your new password. Check your mail box.",
"AUTHORIZATION_PASSWORD_CHANGED_SUBMENU_EXIT_BUTTON_TEXT" : "Back to Sign In page",

"AUTHORIZATION_CREATE_ACCOUNT_TITLE" : "CREATE ACCOUNT",
"AUTHORIZATION_CREATE_ACCOUNT_NAME_PLACEHOLDER_TEXT" : "Name",
"AUTHORIZATION_CREATE_ACCOUNT_EMAIL_PLACEHOLDER_TEXT" : "Email",
"AUTHORIZATION_CREATE_ACCOUNT_PASSWORD_PLACEHOLDER_TEXT" : "Password",
"AUTHORIZATION_CREATE_ACCOUNT_CONFIRM_PASSWORD_PLACEHOLDER_TEXT" : "Confrim password",
"AUTHORIZATION_CREATE_ACCOUNT_CREATE_ACCOUNT_BTN_TEXT" : "Create account",

"AUTHORIZATION_CONDITIONS_TITLE" : "TERMS & CONDITIONS",

"AUTHORIZATION_CONDITIONS_STATE_TEXT_1" : "<b>Terms of Service</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_2" : "3DLook LLC",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_3" : "Effective Date: December 15, 2016",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_4" : "<i>Please read these terms and conditions of use carefully before accessing, using or obtaining any materials, information, products or our services.</i>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_5" : "3DLook offers services, such as the mobile apps (the <i>Apps</i>) that enable people to create 3D models of their bodies for a range of activities, like entertainment, sport, communication and sharing. By accessing 3DLook website, mobile applications or any other feature powered by 3DLook (collectively <i>Our Service</i>) you agree to be bound by these terms and conditions (<i>Terms</i>) and our Privacy Policy. If you do not accept all of these Terms, then you may not use Our Website. In these Terms, <i>we</i>, <i>us</i>, <i>our</i> or <i>3DLook</i> refers to 3DLook LLC, and <i>you</i> or <i>your</i> refers to you as the user of Our Service. <i>Our Website</i> means http:\/\/3dlook.me web site with all affiliated pages.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_6" : "We may modify these Terms, for any reason at any time, by posting a new version on Our Website; these changes do not affect rights and obligations that arose prior to such changes. Your continued use of Our Website following the posting of modified Terms will be subject to the Terms in effect at the time of your use. Please review these Terms periodically for changes. If you object to any provision of these Terms or any subsequent modifications to these Terms or become dissatisfied with Our Service in any way, your only recourse is to immediately terminate use of Our Service.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_7" : "We may modify these Terms, for any reason at any time, by posting a new version on Our Website; these changes do not affect rights and obligations that arose prior to such changes. Your continued use of Our Website following the posting of modified Terms will be subject to the Terms in effect at the time of your use. Please review these Terms periodically for changes. If you object to any provision of these Terms or any subsequent modifications to these Terms or become dissatisfied with Our Service in any way, your only recourse is to immediately terminate use of Our Service.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_8" : "<b>1. About Our Services</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_9" : "Age. You must be at least 13 years old to use Our Service. If you are not old enough to have authority to agree to our Terms in your country, your parent or guardian must agree to our Terms on your behalf.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_10" : "Registration. When you use Our Service, you may have the option, or be required, to register for an account. When you create an account, you agree to provide accurate, up to date information as required for the account registration. You also agree that you will not use, or attempt to use, another user’s account without authorization from such user.  You will be responsible for all use of Our Service by you, anyone using your password and login information (with or without your permission) and anyone whom you allow to access your account credentials.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_11" : "Mobile Devices. For as long as you use Our Service, you consent to downloading and installing updates to our Services, including automatically. Please be aware that even when you use Our Service for free, you are responsible for your carrier’s rates and fees, such as text messaging and data charges, which apply. We do not guarantee that the Services (including the App) can be used on any particular device or with any particular service plan.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_12" : "<b>2. Privacy Policy and User Data</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_13" : "3DLook's Privacy Policy describes our information practices, including the types of information we receive and collect and how we use and share this information. You agree to our data practices, including the collection, use, processing, and sharing of your information as described in our Privacy Policy, as well as the transfer and processing of your information to the United States and other countries globally where we have or use facilities, service providers, or partners, regardless of where you use our Services. You acknowledge that the laws, regulations, and standards of the country in which your information is stored or processed may be different from those of your own country.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_14" : "<b>3. Intellectual Property</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_15" : "3DLook own all of the text, images, software, trademarks, video or other material contained on Our Website or in our Apps. You will not copy or transmit any of the material except for your personal, non-commercial use. Your use of and access to Our Service does not grant you any license or property right for any content included on Our Service",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_16" : "<b>4. Third-Party Services</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_17" : "Our Services may allow you to access, use or interact with websites, apps, content or other products and services provided by third parties (<i>Third-Party Services</i>) like Facebook, Apple, Instagram etc. In this case Third-Party Services' terms and privacy policies will govern your use of such services. For example, you may choose to share photos, video or other Content from our Services to your Facebook account and Facebook audiences you choose, and then Facebook's Statement of Rights and Responsibility and Data Policy will apply to your use of the Facebook service as well as the Content once it is shared to the Facebook service. Please note that when you access Third-Party Services through Our Service, 3DLook is not responsible for those Third-Party Services and 3DLook does not endorse or make any representations or warranties about those Third-Party Services.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_18" : "<b>5. Acceptable Uses</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_19" : "You may only use our Services in accordance with our Terms and policies (our <i>Terms and Policies</i>). If we disable your account for a violation of our Terms and Policies, you agree that you will not create another account without our permission. If you have reason to believe that your account is no longer secure (e.g., loss, theft or unauthorized disclosure or use of your information or computer or mobile device used to access Our Service), you must promptly change your Personal information that is affected.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_20" : "<b>6. Warranty disclaimer</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_21" : "Our Website, Apps and services are provided on an <i>as is</i> and <i>as available</i> basis; errors can and do happen. We expressly disclaims to the fullest extent permissible all warranties of any kind, whether express or implied, including, but not limited to, any implied warranties of merchantability, fitness for a particular purpose, title, non-infringement, and security and accuracy, as well as all warranties arising by usage of trade, course of dealing, or course of performance.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_22" : "<b>7. Our liability is limited</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_23" : "We (together with our directors, employees, representatives, shareholders, affiliates, and providers) to the extent permitted by law hereby expressly exclude any responsibility and liability for (a) any loss or damages to, or viruses that may infect, your computer equipment or other property as the result of your access to Our Website, your downloading of our Apps from e-shops or Third-Party websites. WE DO NOT WARRANT THAT ANY INFORMATION PROVIDED BY US IS ACCURATE, COMPLETE, OR USEFUL, THAT OUR SERVICES WILL BE OPERATIONAL, ERROR FREE, SECURE, OR SAFE, OR THAT OUR SERVICES WILL FUNCTION WITHOUT DISRUPTIONS, DELAYS, OR IMPERFECTIONS. WE DO NOT CONTROL, AND ARE NOT RESPONSIBLE FOR, CONTROLLING HOW OR WHEN OUR USERS USE OUR SERVICES OR THE FEATURES, SERVICES, AND INTERFACES OUR SERVICES PROVIDE. WE ARE NOT RESPONSIBLE FOR AND ARE NOT OBLIGATED TO CONTROL THE ACTIONS OR INFORMATION (INCLUDING CONTENT) OF OUR USERS OR OTHER THIRD PARTIES. SOME JURISDICTIONS MAY NOT ALLOW CERTAIN LIMITATION ON OR EXCLUSION OF IMPLIED WARRANTIES, SO SOME OF THE ABOVE LIMITATION OR EXCLUSIONS MAY NOT APPLY IN FULL TO YOU.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_24" : "<b>8. Arbitration and Waiver</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_25" : "If You intend to seek arbitration You must first send to the Company, by certified mail, a written Notice of Dispute (<i>Notice</i>). The Notice to Company must be sent to hello@3dlook.me. The Notice shall describe the nature and basis of the claim or disputes and the specific relief sought. If you and we cannot reach an agreement to resolve the claim within thirty (30) days after the Notice is received, you or we may commence arbitration. These Terms and Conditions, the terms of Our Service and the relationship contemplated thereby, shall be governed by the laws of United States and the laws of the State of Nevada, without giving effect to principles of conflicts of law. You agree that exclusive jurisdiction for any claim or dispute with 3DLook or relating in any way to your use of Our Service resides in the courts of the County of Nevada, State of Nevada, and you further agree and expressly consent to the exercise of personal jurisdiction in the courts of the County of Nevada, State of Nevada, in connection with any such dispute and including any claim involving 3DLook or its affiliates, subsidiaries, employees, contractors, directors and content providers. You agree that any cause of action or claim that you may have with respect to your use of Our Service must be commenced within one year after the act or omission giving rise to the claim or cause of action arose. ",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_26" : "<b>9. Your Feedback</b>",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_27" : "We encourage you to share your comments and questions with us, but we may not be able to respond to all of them. Remember that you are responsible for whatever material you submit, including its reliability, originality, and copyright. Please do not reveal trade secrets or other confidential information in your messages. Further, by submitting Feedback you are granting us an irrevocable, perpetual, non-exclusive, transferable, fully paid, worldwide license (with the right to freely sublicense) to use, copy, modify, publicly perform, publicly display, reformat, translate, syndicate, republish, excerpt (in whole or in part) and distribute Feedback we receive from you for any purpose, including business, commercial, marketing, advertising, or otherwise.",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_28" : "Our Website is operated by:",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_29" : "3DLook LLC",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_30" : "5348 Vegas Drive,",
"AUTHORIZATION_CONDITIONS_STATE_TEXT_31" : "Las Vegas, NV 89108",

"TRAINING_SESSIONS_HEADER_TEXT" : "SESSION ", 
"TRAINING_SESSIONS_TITLE_SIMPLE" : "GOOD JOB TODAY!",
"TRAINING_SESSIONS_TITLE_ROUND" : "CONGRATULATIONS!",
"TRAINING_SESSIONS_DESCRIPTION_SIMPLE" : "You have completed the * session on the way to your best shape. Let’s share this result in your timeline!",
"TRAINING_SESSIONS_DESCRIPTION_ROUND" : "You completed the * week workout, let’s share this result in your timeline!",
"TRAINING SESSIONS_NOTIFICATION_COMPLETE" : "PROGRAM COMPLETE",
"TRAINING_SESSIONS_TITLE_COMPLETE" : "PERFECT!",
"TRAINING_SESSIONS_DESCRIPTION_COMPLETE" : "Let's check the results!",
"TRAINING_SESSIONS_FROM_TOTAL_TEXT" : "to your goal",
"TRAINING_SESSIONS_CIRCLE_TITLE_SIMPLE" : "SESSION *  DONE",
"TRAINING_SESSIONS_CIRCLE_TITLE_ROUND" : "SESSION *  DONE",
"TRAINING_SESSIONS_CIRCLE_TITLE_COMPLETE" : "PROGRAM COMPLETE",
"TRAINING_SESSIONS_UPPER_BUTTON_TEXT_SIMPLE" : "Done",
"TRAINING_SESSIONS_UPPER_BUTTON_TEXT_ROUND" : "Next session",
"TRAINING_SESSIONS_DESCRIPTION_WHITE" : "You have completed  *  training session on the way to your best shape. Let’s share this result in your timeline!",
"TRAINING_SESSIONS_DESCRIPTION_ORANGE" : "Tomorrow you will get a new set of exercises. See you soon!",
"TRAINING_SESSIONS_PLAY_BTN_TEXT" : "START",

"TRAINING_SESSIONS_SHARE_SESSION_COMPLETE" : "SESSION DONE",
"TRAINING_SESSIONS_SHARE_SESSION_FROM" : "from ",

"TRAINING_SESSIONS_PHOTO_BUTTON_DESCR" : "Take a picture",
"TRAINING_SESSIONS_FB_BUTTON_DESCR" : "Share with FB",
"TRAINING_SESSIONS_TW_BUTTON_DESCR" : "Twit",

"TRAINING_SESSIONS_SHARE" : "SHARE",
"TRAINING_SESSIONS_DESCR_BOTTOM" : "Tomorrow you will get a new set of exercises. See you soon!",

"TRAINING_STATE_INTRO_TITLE" : "TRAINING",
"TRAINING_STATE_EXERCISES_TITLE" : "SESSION",
"TRAINING_STATE_INTRO_SESSION_TEXT" : "SESSION",
"TRAINING_STATE_INTRO_WEEK_TEXT" : "WEEK",
"TRAINING_STATE_INTRO_DAY_TEXT" : "DAY",
"TRAINING_STATE_INTRO_TIME_TEXT" : "TIME",
"TRAINING_STATE_INTRO_START_TRAINING_BUTTON_TEXT" : "Let's go!",

"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_BTN_TEXT" : "Skip",
"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_BTN_COMPLETED_EXERSISE" : "Completed",
"TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_BTN_DOWNLOADING" : "Loading",
"TRAINING_STATE_EXERSISES_EXERCISE_INFO_SETS_TITLE" : "Series",
"TRAINING_STATE_EXERSISES_EXERCISE_INFO_REPS_TITLE" : "Repetitions",
"TRAINING_STATE_EXERSISES_EXERCISE_INFO_WEIGHT_TITLE" : "Weight(kg)",

"TRAINING_STATE_EXERSISES_BREAK_TIMER_UPPER_TEXT" : "YOU NEED A BREAK",
"TRAINING_STATE_EXERSISES_BREAK_TIMER_LOWER_TEXT_SINGLE_SECOND" : "SECOND",
"TRAINING_STATE_EXERSISES_BREAK_TIMER_LOWER_TEXT_MULTIPLY_SECOND" : "SECONDS",
"TRAINING_STATE_EXERSISES_EXIT_BUTTON_TEXT" : "exit",

"TRAINING_STATE_EXERCISE_SUB_STATE_WELL_DONE_POP_UP_TEXT": "EXCELLENT, NEXT EXERCISE",

"TRAINING_STATE_JOG_SUBSTATE_DESCRIPTION" : "YOU HAVE 15 MINUTES OF RUNNING TODAY",
"TRAINING_STATE_JOG_SUBSTATE_CLICK_BUTTON_HINT" : "Click the button when you're done",
"TRAINING_STATE_JOG_SUBSTATE_BUTTON_TEXT" : "Running done",
"TRAINING_STATE_JOG_SUBSTATE_TITLE" : "TRAINING",

"TRAINING_QUOTE_COUNT" : "7",
"TRAINING_STATE_INTRO_QUOTE_1" : "THE MORE DIFFICULT THE VICTORY, THE GREATER THE HAPPINESS IN WINNING.^Pele, Brazillian soccer legend",
"TRAINING_STATE_INTRO_QUOTE_2" : "Pain is weakness leaving the body.",
"TRAINING_STATE_INTRO_QUOTE_3" : "Being defeated is often a temporary condition. Giving up is what makes it permanent.",
"TRAINING_STATE_INTRO_QUOTE_4" : "Failure is only a temporary change in direction to set you straight for your next success.",
"TRAINING_STATE_INTRO_QUOTE_5" : "To achieve something you’ve never had before, you must do something you’ve never done before.",
"TRAINING_STATE_INTRO_QUOTE_6" : "You are born weak and die weak, what you are in between those two periods of time is up to you..",
"TRAINING_STATE_INTRO_QUOTE_7" : "If the bar ain’t bending you‘re just pretending.",

"STUDIO_SETGOAL_SUB_TITLE" : "SELECT YOUR TRAINING",
"STUDIO_SETGOAL_SUB_SUBTITLE" : "WEEKS TO YOUR IDEAL BODY",
"STUDIO_SETGOAL_SUBTEXT": "Drag the slider to see how you would look if you will start practicing now!",
"STUDIO_SETGOAL_BUTTON_WEIGHT" : "Lose Weight",
"STUDIO_SETGOAL_BUTTON_CUSTOM" : "Custom training",


"STUDIO_SETGOAL_LOSEWEIGHT_TXT" : "Lose Weight",
"STUDIO_SETGOAL_GETFIT_TXT" : "Get Fit",
"STUDIO_SETGOAL_CUSTOM_TXT" : "Custom",
"STUDIO_SETGOAL_STARTBUTTON" : "Start training",


"STUDIO_PROGRESS_SUB_START_BTN" : "START",
"STUDIO_PROGRESS_SUB_PROGRESS_BTN" : "NOW",
"STUDIO_PROGRESS_SUB_GOAL_BTN" : "GOAL",
"STUDIO_PROGRESS_SUB_DATE_COUNTER_TEXT" : "* days till you can measure your progress",
"STUDIO_PROGRESS_SUB_MAKE_PHOTO" : "Resume training",
"STUDIO_PROGRESS_SUB_START_TRAINING" : "Resume training",
"STUDIO_PROGRESS_HEADER" : "PROGRESS",


"STUDIO_DESCRIPTION_SUB_BASIC_BUTTON_TEXT" : "I want to go with the basic training",
"STUDIO_DESCRIPTION_SUB_BASIC_PREMIUM_BUTTON_TEXT" : "Unlock the fast training - *",
"STUDIO_DESCRIPTION_SUB_BASIC_DESCRIPTION" : "The program can be easely performed at home using such things as dumbbells (can be switched by bottle of water or something else heavy enough), a chair, a table or a sofa for body support.",
"STUDIO_DESCRIPTION_SUB_BASIC_MONTHS" : "MONTHS",
"STUDIO_DESCRIPTION_SUB_BASIC_EXCERCISES" : "EXCERCISES",
"STUDIO_DESCRIPTION_SUB_BASIC_HOURS" : "HOURS",


"STUDIO_DESCRIPTION_SUB_PREMIUM_BUTTON_TEXT" : "Start training",

"STUDIO_DESCRIPTION_SUB_PREMIUM_ONLY" : "ONLY",

"STUDIO_BODY_TYPE_TITLE" : "YOUR BODY NOW",
"STUDIO_BODY_TYPE_SUBTITLE" : "YOUR BODY TYPE",
"STUDIO_BODY_TYPE_BODY_TYPE_MESO" : "MESOMORPH",
"STUDIO_BODY_TYPE_BODY_TYPE_EXO" : "ECTOMORPH",
"STUDIO_BODY_TYPE_BODY_TYPE_ENDO" : "ENDOMORPH",
"STUDIO_BODY_TYPE_BODY_TYPE_OSHAPED" : "O-Shaped",
"STUDIO_BODY_TYPE_BODY_TYPE_TSHAPED" : "T-Shaped",
"STUDIO_BODY_TYPE_BODY_TYPE_XSHAPED" : "X-Shaped",
"STUDIO_BODY_TYPE_BODY_SUBSTRING" : "Some text and it is so long as something too long to fit in several lines, indeed",

"STUDIO_BODY_TYPE_BODY_SUBSTRING_ENDO" : "Endomorphs tend to struggle with their weight, gaining weight easily and losing weight with difficulty. They are naturally strong and have good endurance and movement, and tend to do well in middle-distance activities.",
"STUDIO_BODY_TYPE_BODY_SUBSTRING_MESO" : "A mesomorph has a large bone structure, large muscles and a naturally athletic physique. Mesomorphs are the best body type for bodybuilding.",
"STUDIO_BODY_TYPE_BODY_SUBSTRING_EXO" : "Ecto’s have a light build with small joints and lean muscle. Workouts should be short and intense focusing on big muscle groups.",
"STUDIO_BODY_TYPE_BODY_SUBSTRING_TSHAPED" : "With a ruler, there’s only a tiny bit of a curve at the hips. They mostly have a straight torso, with shoulders that align with the torso.",
"STUDIO_BODY_TYPE_BODY_SUBSTRING_OSHAPED" : " Commonly called apples, women with a circle body shape have smaller shoulders and hips. They also tend to have slender legs and a slim booty. All fit!",
"STUDIO_BODY_TYPE_BODY_SUBSTRING_XSHAPED" : "This body shape is curvy in all the right places: bust and booty. It’s pretty much a universal perception of what is womanly and attractive.",

"STUDIO_BODY_TYPE_BODY_START_BUTTON_TEXT" : "Start",

"FRIEND_INVITE_STATE_TITLE" : "TRAINING",
"FRIEND_INVITE_STATE_DESCRIPTION" : "Invite your friends to join: you can train together or compete with each other. Anyway it will be fun and even more motivation!",
"FRIEND_INVITE_STATE_START_TRAINING_BTN" : "Start training",

"RELAX_STATE_TITLE" : "TRAINING",
"RELAX_STATE_DESCRIPTION_FREE_DAY" : "YOU WORKED HARD YERSTERDAY. RELAX TODAY.",
"RELAX_STATE_DESCRIPTION_TOO_EALRY_FIRST_PART" : "Your muscles need some time for recovering.",
"RELAX_STATE_DESCRIPTION_TOO_EALRY_SECONDT_PART" : "You'll get a new program in",
"RELAX_STATE_STUDIO_BTN_NEXT" : "HOME",
"RELAX_STATE_DESCRIPTION_HOUR_MULTIPLY" : "HOURS",
"RELAX_STATE_DESCRIPTION_HOUR_SINGLE" : "HOUR",

"INNER_MENU_PROGRESS_BTN" : "Progress",
"INNER_MENU_LOGOUT_BTN" : "Log Out",
"INNER_MENU_PROGRAM_BTN" : "Program",
"INNER_MENU_FAQ_BTN" : "FAQ", 
"INNER_MENU_SUPPORT_BTN" : "Support",
"INNER_MENU_FEEDBACK_BTN" : "Leave feedback",
"INNER_MENU_RESUME_BTN" : "Resume",

"PHOTO_STATE_HINT_UP_1_2RAW" : "the frame. When shooting in the",
"PHOTO_STATE_HINT_UP_1_3RAW" : "mirror, one's hands should be",
"PHOTO_STATE_HINT_UP_1_4RAW" : "placed apart from the body.",

"PHOTO_STATE_HINT_UP_2_2RAW" : "the frame. When shooting in the",
"PHOTO_STATE_HINT_UP_2_3RAW" : "mirror, one's hands should be",
"PHOTO_STATE_HINT_UP_2_4RAW" : "placed along the body.",

"PHOTO_STATE_HINT_UP_3_2RAW" : "Remove glasses,",
"PHOTO_STATE_HINT_UP_3_3RAW" : "Make sure that you have",
"PHOTO_STATE_HINT_UP_3_4RAW" : "removed hair from the face",
"PHOTO_STATE_BACK_BUTTON" : "Create a 3D model",

"INFO_INPUT_STATE_HINT_TITLE" : " ",
"INFO_INPUT_STATE_WEIGHT_OUT_OF_RANGE_TITLE" : "Weight is out of range",
"INFO_INPUT_STATE_WEIGHT_OUT_OF_RANGE_TEXT" : "Weight is absolutely out of range",

"INFO_INPUT_STATE_WRONG_WEIGHT_FIELD_TITLE" : "ERROR",
"INFO_INPUT_STATE_WRONG_WEIGHT_FIELD_TEXT" : "Weight input is not valid",

"DESCRIPTION_STATE_THIRD_PAGE_TITLE" : "YOUR GOAL",
"DESCRIPTION_STATE_THIRD_PAGE_DESCRIPTION" : "Some long text. Some long text. Some long text. Some long text. Some long text. Some long text. Some long text. Some long text. Some long text. ",
"STUDIO_SETGOAL_SUB_CURRENT_WEIGHT_KG" : "Current weight is * kg",
"STUDIO_SETGOAL_SUB_CURRENT_WEIGHT_LB" : "Current weight is * lb",

"STUDIO_SETGOAL_SUB_GOAL_TEXT_KG" : "I want to lose * kg",
"STUDIO_SETGOAL_SUB_GOAL_TEXT_LB" : "I want to lose * lb",

"STUDIO_DESCRIPTION_SUB_BASIC_TITLE" : "BASIC TRAINING",
"STUDIO_DESCRIPTION_SUB_PREMIUM_TITLE" : "FAST TRAINING",
"STUDIO_DESCRIPTION_SUB_BASIC_SUBTITLE_FROM_KG" : "FROM * KG",
"STUDIO_DESCRIPTION_SUB_BASIC_SUBTITLE_TO_KG" : "TO * KG",

"STUDIO_DESCRIPTION_SUB_PREMIUM_CANCEL_TITLE" : "Cancelation policies",
"STUDIO_DESCRIPTION_SUB_PREMIUM_CANCEL_DESCR" :  "Cancel anytime. But if you unsubscribe, you'll lose your progress. However, you will have 1 month to resume your training.",


"FAQ" : {

"I have got the \Incorrect photo\ error" : "Please check the following: Are you standing in a well lighted place? Try this: let the photographer stand towards the sun while it is behind your back. The contrast will do the thing. Whether your body is within the frame during the shooting. Literally, your head and your feet should remain between the two lines on your screen. Perhaps, something went wrong and you could not take photos anyway. Don't worry, we can create a 3D model manually. However in 2 weeks you will be prompted to take new photos to estimate your progress. Hopefully, you’ll have more lucky shooting the next time.",
"Why should I pay for the subscription if the free version of 3DFit offers me a nice workout program?" : "You will definitely get results using the free version, especially the first weeks. 3DFit proposes an effective fitness program which helps you to balance your weight (lose extra pounds or, vice versa, add some muscles mass if you need) and get more attractive body curves. But perhaps you want to reach your personal goal. For example, you are happy with your hips but want your waist to be slimmer. Subscription option allows you to get a custom workout focused on the body parts which you have specified. Also, there is a medical fact that in 4 weeks a body gets used to any workout course and responds less effectively to the same exercises. Subscription opens you different training programs every new month.",
"The application creates a 3D model that doesn't reflect my body type correctly." : "Most likely you were wearing not fitting clothes during the shooting. The more close things that you put on, the more accurate results we get. Fitness top or a sleeveless shirt and shorts are the best option.",
"I have to choose between \Lose weight\ and \Get fitter\ options. What if I want both them?" : "Some exercises that form your body shape lead to growth of your muscles mass and volume. That is why it makes sense to lose some extra weight at first, and then switch toa body building program.",
"How did you compose these workout programs? Are they medically proven?" : "Yes, they are. We only implemented the programs which were composed by professional fitness coaches with many years of experience. Anyway, remember that there is no single solution for everyone. 3DFit gives you workout routines as a recommendation, not an order. If you have any doubts or feel pain during the exercises, please consult with your doctor.",
"I was asked for some personal information like my age and body measurements. Can I be sure that it remains confidential?" : "We respect your privacy and won't share your data with any third parties without your permission. You can read more about our Privacy Policy here: 3dlook.me/privacy-policy.html. If you didn't find an answer to your question here, please contact us directly at support@3dlook.me",
"I don't like a created 3D model, can I re-make it?" : "Yes."

	}


}

