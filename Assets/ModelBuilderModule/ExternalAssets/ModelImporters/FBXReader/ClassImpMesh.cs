using UnityEngine;
using System.Collections;

namespace FBXImporter
{
	public class ClassImpMesh{

		uint id;
		string name;
		uint pId;
		Vector3 origin;

		public ClassImpMesh(uint i)
		{
			id = i;
			origin = new Vector3(0f,0f,0f);
			pId = 0;
			name = "";
		} // ClassImpMesh

		public void SetID(uint i)
		{
			id = i;
		} // SetID()

		public void SetName(string n)
		{
			name = n;
		} // SetName()

		public void SetPID(uint i)
		{
			pId = i;
		} // SetPID()

		public void SetOrig(Vector3 v)
		{
			origin = v;
		} // SetOrig()

		public uint GetID()
		{
			return id;
		} // GetID()

		public uint GetPID()
		{
			return pId;
		} // GetPID()

		public string GetName()
		{
			return name;
		} // GetName()

		public Vector3 GetOrig()
		{
			return origin;
		} // GetOrig()
	}
}
