﻿using UnityEngine;
using System.Collections;

public class bl_EnableController : MonoBehaviour, IProjectEventListener
{
    [SerializeField]
    private GameObject mOrbitCamera;

    [SerializeField]
    private GameObject mTouchOrbitCamera;

    void Awake()
    {
        ProjectEventManager.getInstance ().addListener (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_CHANGED, this);
    }

    /*===============================================================================================
    * 
    * IEventListener implementation
    * 
    * =============================================================================================*/
    public void onEvent(ProjectEventManager.eEventType eventType, IProjectEventSender sender, object[] parameters, int paramsCunt)
    {
        switch (eventType) 
        {
            case ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_CHANGED:
                {
				onFlowChanged((eFlowStateBase)parameters [0]);
                    break;
                }
        }
    }

	private void onFlowChanged(eFlowStateBase newState)
    {
		if (newState == eFlowStateBase.E_STATE_RESULT_SHOWING)
		{
			mOrbitCamera.gameObject.SetActive (true);
			mTouchOrbitCamera.gameObject.SetActive (true);
		} 
		else 
		{
	//		mOrbitCamera.gameObject.SetActive(false);
	//		mTouchOrbitCamera.gameObject.SetActive(false);
		}
    }
    /*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/
}
