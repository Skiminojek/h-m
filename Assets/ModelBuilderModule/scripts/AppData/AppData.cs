﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class AppData : MonoBehaviour 
{
    private static AppData mInstance = null;
	private string mUserID = "";
	private string mAuthKey = "";

    AppDataAdapter mAdapter;

    private HashSet<string> mKeysForExclude = new HashSet<string>();

	HashSet<string> mKeys;

    public static AppData getInstance()
    {
        if (null == mInstance)
        {
            GameObject singleton = new GameObject();
            singleton.name = "AppData";
            mInstance = singleton.AddComponent<AppData>();
			mInstance.setCurrentUserID (PlayerPrefs.GetString ("last_user_id", ""));
			mInstance.setCurrentUserAuthKey (PlayerPrefs.GetString ("last_user_key", ""));
			mInstance.createExludedList ();

			mInstance.fillKeys ();
        }
			
        return mInstance;
    }

	private void fillKeys()
	{
        mAdapter = new AppDataAdapter();

        mKeys = new HashSet<string> ();

		if ("" != mUserID) 
		{
			string keys = PlayerPrefs.GetString(string.Format("player_prefs_user_{0}", mUserID), "");

			if ("" != keys)
			{
				mKeys = new HashSet<string>(keys.Split (','));
			}
		}
	}

	void OnApplicationFocus(bool isFocus)
	{
		JSONObject data = new JSONObject ();
		data.AddField (DataAttributes.UIControllerAttributes.mLastApplicationFocusState, !isFocus);
		AppData.getInstance ().saveDataGlobal ("", "UIController", data);

	}

	void OnApplicationQuit()
	{
		JSONObject data = new JSONObject ();
		data.AddField (DataAttributes.UIControllerAttributes.mIsInStartOfApplicationAttribute, true);
		AppData.getInstance ().saveDataGlobal ("", "UIController", data);
	}
		
	public void saveData(eFlowStateBase state, Dictionary<int, JSONObject> JSONData)
	{
		if ("" == mUserID && state != eFlowStateBase.E_GLOBAL) 
		{
			Debug.LogError ("[AppData.saveData]Unknown user id");
			return;
		}

		foreach (KeyValuePair<int, JSONObject> data in JSONData) 
		{
			List<string> keys = data.Value.keys;

			if (null != keys)
			{
				foreach (string key in keys)
				{
					if (null == key)
						continue;

                    string saveKey = getSaveKey(state, key, mUserID);
					string innerData =  data.Value.GetField(key).ToString();

					if (false == mKeys.Contains (saveKey)) 
					{
						mKeys.Add (saveKey);
						PlayerPrefs.SetString(string.Format("player_prefs_user_{0}", mUserID), string.Join(",", mKeys.ToArray()));
					}

					PlayerPrefs.SetString (saveKey, innerData);
				}
			}
		}
	}

	public void saveDataShared(string firstKey, string secondKey, JSONObject data)
	{
		if ("" == mUserID) 
		{
			Debug.LogError ("[AppData.saveDataShared]Unknown user id");
			return;
		}

		Dictionary<int, JSONObject> mappedData = new Dictionary<int, JSONObject> ();
        int classNameHash = (firstKey + "." + secondKey).GetHashCode ();
		mappedData.Add (classNameHash, data);
		saveData (eFlowStateBase.E_SHARED, mappedData);
	}

	public void saveDataGlobal(string firstKey, string secondKey, JSONObject data)
	{
		Dictionary<int, JSONObject> mappedData = new Dictionary<int, JSONObject> ();
		int classNameHash = (firstKey + "." + secondKey).GetHashCode ();
		mappedData.Add (classNameHash, data);
		saveData (eFlowStateBase.E_GLOBAL, mappedData);
	}

	public T getData<T>(eFlowStateBase state, string paramName, T defaultValue, bool isHadToBePersisted = false)
	{
		if ("" == mUserID && state != eFlowStateBase.E_GLOBAL) 
		{
			//Debug.LogError (string.Format("[AppData.getData]Unknown user id, Default value - {0} will be returned", defaultValue.ToString ()));
			return defaultValue;
		}

        string saveKey = getSaveKey(state, paramName, mUserID);
		string savedData = PlayerPrefs.GetString (saveKey, "");

		if ("" == savedData) 
		{
			if(true == isHadToBePersisted)
				Debug.LogError (string.Format("[AppData.getData]In state [{0}], parameter with name [{1}] is absent", (int)state, paramName));
			
			return defaultValue;
		}

		JSONObject paramData = new JSONObject (savedData);

		if (typeof(T) == typeof(string)) 
		{
			if (true == paramData.IsString) 
			{
				return (T)Convert.ChangeType (paramData.str, typeof(T));
			}
			else 
			{
				Debug.LogError (string.Format("[AppData.getData]In state [{0}], parameter with name [{1}] is not a string", (int)state, paramName));
			}
		}
		else if(typeof(T) == typeof(JSONObject))
		{
			if (true == paramData.IsObject || true == paramData.IsArray) 
			{
				return (T)Convert.ChangeType (paramData, typeof(T));
			}
			else 
			{
				Debug.LogError (string.Format("[AppData.getData]In state [{0}], parameter with name [{1}] is not a JSONObject", (int)state, paramName));
			}
		}
		else if (typeof(T) == typeof(int) || typeof(T) == typeof(float) || typeof(T) == typeof(double)) 
		{
			if (true == paramData.IsNumber) 
			{
				return (T)Convert.ChangeType (paramData.n, typeof(T));
			}
			else 
			{
				Debug.LogError (string.Format("[AppData.getData]In state [{0}], parameter with name [{1}] is not a number", (int)state, paramName));
			}
		}
		else if (typeof(T) == typeof(bool)) 
		{
			if (true == paramData.IsBool) 
			{
				return (T)Convert.ChangeType (paramData.b, typeof(T));
			}
			else 
			{
				Debug.LogError (string.Format("[AppData.getData]In state [{0}], parameter with name [{1}] is not a bool", (int)state, paramName));
			}
		}
		else
		{
			Debug.LogError (string.Format("[AppData.getData]Got ungandeled type - {0}", typeof(T).ToString()));
		}

		return defaultValue;
	}

	public void setCurrentUserID(string userId)
	{
		mUserID = userId;
		PlayerPrefs.SetString ("last_user_id", mUserID);
	}

	public void setCurrentUserAuthKey(string key)
	{
		mAuthKey = key;
		PlayerPrefs.SetString ("last_user_key", key);
	}

	public string getCurrentUserID()
	{
		return mUserID;
	}

	public string getCurrentAuthKey()
	{
		return mAuthKey;
	}

	public bool isUserIDExist()
	{
		return PlayerPrefs.GetString ("last_user_id", "") != "";
	}
		
	public void clearKeys()
	{
		mKeys.Clear ();
	}

	public void removeKey(eFlowStateBase state, string key)
	{
		string record = getSaveKey (state, key, mUserID);
		PlayerPrefs.DeleteKey (record);
		bool result = mKeys.Remove (record);
	}



	private void updatePlayerPrefs(JSONObject prefs)
	{
		if (null == prefs)
			return;

		HashSet<string> keys = new HashSet<string>(prefs.keys);

		if (true == mKeys.Equals (keys))
			return;

		foreach (string key in keys) 
		{
			if (false == mKeys.Contains (key))
				mKeys.Add (key);

			JSONObject obj = new JSONObject ();

			JSONObject js = prefs.GetField (key);

			string stringKey = "";
			if (js.IsString)
				stringKey = prefs.GetField (key).str;
			else
				stringKey = js.ToString ();
			
			int i;
			float j;
			bool k;

			if (null != stringKey) 
			{
				if (true == int.TryParse (stringKey, out i) || true == float.TryParse (stringKey, out j))
					PlayerPrefs.SetString (key, stringKey);
				else if(stringKey.Contains("{") && stringKey.Contains("}"))
					PlayerPrefs.SetString (key, new JSONObject(stringKey).ToString());
				else if(bool.TryParse(key, out k))
					PlayerPrefs.SetString (key, stringKey);
				else
					PlayerPrefs.SetString (key, "\"" + stringKey + "\"");
			}	
		}
	}

	public void TryToUpdatePlayerPrefs(JSONObject data)
	{
		JSONObject playerPrefsVersionServerJS = data.GetField(getSaveKey(eFlowState.E_SHARED, DataAttributes.Authorization.mPlayerPrefsVersion, mUserID));
		int persistedPlayerPrefsVersion = AppData.getInstance().getData<int>(eFlowState.E_SHARED, DataAttributes.Authorization.mPlayerPrefsVersion, 0);
		int serverPlayerPrefsVerison;
		if(null != playerPrefsVersionServerJS && true == int.TryParse(playerPrefsVersionServerJS.str, out serverPlayerPrefsVerison))
		{
			if(persistedPlayerPrefsVersion < serverPlayerPrefsVerison)
				AppData.getInstance().updatePlayerPrefs(data);
		}
	}

	private string getSaveKey(eFlowStateBase state, string paramName, string userID)
    {
		string saveKey = "";

		if ((eFlowStateBase)state == eFlowStateBase.E_GLOBAL) 
		{
			saveKey = string.Format ("state_{0}_attributeName_{1}_data", ((int)(state)).ToString (), paramName);
		} 
		else 
		{
			saveKey = string.Format ("state_{0}_attributeName_{1}_userID_{2}_data", ((int)(state)).ToString (), paramName, userID);
		}
			
        return saveKey;
    }

	private bool isKeyExcluded(string key)
	{
		if (null == mKeysForExclude)
			return false;

		foreach (string excluded in mKeysForExclude)
			if (key.Contains ("attributeName_" + excluded + "_data") || key.Contains ("attributeName_" + excluded + "_userID"))
				return true;
		
		return false;
	}

	private void createExludedList()
	{
		mKeysForExclude = new HashSet<string>(new []{
			DataAttributes.DateAttributes.mStartApplicationDateAttribute,
			DataAttributes.Shared.mFirstName,
			DataAttributes.UIControllerAttributes.mLastStateAttribute,
			DataAttributes.UIControllerAttributes.mPreviousStateAttribute,
			DataAttributes.UIControllerAttributes.mLastSubStateAttribute,
			DataAttributes.UIControllerAttributes.mLastSceneAttribute,
			DataAttributes.UIControllerAttributes.mIsInStartOfApplicationAttribute,
			DataAttributes.UIControllerAttributes.mLastApplicationFocusState
		}
		);
	}

	private void incrementPrefsVersion()
	{
		int version = AppData.getInstance ().getData<int> (eFlowState.E_SHARED, DataAttributes.Authorization.mPlayerPrefsVersion, 0);

		JSONObject data = new JSONObject ();
		data.AddField (DataAttributes.Authorization.mPlayerPrefsVersion, ++version);
		AppData.getInstance ().saveDataShared ("player_prefs", "", data);
	}
}
