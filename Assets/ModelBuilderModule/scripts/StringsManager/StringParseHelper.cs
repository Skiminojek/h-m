﻿using UnityEngine;
using System.Collections;

public static class StringParseHelper
{

    public static string replaceStar(string baseString, string replaceFor)
    {
        string result;
        
            char[] delimiterChars = { '*' };

        try
        {
            string[] splittedString = baseString.Split(delimiterChars);
            result = splittedString[0] + replaceFor + splittedString[1];

        }
        catch
        {
            return baseString;
        }


            return result;
        
    }

}
