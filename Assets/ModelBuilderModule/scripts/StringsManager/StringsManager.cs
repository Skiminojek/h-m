﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class StringsManager : MonoBehaviour
{
	private static StringsManager mInstance = null;
	private Dictionary<string, string> mStrings = null;
	private string mRawString = null;

	// SystemLanguage mDebugSystemLanguage = SystemLanguage.English;
    SystemLanguage mDebugSystemLanguage = SystemLanguage.Russian;

    void Awake()
	{
		//AppInitor.StringsUpdated += () => {getInstance().readStrings(); getInstance().fillDictionary();};
	}

	private void readStrings()
	{
		TextAsset rawString = Resources.Load (getStringsPath()) as TextAsset;
		mRawString = rawString.text;
	}

	private void fillDictionary()
	{
		JSONObject stringsObj = new JSONObject (mRawString);
		List<string> keys = stringsObj.keys;
		mStrings = new Dictionary<string, string> ();

		foreach (string key in keys) 
		{
			string value = stringsObj.GetField (key).str;

			try
			{
				mStrings.Add (key, value);
			}
			catch 
			{
				Debug.LogError ("FILL STRINGS EXEPTION ON - " + key);
			}
		}
	}

	private string getStringsPath()
	{
		string path = "";

		if (true == Debug.isDebugBuild)
		{
			path = getPath (mDebugSystemLanguage);
		} 
		else 
		{
			path = getPath (Application.systemLanguage);
		}

		return path;
	}

	public static string getPath(SystemLanguage type)
	{
		switch (type) 
		{
		case SystemLanguage.Russian:
			{
				return "locales/strings_ru";
			}
		default:
			{
				return "locales/strings_ru";
				//return "locales/strings_en";
			}
		} 
	}

	private static StringsManager getInstance()
	{
		if (null == mInstance) 
		{
			GameObject singleton = new GameObject ();
            singleton.name = "StringsManager";
			mInstance = singleton.AddComponent<StringsManager> ();
			mInstance.readStrings ();
			mInstance.fillDictionary ();
		}

		return mInstance;
	}

	public static string getString(string key)
	{
		if (null == mInstance) 
		{
			getInstance ();
		}

		if (true == mInstance.mStrings.ContainsKey (key)) 
		{
			return mInstance.mStrings [key];
		}

		Debug.LogError (string.Format ("key \"{0}\" was not found", key));
		return "";
	}


   
}
