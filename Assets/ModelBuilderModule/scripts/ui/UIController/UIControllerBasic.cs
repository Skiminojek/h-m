﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class eFlowStateBase
{
	public static readonly eFlowStateBase E_STATE_PHOTO_MAKING = new eFlowStateBase(0);
	public static readonly eFlowStateBase E_STATE_RESULT_SHOWING = new eFlowStateBase(1);
	public static readonly eFlowStateBase E_STATE_AUTHORIZATION = new eFlowStateBase(2);

	//For saving global data
	public static readonly eFlowStateBase E_SHARED = new eFlowStateBase(-1);
	public static readonly eFlowStateBase E_GLOBAL = new eFlowStateBase (-2);

	public int internalValue{ get; set;}

	public static bool operator==(eFlowStateBase first, eFlowStateBase second)
	{
		return first.internalValue == second.internalValue;
	}

	public static bool operator!=(eFlowStateBase first, eFlowStateBase second)
	{
		return !(first.internalValue == second.internalValue);
	}

	public static implicit operator int(eFlowStateBase value)
	{
		return value.internalValue;
	}

	public static implicit operator eFlowStateBase(int value)
	{
		return new eFlowStateBase(value);
	}

	public eFlowStateBase(int value)
	{
		internalValue = value;
	}
}

public class UIControllerBasic : MonoBehaviour, IProjectEventListener, IProjectEventSender
{
	protected eFlowStateBase mCurrentState;
	protected eFlowStateBase mPreviousState;
	protected int mCurrentSubState;

	[SerializeField]
	protected List<FlowUnitBasic> mFlowUnits;

	/*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/

	protected virtual void Awake()
	{
		ProjectEventManager.getInstance ().addListener (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_CHANGED, this);
		ProjectEventManager.getInstance ().addListener (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_RESET, this);
	}

	protected virtual void Start () 
	{
		foreach (FlowUnitBasic flowState in mFlowUnits) 
		{
			flowState.gameObject.SetActive (true);
		}

		setNewFlowState (mCurrentState, mCurrentSubState);
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Opened interface
	* 
	* =============================================================================================*/

	public virtual void setNewFlowState(eFlowStateBase newState, int substate)
	{
		mCurrentState = newState;
		ProjectEventManager.getInstance ().postNotification (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_CHANGED, this, packCurrentState (), 1);
	}

	public virtual void resetCurrentState()
	{
		ProjectEventManager.getInstance ().postNotification (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_RESET, this, packCurrentState (), 1);
	}

	public virtual float getInputHeight()
	{
		return 0.0f;
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/


	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

	protected virtual void onMainFlowStateChanged(eFlowStateBase state, int substate)
	{
		for (int i = 0; i < mFlowUnits.Count; ++i)
		{
			if (mFlowUnits[i].mStateType != state)
			{
				mFlowUnits[i].setEnabled(false, substate);
			}
			else
			{
				mFlowUnits[i].setEnabled(true, substate);
			}
		}
	}

	protected virtual void onMainFlowStateReset()
	{
		for (int i = 0; i < mFlowUnits.Count; ++i)
		{
			if (mFlowUnits[i].mStateType == mCurrentState)
			{
				mFlowUnits [i].reset ();
				break;
			}
		}
	}

	protected virtual object[] packCurrentState()
	{
		object[] parameter = new object[1];
		parameter[0] = mCurrentState;
		return parameter;
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* IEventListener implementation
	* 
	* =============================================================================================*/
	public void onEvent(ProjectEventManager.eEventType eventType, IProjectEventSender sender, object[] parameters, int paramsCunt)
	{
		switch (eventType) 
		{
		case ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_CHANGED:
			{
				if (paramsCunt > 0)
				{
					int substate = 0;

					if (paramsCunt > 1) 
					{
						substate = (int)parameters [1];
					}
					
					onMainFlowStateChanged ((eFlowStateBase)parameters [0], substate);
				} 
				else 
				{
					onMainFlowStateChanged (eFlowStateBase.E_STATE_PHOTO_MAKING, 0);
				}
				break;
			}
		case ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_RESET:
			{
				onMainFlowStateReset ();
				break;
			}
		}
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
