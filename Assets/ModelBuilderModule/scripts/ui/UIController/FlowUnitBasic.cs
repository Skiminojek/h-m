﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class FlowUnitBasic : MonoBehaviour 
{
	public eFlowStateBase mStateType { get ; protected set;}
	public abstract void setEnabled (bool isenabled, int substate);
	public abstract void reset (); 
	public virtual void init (){}
}    
