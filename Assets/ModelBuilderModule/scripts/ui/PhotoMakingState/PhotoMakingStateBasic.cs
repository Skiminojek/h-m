﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;



public class PhotoMakingStateBasic : FlowUnitBasic
{
  //  public delegate void ProcessCallback(eErrorID errorID, ePhotoSubstate mCurrentSubState, Dictionary<InputParams.eExpectedResult, GameObject> mResult, string errorMsg = "");

    //Start params from user
  //  [SerializeField]
 //   protected InputParams mInputParams;

    [SerializeField]
    protected UIControllerBasic mUIControllerBasic;

    //Temporary hints
    [SerializeField]
    protected List<Image> mHints;

    protected Dictionary<ePhotoSubstate, Image> mMapedHints;

    [SerializeField]
    protected Button mCameraSwapButton;

    [SerializeField]
    protected Image mLoadingImage;

    [SerializeField]
    protected Button mBackButton;

    [SerializeField]
    protected Button mPhotoButton;

    //Dummy textures forusing in the debug mode
    [SerializeField]
    protected List<Image> mTestTextures;
    protected Dictionary<ePhotoSubstate, Image> mMappedTestTextures;

    [SerializeField]
    protected CameraStreamRenderrer mCameraRenderrer;

    public enum ePhotoSubstate
    {
        E_SUBSTATE_FRONT = 1,
        E_SUBSTATE_PROFILE,
        E_SUBSTATE_FACE,
        E_SUBSTATE_COMPLETE
    }

    public enum eErrorID
    {
        E_ERROR_NULL,
        E_ERROR_NOT_RECOGNIZED,
        E_ERROR_NO_CONNECTION,
        E_ERROR_UNHANDLED,
        E_ERROR_INTERRUPTED,
        E_ERROR_IMAGE_UPLOADING
    }

    /*===============================================================================================
    * 
    * MonoBehaviuor implementation
    * 
    * =============================================================================================*/
    protected virtual void Awake()
    {
        mStateType = eFlowStateBase.E_STATE_PHOTO_MAKING;

        //Initing of the dummy photos for debug mode
        mMappedTestTextures = new Dictionary<ePhotoSubstate, Image>(3);
        mMappedTestTextures.Add(ePhotoSubstate.E_SUBSTATE_FRONT, mTestTextures[0]);
        mMappedTestTextures.Add(ePhotoSubstate.E_SUBSTATE_PROFILE, mTestTextures[1]);
        mMappedTestTextures.Add(ePhotoSubstate.E_SUBSTATE_FACE, mTestTextures[2]);

        //Hints initing
        mMapedHints = new Dictionary<ePhotoSubstate, Image>();
        mMapedHints.Add(ePhotoSubstate.E_SUBSTATE_FRONT, mHints[0]);
        mMapedHints.Add(ePhotoSubstate.E_SUBSTATE_PROFILE, mHints[1]);
        mMapedHints.Add(ePhotoSubstate.E_SUBSTATE_FACE, mHints[2]);

        //Set up buttons listeners
        mCameraSwapButton.onClick.AddListener(onSwapCameraButtonClicked);
        mBackButton.onClick.AddListener(onBackButtonClicked);
        mPhotoButton.onClick.AddListener(onPhotoButtonClicked);

    }

    void Update()
    {

    }

    /*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

    /*===================================================================================================
     * 
	* FlowUnitBasic implementation
     * 
     * =================================================================================================*/

    public override void setEnabled(bool isEnabled, int substate)
    {

    }

    public override void reset()
    {

    }

    protected virtual void createDataHandler()
    {

    }
    /*===================================================================================================
	* 
	* END OF BLOCK
	* 
	* =================================================================================================*/

    /*===================================================================================================
	* 
	* Internal logic
	* 
	* =================================================================================================*/

    protected virtual void setInteracteble(bool isInteracteble)
    {

    }

    //Shows and hides hints depends on current state
    protected virtual void switchPreSubstateHints()
    {
    }

    protected virtual void switchHintLabels()
    {

    }

    protected virtual void onBackButtonClicked()
    {

    }

    /*===================================================================================================
	* 
	* END OF BLOCK
	* 
	* =================================================================================================*/

    /*===================================================================================================
	* 
	* PHOTO MAKING LOGIC
	* 
	* =================================================================================================*/

    protected virtual void onPhotoButtonClicked()
    {
        activateLoadingPopUp();
        NetworkHelper.checkConnection(checkConectionCallback);
    }

    protected virtual void checkConectionCallback(bool isConnectionExist)
    {
        if (true == isConnectionExist)
        {
            makePhoto();
        }
        else
        {
            MobileNativeMessage error = new MobileNativeMessage(StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TITLE"), StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TEXT"));
            error.OnComplete += onDialogWindowClose;
        }
    }

    protected virtual void makePhoto()
    {
        mCameraRenderrer.createPhoto((tex) =>
        {
            passScreen(tex);
        });
    }

    protected virtual void passScreen(Texture2D photo)
    {

    }
    /*===================================================================================================
	* 
	* END OF BLOCK
	* 
	* =================================================================================================*/

    /*===================================================================================================
	* 
	* POP UPS LOGIC
	* 
	* =================================================================================================*/
    protected virtual void activateLoadingPopUp()
    {
        setInteracteble(false);
        mPhotoButton.gameObject.SetActive(false);
        mLoadingImage.gameObject.SetActive(true);
        mCameraSwapButton.gameObject.SetActive(false);
    }

    protected virtual void deactivateLoadingPopUp()
    {
        mLoadingImage.gameObject.SetActive(false);
        mPhotoButton.gameObject.SetActive(true);
    }

    /*===================================================================================================
	* 
	* END OF BLOCK
	* 
	* =================================================================================================*/

    /*===================================================================================================
	* 
	* CAMERA LOGIC
	* 
	* =================================================================================================*/

    protected virtual void onSwapCameraButtonClicked()
    {
        mCameraRenderrer.SwitchCamera();
    }

 

    /*===================================================================================================
	* 
	* END OF BLOCK
	* 
	* =================================================================================================*/


    /*===================================================================================================
	* 
	* DIALOG WINDOWS LOGIC
	* 
	* =================================================================================================*/

    protected virtual void switchErrorMessage(eErrorID errorID)
    {
        Debug.Log("ERROR_TYPE_ - " + errorID.ToString());
        switch (errorID)
        {
            case eErrorID.E_ERROR_NOT_RECOGNIZED:
                {
                    MobileNativeMessage error = new MobileNativeMessage(StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TITLE"), StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TEXT"));
                    error.OnComplete += onDialogWindowClose;
                    break;
                }
            case eErrorID.E_ERROR_NO_CONNECTION:
                {
                    MobileNativeMessage error = new MobileNativeMessage(StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TITLE"), StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TEXT"));
                    error.OnComplete += onDialogWindowClose;
                    break;
                }
            case eErrorID.E_ERROR_INTERRUPTED:
                {
                    onDialogWindowClose();
                    break;
                }
            case eErrorID.E_ERROR_IMAGE_UPLOADING:
                {
                    MobileNativeMessage error = new MobileNativeMessage(StringsManager.getString("CANT_UPLOAD_IMAGE_DIALOG_WINDOW_TITLE"), StringsManager.getString("CANT_UPLOAD_IMAGE_DIALOG_WINDOW_TEXT"));
                    error.OnComplete += resetlabels;
                    break;
                }
            case eErrorID.E_ERROR_UNHANDLED:
            default:
                {
                    MobileNativeMessage error = new MobileNativeMessage(StringsManager.getString("NOT_HANDELED_ERROR_MSG_TITLE"), StringsManager.getString("NOT_HANDELED_ERROR_MSG_TEXT"));
                    error.OnComplete += onDialogWindowClose;
                    break;
                }
        }
    }

    protected virtual void resetlabels()
    {
        onDialogWindowClose();
        switchHintLabels();
        switchPreSubstateHints();
    }

    protected virtual void onDialogWindowClose()
    {
        deactivateLoadingPopUp();
        setInteracteble(true);
    }

    /*===================================================================================================
	* 
	* END OF BLOCK
	* 
	* =================================================================================================*/
}