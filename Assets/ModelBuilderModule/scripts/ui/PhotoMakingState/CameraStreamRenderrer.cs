﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class CameraStreamRenderrer : MonoBehaviour
{
	//Camera stream destination
	[SerializeField]
	protected RawImage image;

	[SerializeField]
	protected AspectRatioFitter imageFitter;

	// Device cameras
	protected WebCamDevice frontCameraDevice;
	protected WebCamDevice backCameraDevice;
	protected WebCamDevice activeCameraDevice;

	protected WebCamTexture frontCameraTexture;
	protected WebCamTexture backCameraTexture;
	protected WebCamTexture activeCameraTexture;

	//Size of an image which will be sent on the server
	protected  int resWidth = Screen.width; 
	protected  int resHeight = Screen.height;

	//As we make screenshot in the LateUpdate fuction we have to wait for this fuction and save flag for screenshot making
	protected  bool takeHiResShot = false;

	protected Vector3 mFrontCameraScale = new Vector3 (1.0f, -1.0f, 1.0f);
	protected Vector3 mBackCameraScale = new Vector3 (1.0f, 1.0f, 1.0f);

	public delegate void ScreenCreationCallback(Texture2D tex);
	ScreenCreationCallback mCreateTextureCallback = null;

	/*===============================================================================================
    * 
    * MonoBehaviuor implementation
    * 
    * =============================================================================================*/

	void Awake()
	{
		// Check for device cameras
		if (WebCamTexture.devices.Length == 0)
		{
			Debug.Log("No devices cameras found");
			return;
		}

		// Get the device's cameras and create WebCamTextures with them
		frontCameraDevice = WebCamTexture.devices.Last();
		backCameraDevice = WebCamTexture.devices.First();

		//Camera textures
		frontCameraTexture = new WebCamTexture(frontCameraDevice.name, resWidth, resHeight * 2);
		backCameraTexture = new WebCamTexture(backCameraDevice.name, resWidth, resHeight * 2);

		// Set camera filter modes for a smoother looking image
		frontCameraTexture.filterMode = FilterMode.Trilinear;
		backCameraTexture.filterMode = FilterMode.Trilinear;

		// Set the camera to use by default
		SetActiveCamera(backCameraTexture);
	}

	void Update()
	{
		// Skip making adjustment for incorrect camera data
		if (activeCameraTexture.width < 100)
		{
			Debug.Log("Still waiting another frame for correct info...");
			return;
		}

		// change as user rotates iPhone or Android:

		int cwNeeded = activeCameraTexture.videoRotationAngle;
		// Unity helpfully returns the _clockwise_ twist needed
		// guess nobody at Unity noticed their product works in counterclockwise:
		int ccwNeeded = -cwNeeded;

		// IF the image needs to be mirrored, it seems that it
		// ALSO needs to be spun. Strange: but true.
		if ( activeCameraTexture.videoVerticallyMirrored ) ccwNeeded += 180;

		// you'll be using a UI RawImage, so simply spin the RectTransform
		image.transform.localEulerAngles = new Vector3(0f,0f,ccwNeeded);

		float videoRatio = (float)activeCameraTexture.width/(float)activeCameraTexture.height;

		// you'll be using an AspectRatioFitter on the Image, so simply set it
		imageFitter.aspectRatio = videoRatio;

		// alert, the ONLY way to mirror a RAW image, is, the uvRect.
		// changing the scale is completely broken.
		if ( activeCameraTexture.videoVerticallyMirrored )
		{
			image.uvRect = new Rect(1,0,-1,1);  // means flip on vertical axis
		}
		else
		{
			image.uvRect = new Rect(0,0,1,1);  // means no flip
		}

		if (activeCameraDevice.Equals (frontCameraDevice)) 
		{
			image.transform.localScale = mFrontCameraScale;
		}
		else 
		{
			image.transform.localScale = mBackCameraScale;
		}
	}

	void LateUpdate()
	{
		if (takeHiResShot)
		{
			Texture2D tex = makePhoto();

			if (null != mCreateTextureCallback)
				mCreateTextureCallback (tex);
		}
	}

	void OnDisable()
	{
		if (null != activeCameraTexture) 
		{
			activeCameraTexture.Stop ();
		}
	}

	void OnDestroy()
	{
		if (null != activeCameraTexture) 
		{
			activeCameraTexture.Stop ();
		}
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* OPENED INTERFACE
	* 
	* =============================================================================================*/

	public void setEnabled(bool isEnabled)
	{
		if (true == isEnabled) 
		{
			activeCameraTexture.Play ();
		} 
		else 
		{
			if(activeCameraTexture)
				activeCameraTexture.Stop ();
		}
	}

	public bool isTwoCameras()
	{
		return  !GameObject.Equals (frontCameraDevice, backCameraDevice);
	}

	public void createPhoto(ScreenCreationCallback callback)
	{
		mCreateTextureCallback = callback;
		takeHiResShot = true;
	}

	// Switch between the device's front and back camera
	public void SwitchCamera()
	{
		SetActiveCamera(activeCameraTexture.Equals(frontCameraTexture) ? 
			backCameraTexture : frontCameraTexture);
	}

	//returns image angle, depends on device and used camera 
	public float getImageAngle()
	{
		if ((Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.Android && backCameraDevice.Equals(activeCameraDevice)))
		{
			return 90.0f;
		}
		else if (Application.platform == RuntimePlatform.Android && frontCameraDevice.Equals(activeCameraDevice))
		{
			return 270.0f;
		}
		else
		{
			return 90.0f;
		}
	}

	public void resetActiveCamera()
	{
		if (true == activeCameraTexture.Equals (frontCameraTexture)) 
			SetActiveCamera (backCameraTexture);
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/


	/*===============================================================================================
	* 
	* INTERNAL LOGIC
	* 
	* =============================================================================================*/
	private Texture2D makePhoto()
	{
		takeHiResShot = false;
		Texture2D photo = new Texture2D(activeCameraTexture.width, activeCameraTexture.height);
		photo.SetPixels(activeCameraTexture.GetPixels());
		photo.Apply();

		return photo;
	}
		
	private void SetActiveCamera(WebCamTexture cameraToUse)
	{
		if (activeCameraTexture != null)
			activeCameraTexture.Stop();

		activeCameraTexture = cameraToUse;
		activeCameraDevice = WebCamTexture.devices.FirstOrDefault(device => 
			device.name == cameraToUse.deviceName);

		image.texture = activeCameraTexture;
		image.material.mainTexture = activeCameraTexture;

		activeCameraTexture.Play();
	}

	/*===============================================================================================
    * 
    *ENDOF BLOCK
    * 
    * =============================================================================================*/
}
