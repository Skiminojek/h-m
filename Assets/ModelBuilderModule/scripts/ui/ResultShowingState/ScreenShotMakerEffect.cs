﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenShotMakerEffect : MonoBehaviour 
{
	[SerializeField]
	private Image mBack;

	[SerializeField]
	private float mOneWayFadeTime = 0.1f;

	public void begin()
	{
		gameObject.SetActive (true);
        mBack.CrossFadeAlpha(0.0f, 0.0f, true);
        Invoke ("fadeIn", 0.05f);
	}

	public float getEffectTime()
	{
		return mOneWayFadeTime * 2;
	}

	private void fadeIn()
	{
		mBack.CrossFadeAlpha (1.0f, mOneWayFadeTime, true);
		Invoke ("fadeOut", mOneWayFadeTime);
	}

	private void fadeOut()
	{
		mBack.CrossFadeAlpha (0.0f, mOneWayFadeTime, true);
		Invoke ("endEffect", mOneWayFadeTime);
	}

	private void endEffect()
	{
		gameObject.SetActive (false);
	}
}
