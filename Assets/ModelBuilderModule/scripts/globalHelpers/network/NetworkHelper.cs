﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;
using System;
using System.Threading;

public class NetworkHelper : MonoBehaviour
{
	private static Action<bool> mCallback;

	private static Thread mCheckConnectionThread;

	private static NetworkHelper mInstance;

    //Check for internet connection before send request to the server
	public static void checkConnection(Action<bool> callback, float waitingTime = 10.0f)
    {
		if (null == mInstance)
		{
			GameObject singleton = new GameObject ();
			singleton.name = "ConnectionChecker";
			mInstance = singleton.AddComponent<NetworkHelper> ();
		}

		mCallback = callback;
		mInstance.StartCoroutine (mInstance.startThread(waitingTime));
    }

	private IEnumerator startThread(float waitingTime)
	{
		bool isComplete = false;
		bool isConnectionExist = false;

		if (null != mCheckConnectionThread && true == mCheckConnectionThread.IsAlive) 
			mCheckConnectionThread.Abort ();

		mCheckConnectionThread = new Thread(()=>realThreadStart(ref isComplete, ref isConnectionExist, (int)(waitingTime * 1000)));
		mCheckConnectionThread.Start ();

		while (false == isComplete)
			yield return null;

		mCallback (isConnectionExist);
	}

	void realThreadStart(ref bool isCompleted, ref bool isConnectionExist, int waitingTime)
	{
		Debug.Log ("[NetworkHelper] CheckConnection");
		string resource = "http://server.3dlook.me";
		bool isException = false;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        req.Timeout = waitingTime;

		try
		{
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse()) {}
		}
		catch(Exception ex)
		{
            Debug.Log ("[NetworkHelper] check connection failed, Exception text - " + ex.ToString());
			isCompleted = true;
			isConnectionExist = false;
			isException = true;
		}

		if(false == isException)
		{
            Debug.Log ("[NetworkHelper] check connection success");
			isCompleted = true;
			isConnectionExist = true; 
		}
	}
}
