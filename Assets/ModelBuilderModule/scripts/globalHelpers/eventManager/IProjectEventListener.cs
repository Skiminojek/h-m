﻿using UnityEngine;
using System.Collections;

public interface IProjectEventListener
{
	void onEvent(ProjectEventManager.eEventType eventType, IProjectEventSender sender, object[] parameters, int paramsCunt);
}
