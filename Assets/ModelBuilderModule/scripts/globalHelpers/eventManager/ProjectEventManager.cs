﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectEventManager
{
	public enum eEventType
	{
		E_MAIN_FLOW_STATE_CHANGED,
		E_MAIN_FLOW_STATE_RESET
	}

	private static ProjectEventManager mIstance = null;
	private Dictionary<eEventType, List<IProjectEventListener>> mListeners = new Dictionary<eEventType, List<IProjectEventListener>>();

	private ProjectEventManager()
	{
	}


	/*===================================================================================================
     * 
     * Opened interface
     * 
     * =================================================================================================*/

	public static ProjectEventManager getInstance()
	{
		if (null == mIstance) 
		{
			mIstance = new ProjectEventManager ();
		}

		return mIstance;
	}

	public void addListener (eEventType eventType, IProjectEventListener listener)
	{
		List<IProjectEventListener> listenList = null;

		if (mListeners.TryGetValue (eventType, out listenList)) 
		{
			listenList.Add (listener);
			return;
		}

		listenList = new List<IProjectEventListener> ();
		listenList.Add (listener);
		mListeners.Add (eventType, listenList);
	}

	public void postNotification(eEventType eventType, IProjectEventSender sender, object[] parameters = null, int paramsCount = 0)
	{
		List<IProjectEventListener> listenList = null;

		if (!mListeners.TryGetValue (eventType, out listenList))
			return;

		for (int i = 0; i < listenList.Count; ++i) 
		{
			if (!listenList [i].Equals (null)) 
			{
				listenList [i].onEvent (eventType, sender, parameters, paramsCount);
			}
		}
	}

	public void removeEvent(eEventType type)
	{
		mListeners.Remove (type);
        removeUnnecessaryEntries();
	}

	public void removeUnnecessaryEntries()
	{
		Dictionary<eEventType, List<IProjectEventListener>> tmpListeners = new Dictionary<eEventType, List<IProjectEventListener>> ();

		foreach (KeyValuePair<eEventType, List<IProjectEventListener>> item in mListeners) 
		{
			for (int i = item.Value.Count - 1; i >= 0; --i) 
			{
				if (item.Value [i].Equals (null)) 
				{
					item.Value.RemoveAt (i);
				}
			}

			if (item.Value.Count > 0) 
			{
				tmpListeners.Add (item.Key, item.Value);
			}
		}

		mListeners = tmpListeners;
	}

	/*===================================================================================================
     * 
     * END OF BLOCK
     * 
     * =================================================================================================*/
}
