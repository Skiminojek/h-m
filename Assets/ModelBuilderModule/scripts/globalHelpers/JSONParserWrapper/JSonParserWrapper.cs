﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JSonParserWrapper : MonoBehaviour 
{
	private const string STATUS_ATTRIBUTE = "status";
	public const string IMAGE_HASH_ATTRIBUTE = "name";
	public const string IMAGE_FACE_ADDRESS_ATTRIB = "object";

	public static bool getStatusResult(string JSONString)
	{
		JSONObject obj = new JSONObject (JSONString);
		JSONObject status = obj.GetField (STATUS_ATTRIBUTE);

		if (null != status) 
		{
			if (JSONObject.Type.BOOL == status.type)
			{
				return status.b;
			} 
			else 
			{
				Debug.LogError("[JSonModelParser] status is not a bool");
			}
		} 
		else 
		{
			Debug.LogError("[JSonModelParser] string doesn;t contain filed status");
		}
		return false;
	}

	public static string getImageHash(string jsonString)
	{
		JSONObject json = new JSONObject(jsonString);
		JSONObject nameJson = json.GetField (IMAGE_HASH_ATTRIBUTE);

		if (null != nameJson) 
		{
			if (JSONObject.Type.STRING == nameJson.type) 
			{
				return nameJson.str;
			} 
			else
			{
				Debug.LogError (string.Format( "[JSonModelParser] {0} is not a string", IMAGE_HASH_ATTRIBUTE));
			}
		} 
		else 
		{
			Debug.LogError ("[JSonModelParser] Can't find field name " + IMAGE_HASH_ATTRIBUTE);
		}

		return "";
	}

	public static string getModelFBXAddress(string JSONString)
	{
        JSONObject obj = new JSONObject(JSONString);
        JSONObject bodyURL = obj.GetField("body_object");

        if (null != bodyURL)
        {
            if (JSONObject.Type.STRING == bodyURL.type)
            {
                return bodyURL.str;
            }
            else
            {
                Debug.LogError("[JSonModelParser] body URL is not a string");
            }
        }
        else
        {
            Debug.LogError("[JSonModelParser] string doesn't contain body attribute");
        }

        return "";

    }

	public static string getFaceImageAddress(string JSONString)
	{
		JSONObject obj = new JSONObject (JSONString);
		JSONObject faceURL = obj.GetField (IMAGE_FACE_ADDRESS_ATTRIB);

		if (null != faceURL) 
		{
			if (JSONObject.Type.STRING == faceURL.type)
			{
				return faceURL.str;
			} 
			else 
			{
				Debug.LogError("[JSonModelParser] face URL is not a string");
			}
		} 
		else 
		{
			Debug.LogError("[JSonModelParser] string doesn't contain face attribute");
		}

		return "";
	}
}
