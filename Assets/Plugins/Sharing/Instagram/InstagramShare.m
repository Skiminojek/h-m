#import "InstagramShare.h"
#import <AssetsLibrary/AssetsLibrary.h>

void _handshake()
{
    [[InstagramShare sharedInstance] handshake];
}

bool _isInstalled()
{
    [[InstagramShare sharedInstance] isInstalled];
}

void _postToInstagram(const char * message, const char * imagePath)
{
    NSString *m = [NSString stringWithUTF8String:message];
    NSString *i = [NSString stringWithUTF8String:imagePath];
    [[InstagramShare sharedInstance] postToInstagram:m WithImage:i];
}

void _postToInstagramVideo(const char * message, const char * videoPath)
{
    NSString *m = [NSString stringWithUTF8String:message];
    NSString *v = [NSString stringWithUTF8String:videoPath];
    [[InstagramShare sharedInstance] postToInstagram:m WithImage:v];
}

@implementation InstagramShare

@synthesize dic;

static InstagramShare *sharedInstance = nil;
+(InstagramShare*)sharedInstance {
    if( !sharedInstance )
        sharedInstance = [[InstagramShare alloc] init];
    
    return sharedInstance;
}


-(id)init
{
    if (self = [super init])
    {
        nativeWindow = [UIApplication sharedApplication].keyWindow;
    }
    
    return self;
}

-(void)handshake
{
    NSLog(@"Handshake completed!");
}

-(bool)isInstalled
{
    NSURL *appURL = [NSURL URLWithString:@"instagram://app"];
    return [[UIApplication sharedApplication] canOpenURL:appURL];
}

-(void)postToInstagram:(NSString*)message WithImage:(NSString*)imagePath;
{
    NSURL *appURL = [NSURL URLWithString:@"instagram://app"];
    if([[UIApplication sharedApplication] canOpenURL:appURL])
    {
        /*// Image
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        
        // Post
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[self photoFilePath] atomically:YES];
        NSURL *fileURL = [NSURL fileURLWithPath:[self photoFilePath]];
        
        self.dic = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        self.dic.UTI = @"com.instagram.exclusivegram";
        self.dic.delegate = self;
        if (message)
            self.dic.annotation = [NSDictionary dictionaryWithObject:message forKey:@"InstagramCaption"];
        
        [self.dic presentOpenInMenuFromRect:CGRectZero inView:nativeWindow.rootViewController.view animated:YES];*/
        
        
         // Image
         UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
         
         // Post
         [UIImageJPEGRepresentation(image, 1.0) writeToFile:[self photoFilePath] atomically:YES];
         NSURL *fileURL = [NSURL fileURLWithPath:[self photoFilePath]];
         NSString *caption = message;
         
         ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
         [library writeImageToSavedPhotosAlbum:image.CGImage metadata:nil completionBlock:^(NSURL *assetURL, NSError *error)
         {

         NSString *escapedString   = [self URLEncodedString:assetURL.absoluteString];
         NSString *escapedCaption  = [self URLEncodedString:caption];
             
         NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",escapedString,escapedCaption]];
         [[UIApplication sharedApplication] openURL:instagramURL];
         }];
        
    }
    else
    {
        NSLog(@"Instagram not installed!");
    }
}

-(void)postToInstagramVideo:(NSString*)message WithImage:(NSString*)videoPath;
{
    NSURL *appURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:appURL])
    {
        NSURL *videoFilePath = [NSURL URLWithString:videoPath]; // Your local path to the video
        NSString *caption = message;
        NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",[videoFilePath absoluteString],caption]];
        
        NSLog(@"RESULT URL = %@", instagramURL.absoluteString);
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
        {
            NSLog(@"CAN OPEN URL");
            [[UIApplication sharedApplication] openURL:instagramURL];
        }
    }
    else
    {
        NSLog(@"Instagram not installed!");
    }
}

-(NSString*)photoFilePath
{
    return [NSString stringWithFormat:@"%@/%@",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"], @"tempinstgramphoto.igo"];
}

-(UIDocumentInteractionController*)setupControllerWithURL:(NSURL*)fileURL usingDelegate:(id<UIDocumentInteractionControllerDelegate>)interactionDelegate
{
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (NSString *) URLEncodedString: (NSString *)str {
    NSMutableString * output = [NSMutableString string];
    const char * source = [str UTF8String];
    int sourceLen = strlen(source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = (const unsigned char)source[i];
        if (false && thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
