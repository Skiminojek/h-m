#import <Foundation/Foundation.h>

void _handshake();
void _postToInstagram(const char *, const char *);
void _postToInstagramVideo(const char * message, const char * videoPath);

@interface InstagramShare : NSObject <UIDocumentInteractionControllerDelegate>
{
    UIWindow *nativeWindow;
}

@property (nonatomic, retain) UIDocumentInteractionController *dic;

+(InstagramShare*)sharedInstance;

-(void)handshake;
-(void)postToInstagram:(NSString*)message WithImage:(NSString*)imagePath;
-(void)postToInstagramVideo:(NSString*)message WithImage:(NSString*)videoPath;
-(bool)isInstalled;
-(NSString *)URLEncodedString:(NSString*)path;
@end
