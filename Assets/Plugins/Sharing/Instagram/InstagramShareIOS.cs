﻿using UnityEngine;
using System.Runtime.InteropServices;

public class InstagramShareIOS
{
    static string imagePath = Application.temporaryCachePath + "/temp.png";

    static bool HasHandshook = false;

    [DllImport("__Internal")]
    static extern void _handshake();

    public static void HandShake() 
    {
        _handshake();
        HasHandshook = true;
    }

    [DllImport ("__Internal")]
    static extern void _postToInstagram (string message, string imagePath);

	[DllImport ("__Internal")]
	static extern void _postToInstagramVideo (string message, string imagePath);

	[DllImport ("__Internal")]
	static extern bool _isInstalled ();

	public static bool isInstalled()
	{
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            return _isInstalled ();
        }

        return false;
	}

	public static void PostToInstagramPicture(string message, string imagePath)
    {
        if(!HasHandshook)
            HandShake();

        _postToInstagram(message, imagePath);
    }

	public static void PostToInstagramVideo(string message, string videoPath)
	{
		if(!HasHandshook)
			HandShake();

		Debug.Log (string.Format ("[InstagramShareIOS] PostToInstagramVideo, message - {0}, VIDEO PATH - ", message, videoPath));
		_postToInstagramVideo(message, videoPath);
	}
}