#import <Foundation/Foundation.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>

void _openFBShareDilaog(const char *, const char *);
void _openFBShareDialogVideo(const char*);
bool _isFBInstalled();

@interface FBShareDialog : NSObject <FBSDKSharingDelegate>
{
    UIWindow *nativeWindow;
}

@property (nonatomic, retain) UIDocumentInteractionController *dic;

+(FBShareDialog*)sharedInstance;

-(void)openFBShareDilaog:(NSString*)imagePath :(NSString*)url;
-(void)openFBShareDialogVideo:(NSString*)videoPath;


@end
