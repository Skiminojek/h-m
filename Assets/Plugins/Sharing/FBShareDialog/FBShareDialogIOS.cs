﻿using UnityEngine;
using System.Runtime.InteropServices;

public class FBShareDialog
{
    [DllImport ("__Internal")]
	static extern void _openFBShareDilaog (string imagePath, string url);

	[DllImport ("__Internal")]
	static extern void _openFBShareDialogVideo (string imagePath);

	[DllImport ("__Internal")]
	static extern bool _isFBInstalled ();

	public static void OpenFBShareDilaog(string imagePath, string url = "")
    {
		Debug.Log ("_openFBShareDilaog, imagePath = " + imagePath);
		url = "https://itunes.apple.com/app/id1224533573";
		_openFBShareDilaog (imagePath, url);
    }

	public static void OpenFBShareDilaogVideo(string videoPath)
	{
		Debug.Log ("_openFBSharingDialogVideo, video path = " + videoPath);
		_openFBShareDialogVideo (videoPath);
	}

	public static bool IsFBInstalled()
	{
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            return _isFBInstalled ();
        }

        return false;
	}
}