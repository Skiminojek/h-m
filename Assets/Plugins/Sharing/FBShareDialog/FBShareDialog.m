#import "FBShareDialog.h"
#import <FBSDKShareKit/FBSDKSharePhotoContent.h>
#import <FBSDKShareKit/FBSDKShareVideoContent.h>
#import <FBSDKShareKit/FBSDKSharePhoto.h>
#import <FBSDKShareKit/FBSDKShareVideo.h>

void _openFBShareDilaog(const char * rawImagePath, const char * rawUrl)
{
    NSString *imagePath = [NSString stringWithUTF8String:rawImagePath];
    NSString *url = [NSString stringWithUTF8String:rawUrl];
    [[FBShareDialog sharedInstance] openFBShareDilaog: imagePath : url];
}

void _openFBShareDialogVideo(const char * videoPath)
{
    NSString *i = [NSString stringWithUTF8String:videoPath];
    [[FBShareDialog sharedInstance] openFBShareDialogVideo:i];
}

bool _isFBInstalled()
{
    NSURL *appURL = [NSURL URLWithString:@"fbauth2://"];
    return [[UIApplication sharedApplication] canOpenURL:appURL];
}

@implementation FBShareDialog
@synthesize dic;

static FBShareDialog *sharedInstance = nil;
+(FBShareDialog*)sharedInstance {
    if( !sharedInstance )
        sharedInstance = [[FBShareDialog alloc] init];
    
    return sharedInstance;
}


-(id)init
{
    if (self = [super init])
    {
        nativeWindow = [UIApplication sharedApplication].keyWindow;
    }
    
    return self;
}

-(void)openFBShareDilaog:(NSString*)imagePath :(NSString*)url;
{
    NSLog(@"ImagePath = %@", imagePath);
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = image;
    photo.userGenerated = NO;
    
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    content.contentURL = [NSURL URLWithString:url];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]])
    {
        dialog.mode = FBSDKShareDialogModeNative;
    }
    else
    {
        dialog.mode = FBSDKShareDialogModeBrowser;
    }
    
    dialog.shareContent = content;
    dialog.delegate = self;
    UIViewController *viewController = [[UIViewController alloc] init];
    dialog.fromViewController = viewController;
    [dialog show];
}

-(void)openFBShareDialogVideo:(NSString*)videoPath
{
    NSURL *videoURL = [NSURL URLWithString:videoPath];
    
    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
    video.videoURL = videoURL;
    
    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
    content.video = video;
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]])
    {
        dialog.mode = FBSDKShareDialogModeNative;
    }
    else
    {
        dialog.mode = FBSDKShareDialogModeBrowser;
    }
    
    dialog.shareContent = content;
    dialog.delegate = self;
    UIViewController *viewController = [[UIViewController alloc] init];
    dialog.fromViewController = viewController;
    [dialog show];
    
    NSLog(@"OPEN SHARE DIALOG VIDEO COMPLETE");
}

-(void) sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"SHARING ERROR");
    NSLog(@"%@", error.debugDescription);
}

-(void) sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"SHARING CANCELED");
}

-(void) sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"COMPLETE WITH RESULTS");
}

-(NSString*)photoFilePath
{
    return [NSString stringWithFormat:@"%@/%@",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"], @"screenforfb.igo"];
}

@end
