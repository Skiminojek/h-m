﻿Shader "dlook/Cloth_culloff_basic" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}


	_Normvalue ("Normal value", Range(0,2)) = 0.9
	_NormalMap("NormalMap", 2D) = "black" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
		Lighting On
		Tags{ RenderType = Opaque }
		LOD 400
		
			ZTest On
		//	Offset 0,-1
		
		Cull Off


		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;


		sampler2D _NormalMap;

	struct Input {
		float2 uv_MainTex;
		//		float2 uv_BlendMask;
		//		float2 uv_BlendedTexture;

	};

	half _Glossiness;
	half _Metallic;
	half _Normvalue;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;


		o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex)) * _Normvalue;
		o.Albedo = c.rgb;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = 1;
	}
	ENDCG
	}
		//	FallBack "Transparent/Cutout/Diffuse"
}
