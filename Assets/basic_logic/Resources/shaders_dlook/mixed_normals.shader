﻿Shader "dlook/mixed_normals" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_BlendedTexture("BlendedTexture (RGB)", 2D) = "black" {}
	_BlendMask("BlendMask", 2D) = "black" {}
	_NormalMap("NormalMap", 2D) = "black" {}
	_NormalMixedMap("NormalMixedMap", 2D) = "black" {}
	_NormalMixValue("NormalMixValue", Range(0,1)) = 0
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200
		Cull Off

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
	sampler2D _BlendMask;
	sampler2D _BlendedTexture;
	sampler2D _NormalMap;
	sampler2D _NormalMixedMap;

	struct Input {
		float2 uv_MainTex;
		//		float2 uv_BlendMask;
		//		float2 uv_BlendedTexture;

	};

	half _Glossiness;
	half _Metallic;
	half _NormalMixValue;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		fixed4 bm = tex2D(_BlendMask, IN.uv_MainTex);
		fixed4 bt = tex2D(_BlendedTexture, IN.uv_MainTex);
		
		

		fixed4 normMix = tex2D(_NormalMixedMap, IN.uv_MainTex)*_NormalMixValue;
		fixed4 normBase = tex2D(_NormalMap, IN.uv_MainTex)*(1 - _NormalMixValue);
		fixed4 normBlended = normMix + normBase;

		o.Normal = UnpackNormal(normBlended);

		o.Albedo = c.rgb*(1 - bm.r) + bt.rgb*bm.r;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = 1;
	}
	ENDCG
	}
		FallBack "Diffuse"
}