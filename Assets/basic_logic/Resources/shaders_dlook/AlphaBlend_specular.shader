﻿Shader "dlook/AlphaBlend_specular" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}

	
	_MaskTex("AlphaMask", 2D) = "white" {}
	_NormalMap("NormalMap", 2D) = "black" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
	_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
		Lighting On
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Opaque" }
		LOD 400

		ZWrite on
//		Offset -1, -3
		ZTest Less
		
	Blend SrcAlpha OneMinusSrcAlpha

		Cull Back
//		AlphaToMask On

		

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MaskTex;

		sampler2D _NormalMap;

		struct Input {
			float2 uv_MainTex;
		//		float2 uv_BlendMask;
		//		float2 uv_BlendedTexture;

		};
	
	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		float alp = tex2D(_MaskTex, IN.uv_MainTex).r;
		
		//		o.Normal = tex2D(_NormalMap, IN.uv_MainTex);
		o.Albedo = c.rgb;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = alp;
	}
	ENDCG
	}
	//	FallBack "Transparent/Cutout/Diffuse"
}
