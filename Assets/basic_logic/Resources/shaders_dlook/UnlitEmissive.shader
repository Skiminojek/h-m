﻿Shader "dlook/UnlitEmissive"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_UnlitValue("Emission", Range(0,1)) = 0.5
	}
	
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert


	struct Input {
		
		float2 uv_MainTex;
	};

		float _UnlitValue;
		sampler2D _MainTex;

	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = tex2D(_MainTex, IN.uv_MainTex);
		o.Emission = tex2D(_MainTex, IN.uv_MainTex)*_UnlitValue;

	}
	ENDCG
	}

}