﻿{
"PHOTO_STATE_HINT_UP_FRONT" : "STEP 1: FRONT BODY",
"PHOTO_STATE_HINT_UP_PROFILE" : "STEP 2: PROFILE BODY",
"PHOTO_STATE_HINT_UP_FACE" : "TAKE A SELFIE: 1/2",
"PHOTO_STATE_HINT_UP_FRONT_1RAW" : "Your body should remain inside the frame. Try to keep your hand along the body.",
"PHOTO_STATE_HINT_UP_PROFILE_1RAW" : "Your body should remain inside the frame. Try to keep your hand along the body.",
"PHOTO_STATE_HINT_UP_FACE_1RAW" : "Please remove the hair and glasses from your face.",

"PHOTO_STATE_SAVING" : "Saving data",


"NOT_CONNECTION_DIALOG_WINDOW_TITLE" : "NO CONNECTION",
"NOT_CONNECTION_DIALOG_WINDOW_TEXT" : "Stable connection is required.",

"INCORRECT_PHOTO_DIALOG_WINDOW_TITLE" : "INCORRECT PHOTO",
"INCORRECT_PHOTO_DIALOG_WINDOW_TEXT" : "A person should be inside the borders on the screen.",

"NOT_HANDLED_ERROR_MSG_TITLE" : "Attention",
"NOT_HANDLED_ERROR_MSG_TEXT" : "Unexpected error. Please try again.",

"UPLOAD_ERROR_MSG_TITLE" : "Attention",
"UPLOAD_ERROR_MSG_TEXT" : "Server connection error. Please try again",

"CANT_UPLOAD_IMAGE_DIALOG_WINDOW_TITLE" : "ERROR",
"CANT_UPLOAD_IMAGE_DIALOG_WINDOW_TEXT" : "Can't upload the image.",

"LOW_INTERNET_CONNECTION_DIALOG_TITLE" : "Low internet connection speed",
"LOW_INTERNET_CONNECTION_PHOTO_DIALOG_TEXT" : "Continue at slow rate?, Yes. No, go back [to taking pictures].",

"YES_NO_DIALOG_YES_TEXT" : "Yes",
"YES_NO_DIALOG_NO_TEXT" : "No",
"YES_NO_DIALOG_OK_TEXT" : "Ok",
"YES_NO_DIALOG_CANCEL_TEXT" : "Cancel",
"YES_NO_DIALOG_TRY_AGAIN_TEXT" : "Try again",
"YES_NO_DIALOG_UNLOCK_TEXT" : "Unlock",
"YES_NO_DIALOG_MANUALY_TEXT" : "Manually",

"INFO_INPUT_STATE_DESCRIPTION" : "Select",
"INFO_INPUT_STATE_GENDER_TOGGLE_MAN" : "man",
"INFO_INPUT_STATE_GENDER_TOGGLE_WOMAN" : "woman",
"INFO_INPUT_STATE_HEIGHT_PLACEHOLDER" : "enter your height",
"INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_CM_KG" : "cm",
"INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_IN_LBS" : "in",
"INFO_INPUT_STATE_HEIGHT_INPUT_IN" : "in",
"INFO_INPUT_STATE_HEIGHT_INPUT_FT" : "ft",
"INFO_INPUT_STATE_DONE_BUTTON" : "OK",
"INFO_INPUT_STATE_WRONG_HEIGHT_FIELD_TITLE" : "ERROR",
"INFO_INPUT_STATE_WRONG_HEIGHT_FIELD_TEXT" : "Height input is not valid",

"INFO_INPUT_STATE_HINT_DESCRIPTION_ONE" : "Now you will take 2 pictures of yourself with a smartphone camera. Then we will ask about your height and gender - we need them to accurately determine your body shape, measurements and size.",
"INFO_INPUT_STATE_HINT_DESCRIPTION_TWO" : "You can find a lot of useful information about healthy meals and lifestyles in our Help section.",
"INFO_INPUT_STATE_HINT_BUTTON_TEXT" : "NEW STARLOOK",
"INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TITLE" : "Height is out of range",
"INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TEXT" : "It should be between 150 and 250 (in cm) or between 4'11 and 8'2 (in feet and inches)",


"INFO_INPUT_STATE_AGE_OUT_OF_RANGE_TITLE" : "Age is out of range",
"INFO_INPUT_STATE_AGE_OUT_OF_RANGE_TEXT" : "It should be between 13 and 90 years old",

"DESCRIPTION_STATE_FIRST_PAGE_TITLE" : "FIND YOUR STYLE",
"DESCRIPTION_STATE_FIRST_PAGE_DESCRIPTION" : "We help you to solve the problem of choosing the right size and clothing style",
"DESCRIPTION_STATE_SECOND_PAGE_TITLE" : "IT'S SIMPLE",
"DESCRIPTION_STATE_SECOND_PAGE_DESCRIPTION" : "Scan your body with only two photos and find out measurements, clothing size and body shape",
"DESCRIPTION_STATE_THIRD_PAGE_TITLE" : "DRESS LIKE A STAR",
"DESCRIPTION_STATE_THIRD_PAGE_DESCRIPTION" : "Discover the style of celebrities that are similar to your body shape",

"VIDEO_DESCRIPTION_STATE_FULL_SCREEN_BTN_TEXT" : "see fullscreen",
"VIDEO_DESCRIPTION_STATE_SKIP_TEXT" : "Skip",

"PHOTO_STATE_HINT_UP_FRONT_2RAW" : "the frame. When shooting in the",
"PHOTO_STATE_HINT_UP_FRONT_3RAW" : "mirror, one's hands should be",
"PHOTO_STATE_HINT_UP_FRONT_4RAW" : "placed apart from the body.",

"PHOTO_STATE_HINT_UP_PROFILE_2RAW" : "the frame. When shooting in the",
"PHOTO_STATE_HINT_UP_PROFILE_3RAW" : "mirror, one's hands should be",
"PHOTO_STATE_HINT_UP_PROFILE_4RAW" : "placed along the body.",

"PHOTO_STATE_HINT_UP_FACE_2RAW" : "Remove glasses,",
"PHOTO_STATE_HINT_UP_FACE_3RAW" : "Make sure that you have",
"PHOTO_STATE_HINT_UP_FACE_4RAW" : "removed hair from the face",
"PHOTO_STATE_BACK_BUTTON" : "Create a 3D model",

"INFO_INPUT_STATE_HINT_TITLE" : " ",
"INFO_INPUT_STATE_WEIGHT_OUT_OF_RANGE_TITLE" : "Weight is out of range",
"INFO_INPUT_STATE_WEIGHT_OUT_OF_RANGE_TEXT" : "Weight is absolutely out of range",

"PHOTO_PRELOADER_TEXT_LOW" : "YOUR INTERNET CONNECTION IS TOO SLOW. PLEASE, WAIT A BIT.",
"PHOTO_PRELOADER_TEXT_0" : "PROCESSING YOUR PHOTO ...",
"PHOTO_PRELOADER_TEXT_UPLOADING" : "DETERMINING THE BODY SHAPE ...",
"PHOTO_PRELOADER_TEXT_PROCESSING" : "CALCULATING YOUR SIZE ...",
"PHOTO_PRELOADER_TUNRECOGNIZED_TEXT" : "We can't process this photo. Please, try again.",

"UPLOAD_CANCELED_MSG": "Photo uploading was canceled.",


"RESULT_SHOWING_STATE_TITLE" : "OBJ LOADER",

"RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIZE_XS" : "XS",
"RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIZE_S" : "S",
"RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIZE_M" : "M",
"RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIZE_L" : "L",
"RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIZE_XL" : "XL",


"RESULT_SHOWING_STATE_BODYTYPE_SUB_TITLE":"BODY SHAPE",
"RESULT_SHOWING_STATE_SIZE_SUB_TITLE":"SIZE",



"INNER_MENU_CREATEMODEL" : "Start new",



"ABOUT_MENU_TITLE" : "FOR B2B INQUIRIES",
"ABOUT_MENU_DESCRIPTION" : "Thank you for using Starlook! Do you want to have our mobile body scanning and measurement technology inside your mobile app or a website? Please feel free to learn more and leave your inquiry on                       .",
"ABOUT_MENU_DESCRIPTION_ADDON" : "Or feel free to send us your ideas, questions and suggestions via ",
"ABOUT_MENU_SEND_EMAIL_BTN_TEXT" : "e-mail",
"ABOUT_MENU_MAKE_CALL_BTN_TEXT" : "Make a call",
"ABOUT_MENU_LINK_TEXT" : "our website",
"ABOUT_MENU_ADRESS" : "3DLOOK, LLC 981 Mission street San Francisco, CA 94103",
"ABOUT_MENU_TELEPHONE" : " +1 424 288 95 06",

"PARAMS_CORRECTION_STATE_TITLE" : "PHOTO CORRECTION",
"PARAMS_CORRECTION_STATE_SEND_DATA_BTN_TEXT" : "Continue",

}