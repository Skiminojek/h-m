﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class eFlowState : eFlowStateBase
{
	public static readonly eFlowStateBase E_STATE_DESCRIPTION = new eFlowStateBase(3);
	public static readonly eFlowStateBase E_STATE_INFO_INPUT = new eFlowStateBase(5);
	public static readonly eFlowStateBase E_STATE_VIDEO_DESCRIPTION = new eFlowStateBase(6);
	public static readonly eFlowStateBase E_STATE_RESULTS = new eFlowStateBase(7);
	public static readonly eFlowStateBase E_STATE_CLOTHES = new eFlowStateBase(8);
    public static readonly eFlowStateBase E_STATE_CORRECTION = new eFlowStateBase(9);

    public eFlowState(int value) : base(value)
    {
        internalValue = value;
    }
}

public class UIController : UIControllerBasic
{
	public enum eSceneType
	{
		E_SCENE_PARAMETERS_CALCULATING
	}

	private static Dictionary<eFlowStateBase, string> mStatesToSceneMapping;

	/*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/

	protected override void Awake()
    {
        base.Awake();

		mStatesToSceneMapping = new Dictionary<eFlowStateBase, string> ();

		mStatesToSceneMapping.Add (eFlowState.E_STATE_DESCRIPTION, "ParamsCalculatingScene");
        mStatesToSceneMapping.Add (eFlowState.E_STATE_INFO_INPUT, "ParamsCalculatingScene");
		mStatesToSceneMapping.Add (eFlowState.E_STATE_PHOTO_MAKING, "ParamsCalculatingScene");
		mStatesToSceneMapping.Add (eFlowState.E_STATE_VIDEO_DESCRIPTION, "ParamsCalculatingScene");
		mStatesToSceneMapping.Add (eFlowState.E_STATE_RESULTS, "Results");
        mStatesToSceneMapping.Add (eFlowState.E_STATE_CORRECTION, "ParamsCalculatingScene");
        mStatesToSceneMapping.Add (eFlowState.E_STATE_CLOTHES, "ClothesScene");

        int state = (int)AppData.getInstance ().getData<int> (eFlowState.E_SHARED, DataAttributes.UIControllerAttributes.mLastStateAttribute, (int)eFlowState.E_STATE_INFO_INPUT);  //start state
		mCurrentSubState = AppData.getInstance ().getData<int> (eFlowState.E_SHARED, DataAttributes.UIControllerAttributes.mLastSubStateAttribute, 0);

		bool isApplicationWasClosedDirectly = AppData.getInstance ().getData<bool> (eFlowStateBase.E_GLOBAL, "is_on_start_of_application", true);
		bool isAppllicationWasClosedOnBackgroundMode = AppData.getInstance ().getData<bool> (eFlowStateBase.E_GLOBAL, "last_application_focusState", false);

		if (isApplicationWasClosedDirectly || isAppllicationWasClosedOnBackgroundMode) 
		{
			JSONObject data = new JSONObject ();
			data.AddField ("is_on_start_of_application", false);
			data.AddField ("last_application_focusState", false);
			AppData.getInstance ().saveDataGlobal ("", "UIController", data);

			//state = (int)eFlowState.E_STATE_DESCRIPTION;

            mCurrentSubState = 0;
		} 
			
		//state = (int)eFlowState.E_STATE_RESULTS;
		mCurrentState = getFlowStateFromInt (state);

		foreach (var unit in mFlowUnits) 
		{
			unit.init ();
		}
    }

	static public KeyValuePair<int, int> getstartState()
	{
		int startState = (int)eFlowState.E_STATE_DESCRIPTION;
		int substate = 0;
		return new KeyValuePair<int, int>(startState, substate);
	}

	protected override void Start()
	{
		base.Start ();
	}

	protected override object[] packCurrentState()
	{
		object[] parameter = new object[2];
		parameter[0] = mCurrentState;
		parameter[1] = mCurrentSubState;
		return parameter;
	}

	void OnDestroy()
	{
		if(null != mStatesToSceneMapping)
			mStatesToSceneMapping.Clear ();
	}

	public static eFlowStateBase getFlowStateFromInt(int state)
	{
		foreach(var st in mStatesToSceneMapping)
		{
			if ((int)st.Key == state) 
			{
				return st.Key;
			}
		}

		return eFlowStateBase.E_STATE_AUTHORIZATION;
	}

	public int getCurrentSubState()
	{
		return mCurrentSubState;
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/


	/*===============================================================================================
	* 
	* UIControllerBasic implementation
	* 
	* =============================================================================================*/

	public override void setNewFlowState (eFlowStateBase newState, int substate)
	{
		mPreviousState = mCurrentState;
		mCurrentState = newState;

		foreach (var state in mStatesToSceneMapping) 
		{
			if (newState.internalValue == state.Key.internalValue) 
			{
				mCurrentSubState = substate;

				if (mStatesToSceneMapping [state.Key] == SceneManager.GetActiveScene().name) 
				{
					saveState ();
					ProjectEventManager.getInstance ().postNotification (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_CHANGED, this, packCurrentState (), 2);
				} 
				else 
				{
					saveState ();
					SceneManager.LoadScene (mStatesToSceneMapping [state.Key], LoadSceneMode.Single);
				}

				return;
			}
		}

		Debug.LogError ("[UIController] Try to set unmapped state - " + (int)newState);
		base.setNewFlowState (eFlowState.E_STATE_AUTHORIZATION, substate);
	}

	public override void resetCurrentState()
	{
		ProjectEventManager.getInstance ().postNotification (ProjectEventManager.eEventType.E_MAIN_FLOW_STATE_RESET, this, packCurrentState (), 2);
	}

	void saveState()
	{
		JSONObject states = new JSONObject ();
		states.AddField (DataAttributes.UIControllerAttributes.mLastStateAttribute, (int)mCurrentState);
		states.AddField (DataAttributes.UIControllerAttributes.mPreviousStateAttribute, (int)mPreviousState);
		states.AddField (DataAttributes.UIControllerAttributes.mLastSubStateAttribute, mCurrentSubState);

		if ("" != AppData.getInstance ().getCurrentUserID ()) 
		{
			AppData.getInstance ().saveDataShared ("", this.name, states);
		}
	}

    // GET CURRENT STATE/SUBSTATE DATA FOR ANALYTIC
    public string getCurrentState()
    {
        string stateCurrent="";

        foreach (var st in mStatesToSceneMapping)
        {
            if ((int)st.Key == mCurrentState)
            {
                stateCurrent = st.Value;
            }
        }

        stateCurrent += " : " + mCurrentSubState.ToString();
        return stateCurrent;

    }

	public eFlowStateBase getCurrentStateType()
	{
		return mCurrentState;
	}

	/*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/
}
