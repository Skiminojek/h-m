﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class innerMenu : MonoBehaviour {

	[SerializeField]
	Button mClose, mAboutBtn, mBodyShapeBtn, mSizeBtn, mParamsBtn, mLookBtn, mCreateBtn;

	[SerializeField]
	AboutMenu mAbout;

    [SerializeField]
    Text mAboutBtnText, mSizeBtnText, mParamsBtnText, mLookBtnText, mBodyshapeBtnText, mCreateModelBtnText;


    [SerializeField]
	UIController mUIController;

    [SerializeField]
    soundsHandler mSoundHandler;

	private bool mIsInitted = false;

	void OnEnable()
	{
		if(false == mIsInitted)
			init();
	}

	void init ()

	{
        iniTexts();
		iniButtons();
		mIsInitted = true;
	//	gameObject.SetActive (false);
	}

    private void iniTexts()
    {
     //   mTitleText.text = StringsManager.getString("INNER_MENU_TITLE_TEXT");

        mCreateModelBtnText.text = StringsManager.getString("INNER_MENU_CREATEMODEL");
       


    }

	private void iniButtons()
	{
	//	mAboutBtn.onClick.AddListener (onAboutClicked);
		mCreateBtn.onClick.AddListener (onCreateClicked);

		mClose.onClick.AddListener (onClose);


    }




    private void clickSound()
    {
        mSoundHandler.beepClick();
    }

	public void setEnabled(bool isEnabled)
	{
		gameObject.SetActive (isEnabled);

	}

	private void onAboutClicked()
	{
        clickSound();
        mAbout.setEnabled (true);
		setEnabled (false);

	}

	private void onCreateClicked()
	{
        clickSound();
        mUIController.setNewFlowState (eFlowState.E_STATE_INFO_INPUT, 0);
		setEnabled (false);

	}

	private void onClose()
	{
        clickSound();
        setEnabled (false);
	}



}
