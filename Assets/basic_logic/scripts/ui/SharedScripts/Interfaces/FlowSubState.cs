﻿using UnityEngine;
using System.Collections;

public abstract class FlowSubState : MonoBehaviour
{
	protected IFlowState mListener;

	public abstract void setInteracteble (bool isInteracteble);
	public abstract void reset ();
	public virtual void init (){}
	public virtual void setEnabled(bool isEnabled){}

	public virtual void setListener (IFlowState listener)
	{
		mListener = listener;
	}
}
