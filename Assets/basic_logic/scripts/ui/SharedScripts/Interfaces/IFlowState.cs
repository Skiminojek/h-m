﻿using UnityEngine;
using System.Collections;

public interface IFlowState 
{
	void setSubState (int subState);
	void setCollectedData (int subState, string JSONData);
	string getCollectedData (int substate);

	//Should be removed in the future
	void setGlobalState(eFlowStateBase flowState, int substate);
}
