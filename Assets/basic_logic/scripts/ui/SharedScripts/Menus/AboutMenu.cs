﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AboutMenu : MonoBehaviour 
{
    [SerializeField]
    soundsHandler mSoundHandler;

	[SerializeField]
	Button mMenuBtn;

	[SerializeField]
	Text mTitle;

	[SerializeField]
	Text mDescription;

	[SerializeField]
	Button mSendEmailBtn;

	[SerializeField]
	Button mCallBtn;

	[SerializeField]
	Button mLinkBtn;

	[SerializeField]
	Text mAddressText;

	private bool mIsInitted = false;

	void OnEnable()
	{
		if(false == mIsInitted)
			init();
	}

	public void setEnabled(bool isEnabled)
	{
		gameObject.SetActive (isEnabled);
	}

	private void init()
	{
		mSendEmailBtn.onClick.AddListener (onSendEmailBtnClicked);
		mCallBtn.onClick.AddListener (onCallBtnClicked);
		mLinkBtn.onClick.AddListener (onLinkBtnClicked);
		mMenuBtn.onClick.AddListener (onMenuBtnClicked);

		mTitle.text = StringsManager.getString ("ABOUT_MENU_TITLE");
		mDescription.text = StringsManager.getString ("ABOUT_MENU_DESCRIPTION") + "\n" + "\n" + StringsManager.getString ("ABOUT_MENU_DESCRIPTION_ADDON");
		mSendEmailBtn.GetComponentInChildren<Text>().text = StringsManager.getString ("ABOUT_MENU_SEND_EMAIL_BTN_TEXT");
		mCallBtn.GetComponentInChildren<Text>().text = StringsManager.getString ("ABOUT_MENU_TELEPHONE");
		mLinkBtn.GetComponentInChildren<Text>().text = StringsManager.getString ("ABOUT_MENU_LINK_TEXT");
		mAddressText.text = StringsManager.getString ("ABOUT_MENU_ADRESS");
	}

	private void onMenuBtnClicked()
	{
        mSoundHandler.beepClick();
		setEnabled (false);
	}

	private void onSendEmailBtnClicked()
	{
        mSoundHandler.beepClick();
        Application.OpenURL("mailto:hello@3dlook.me");
	}

	private void onCallBtnClicked()
	{
        mSoundHandler.beepClick();
        Application.OpenURL ("tel://+17254654660");
	}

	private void onLinkBtnClicked()
	{
        mSoundHandler.beepClick();
        Application.OpenURL ("https://www.3dlook.me/saia");
	}
}
