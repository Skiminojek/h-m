﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SubstateHintButton : Button {

	// Use this for initialization
	void Start () {

        this.onClick.AddListener(onSubstateHintBtnClick);
	
	}

    void onSubstateHintBtnClick()
    {
        this.transform.parent.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
