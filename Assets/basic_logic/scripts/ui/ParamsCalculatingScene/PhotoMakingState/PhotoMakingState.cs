﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Threading;

public class PhotoMakingState : FlowUnitBasic, IFlowState
{
	[SerializeField]
	UIController mUIController;

	[SerializeField]
	PhotoMakingLogic mPhotoMakingLogic;

	[SerializeField]
	Preloader mPreloader;

	public enum ePhotoMakingSubState
	{
		E_PHOTO_MAKING,
	}

	private Dictionary<ePhotoMakingSubState, FlowSubState> mSubStates;

	/*===============================================================================================
    * 
    * MonoBehaviour iomplementation
    * 
    * =============================================================================================*/

	void Awake()
	{
		mStateType = eFlowState.E_STATE_PHOTO_MAKING;
		mPreloader.hide (0.0f);

		mSubStates = new Dictionary<ePhotoMakingSubState, FlowSubState> ();
		mSubStates.Add(ePhotoMakingSubState.E_PHOTO_MAKING, transform.Find("CameraUsageSubState").GetComponent<FlowSubState>());


		foreach (var substate in mSubStates) 
			substate.Value.setListener (this);
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* FlowUnitBasic iomplementation
	* 
	* =============================================================================================*/
	public override void setEnabled(bool isEnabled, int substate)
	{
		gameObject.SetActive (isEnabled);

		if (true == isEnabled) 
		{
			reset ();
			setSubState (substate);
		}
	}

	public override void reset()
	{
		saveManualParamsInputState (false);
		setSubState ((int)ePhotoMakingSubState.E_PHOTO_MAKING);
		mSubStates [ePhotoMakingSubState.E_PHOTO_MAKING].reset ();
		mPreloader.hide (0.0f);

		JSONObject json = new JSONObject ();


		Dictionary<int, JSONObject> data = new Dictionary<int, JSONObject> ();
		data.Add ((int)eFlowState.E_STATE_PHOTO_MAKING, json);

		AppData.getInstance ().saveData (eFlowState.E_STATE_PHOTO_MAKING, data);
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/


	/*===============================================================================================
	* 
	* IPhotoMakingState iomplementation
	* 
	* =============================================================================================*/
	public void setSubState (int subState)
	{
         mPhotoMakingLogic.setEnabled(true, subState);

        foreach (var sstate in mSubStates) 
		{
			if ((int)sstate.Key == subState) 
				sstate.Value.setEnabled (true);
			else 
				sstate.Value.setEnabled (false);
		}
	}

	public void setCollectedData(int stateType, string JSONInfo)
	{
		switch ((ePhotoMakingSubState)stateType) 
		{
	
		case ePhotoMakingSubState.E_PHOTO_MAKING:
			{
				savePhotoCase (stateType, JSONInfo);
				break;
			}
		default:
			{
				Debug.LogError ("[PhotoMakingSubState] Got unhandeled substate - " + stateType);
				break;
			}
		}
	}

	public string getCollectedData(int state)
	{
		return "";
	}

	public void setGlobalState(eFlowStateBase flow, int substate)
	{
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal Logic
	* 
	* =============================================================================================*/

	private void saveCustomCase(int stateType, string JSONInfo)
	{
		JSONObject jo = new JSONObject (JSONInfo);

		Dictionary<int, JSONObject> data = new Dictionary<int, JSONObject> ();
		data.Add (stateType, jo);

		AppData.getInstance ().saveData (eFlowState.E_STATE_PHOTO_MAKING, data);
		setSubState ((int)ePhotoMakingSubState.E_PHOTO_MAKING);

        mPhotoMakingLogic.startCompleteonLogic ();
	}

	private void savePhotoCase(int stateType, string JSONInfo)
	{
		Dictionary<int, JSONObject> photoData = new Dictionary<int, JSONObject> ();
		JSONObject jo = new JSONObject (JSONInfo);


		photoData.Add (stateType, jo);
		AppData.getInstance ().saveData (eFlowState.E_STATE_PHOTO_MAKING, photoData);


			mUIController.setNewFlowState (eFlowState.E_STATE_RESULTS, (int)ResultsState.eSubState.E_RESULTS_PREVIEW);

	}

	private void saveManualParamsInputState(bool isManual)
	{

	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}