﻿using UnityEngine;
using UnityEngine.UI;
using StansAssets.Animation;

public class Preloader : MonoBehaviour 
{

    [SerializeField]
    Image mBack, mStar, mStarTransparent;

    [SerializeField]
    Text mText;

    Material mClipRectMaterial;

    float mLoadingTime;
    float mStartTime;

    private void Awake()
    {
        mClipRectMaterial = mStar.material;
    }

    public void setText(string text)
    {
        mText.text = text;
    }

    public void show(float loadingTime, string text)
    {
        gameObject.SetActive(true);

        mClipRectMaterial.SetFloat("_Y", 0.0f);
        mText.text = text;
        mLoadingTime = loadingTime;
        mStartTime = Time.time;

        iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "onupdatetarget", gameObject, "onupdate", "updateFromValue", "time", loadingTime, "easetype", iTween.EaseType.linear, "oncomplete", "onComplete"));
    }

    public void hide(float time)
    {
        gameObject.SetActive(false);
    }

    private void updateFromValue(float newValue)
    {
        mClipRectMaterial.SetFloat("_Y", newValue);
    }

    void onComplete()
    {

    }
}
