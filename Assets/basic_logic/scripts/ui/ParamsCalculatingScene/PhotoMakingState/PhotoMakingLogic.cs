﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class PhotoMakingLogic : PhotoMakingStateBasic
{
    [SerializeField]
    soundsHandler mSoundHandler;

	[SerializeField]
	List<GameObject> mPreSubstateHints;

	[SerializeField]
	Image mBlurImage;

	[SerializeField]
	Text mModelCreationText;

	[SerializeField]
	Preloader mPreloader;

    [SerializeField]
    ScreenShotMakerEffect mScreenShotMakingEffect;

    [SerializeField]
    Button mCancelUploadButton;

    [SerializeField]
    Image mCurrentPhotoImage;

	[SerializeField]
	GameObject mCurrentPhotoBack;

    [SerializeField]
    Image mCurrentContourImage;

    [SerializeField]
    GameObject mCounturObjectsContainer;

    [SerializeField]
    Button mContourNextStepBtn;

    [SerializeField]
    Button mFromGalleryPhotoBtn;

    bool mIsNextBtnClicked = false;
 

    [SerializeField]
    Button mCountourRetakePhoto;

    [SerializeField]
    Image mGyroHintText;

    [SerializeField]
    Image mGyroLevelerAll;

    [SerializeField]
    Image mGyroLevelerBall, mGyroBallMax, mGyroBallMin;

    private bool isAPIv2 = true; // set the flow method
    private int mCurrentStepAPI2 = 1;


    private IFlowState mPhotoMakingState;

	private Dictionary<ePhotoSubstate, GameObject> mPreSubstatesHintsMap;

	private Material mBlurMaterial;



    /*===============================================================================================
    * 
    * MonoBehaviour iomplementation
    * 
    * =============================================================================================*/

    protected override void Awake()
	{
		base.Awake();

        mContourNextStepBtn.onClick.AddListener(onNextBtnClicked);
        mCountourRetakePhoto.onClick.AddListener(onRetakePhotoClicked);
        mCancelUploadButton.onClick.AddListener(OnCancelUploadButtonClicked);
        mCancelUploadButton.gameObject.SetActive(false);
        mCurrentPhotoImage.gameObject.SetActive(false);
		mCurrentPhotoBack.SetActive (false);
        mCounturObjectsContainer.gameObject.SetActive(false);
        //added
        mContourNextStepBtn.gameObject.SetActive(false);
        mCountourRetakePhoto.gameObject.SetActive(false);
        //
        if (isAPIv2)
        {
            APICoordinator.getInstance().setNotifierText(mPreloader.GetComponentInChildren<Text>());
            List<GameObject> postPhoto = new List<GameObject>(); postPhoto.Add(mContourNextStepBtn.gameObject); postPhoto.Add(mCountourRetakePhoto.gameObject);
            APICoordinator.getInstance().setInPhotoHintsObjects(mHints);
            APICoordinator.getInstance().setSubstateHintsObjects(mPreSubstateHints);
            APICoordinator.getInstance().setPostPhotoObjects(postPhoto);
            APICoordinator.getInstance().setTestTextures(mTestTextures); //set test photos

            StartCoroutine(setHintsPatch());
        }

        mFromGalleryPhotoBtn.onClick.AddListener(onGalleryBtn);

     //   mDataHandlerFabrics.Add(InputParams.eExpectedResult.E_BODY, new BodyHandlerFabric());

		//PreHints init
		mPreSubstatesHintsMap = new Dictionary<ePhotoSubstate, GameObject>();
		mPreSubstatesHintsMap.Add(ePhotoSubstate.E_SUBSTATE_FRONT, mPreSubstateHints[0]);
		mPreSubstatesHintsMap.Add(ePhotoSubstate.E_SUBSTATE_PROFILE, mPreSubstateHints[1]);


	
        switchPreSubstateHints();

		mBlurMaterial = Resources.Load("BlurMaterial/blur") as Material;
		setBlurEnabled (false);
		initStrings ();
        GyroReset(); //resetting gyro every 5 secs

        
    }

    IEnumerator setHintsPatch() { yield return new WaitForSeconds(.04f); APICoordinator.getInstance().switchHints(0); }

    /*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/

    /*===============================================================================================
	* 
	* Opened intrface
	* 
	* =============================================================================================*/
    // CHECK PLUGIN GALLERY USAGE

    void onGalleryBtn()
    {
        PickImage(1024);
    }
    private void PickImage(int maxSize)
    {
        Debug.Log("start picking method");

        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create Texture from selected image

                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize, false, true, false);

                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }

                JSONObject savePath = new JSONObject();
                savePath.AddField(DataAttributes.Shared.mCurrentPhotoFromGallery, path);
                AppData.getInstance().saveDataShared("", "galleryPhoto", savePath);
                onPhotoButtonClicked();

            }

        }, "Select photo", "image/png", maxSize);

        Debug.Log("Permission result: " + permission);
    }


    // PLUGIN METHOD END

    // SOME PATCH FOR GALLERY BTN




    public void setFlowStateInterface(IFlowState flowState)
	{
		mPhotoMakingState = flowState;
	}

	public void startCompleteonLogic()
	{
	//	mDataHandler.startCompleteonLogic ();
	}

	public void updateVisualState()
	{
        if (isAPIv2)
        {


            switchHintLabels();
            switchPreSubstateHints();
            mModelCreationText.gameObject.SetActive(false);
            mCancelUploadButton.gameObject.SetActive(false);
            mCurrentPhotoImage.gameObject.SetActive(false);
            mCounturObjectsContainer.gameObject.SetActive(false);
            mCurrentPhotoBack.SetActive(false);
            mContourNextStepBtn.gameObject.SetActive(false); //API2
            mCountourRetakePhoto.gameObject.SetActive(false); //API2
        }
        else
        {
            switchHintLabels();
            switchPreSubstateHints();
            mModelCreationText.gameObject.SetActive(false);
            mCancelUploadButton.gameObject.SetActive(false);
            mCurrentPhotoImage.gameObject.SetActive(false);
            mCounturObjectsContainer.gameObject.SetActive(false);
            mContourNextStepBtn.gameObject.SetActive(false); //API2
            mCountourRetakePhoto.gameObject.SetActive(false); //API2
            mCurrentPhotoBack.SetActive(false);
        }


    }

    protected  override void switchHintLabels()
    {

        foreach (var hint in mMapedHints)
            hint.Value.gameObject.SetActive(false);


        int curStep = APICoordinator.getInstance().getStep();

        PhotoMakingStateBasic.ePhotoSubstate current = (curStep == 0) ? PhotoMakingStateBasic.ePhotoSubstate.E_SUBSTATE_FRONT : PhotoMakingStateBasic.ePhotoSubstate.E_SUBSTATE_PROFILE;

            if (true == mMapedHints.ContainsKey(current))
              mMapedHints[current].gameObject.SetActive(true);

        mCancelUploadButton.gameObject.SetActive(false);

    }


    public override void setEnabled(bool isEnabled, int substate)
	{
        gameObject.SetActive(isEnabled);
        setInteracteble(isEnabled);

        if (true == isEnabled)
        {
            deactivateLoadingPopUp();
            switchHintLabels();
            APICoordinator.getInstance().setStep(0);
        }
        if (false == isEnabled) { reset(); updateVisualState(); switchPreSubstateHints(); }
        mCameraRenderrer.setEnabled(isEnabled);
        mCurrentStepAPI2 = 1;
        mModelCreationText.gameObject.SetActive (false);
	//	mDataHandler.reset ();
        mCancelUploadButton.gameObject.SetActive(false);
        mCurrentPhotoImage.gameObject.SetActive(false);
		mCurrentPhotoBack.SetActive (false);

        //	mCameraRenderrer.resetActiveCamera ();
    }

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* PhotoMakingStateBasic implementation
	* 
	* =============================================================================================*/

	protected override void onDialogWindowClose()
	{
        //base.onDialogWindowClose ();
        deactivateLoadingPopUp();
        setInteracteble(true);

        setBlurEnabled (false);
        mPreloader.hide(0.0f);

        //fix bug
        mCurrentPhotoImage.gameObject.SetActive(false);
        mModelCreationText.gameObject.SetActive(false);
        //

        //   reset();
        //   resetSubState();
        //
    }

	protected override void onPhotoButtonClicked()
	{
        if (isAPIv2)
        {
            mIsNextBtnClicked = false;
           

           mCameraRenderrer.createPhoto((tex) =>
            {
                Texture2D currentPhoto = APICoordinator.getInstance().passScreen(tex);
                APICoordinator.getInstance().savePhotoFromStep(currentPhoto, mCurrentStepAPI2);
                
                mCurrentPhotoImage.gameObject.SetActive(true);
                APICoordinator.getInstance().setPhotoTexture(mCurrentPhotoImage, APICoordinator.getInstance().getCurrentUserPhoto());
                mCurrentPhotoBack.SetActive(true);
                createPhotoEffect();
                deactivateLoadingPopUp();
                //    
                APICoordinator.getInstance().switchPostPhotoObj(true);
                mPhotoButton.gameObject.SetActive(false);
             //   APICoordinator.getInstance()
            });

            
        }
        else
        {
      //      mIsNextBtnClicked = false;
      //      base.onPhotoButtonClicked();
      //      deactivateLoadingPopUp();
        }

    }



	public override void reset()
	{
        mCurrentStepAPI2 = 1;
        APICoordinator.getInstance().setStep(0);
		setInteracteble (true);
		mCameraRenderrer.resetActiveCamera ();
		mBackButton.gameObject.SetActive (true);
		mBackButton.interactable = true;

		deactivateLoadingPopUp ();
		updateVisualState ();

		setBlurEnabled (false);

        switchHintLabels();
        mPhotoButton.gameObject.SetActive(true);
        setInteracteble(true);

    //    base.reset ();


    }

	protected override void onBackButtonClicked()
	{
        mSoundHandler.beepClick();

		//fix
		DataUploader.removeAllUploaders ();
		reset ();
		resetSubState ();

	//	mDataHandler.reset ();
		//

		mUIControllerBasic.setNewFlowState (eFlowState.E_STATE_INFO_INPUT, 0);
	}
		

	protected override void activateLoadingPopUp()
	{
        setInteracteble(false);
        mPhotoButton.gameObject.SetActive(false);
        mLoadingImage.gameObject.SetActive(true);
        mCameraSwapButton.gameObject.SetActive(false);

        if (!isAPIv2)
        {
            mModelCreationText.gameObject.SetActive(true);
            mModelCreationText.text = StringsManager.getString("PHOTO_PRELOADER_TEXT_0");
        }
	}

	private void changeText()
	{
		mModelCreationText.text = StringsManager.getString ("PHOTO_PRELOADER_TEXT_LOW");
	}

    private void createPhotoEffect()
    {
        setBlurEnabled(true);
        Invoke("createPhotoSoundEffect", mScreenShotMakingEffect.getEffectTime() / 2);
        mScreenShotMakingEffect.begin();
        activateLoadingPopUp();
    }

    private void createPhotoSoundEffect()
    {
        mSoundHandler.beepShutter();
    }

	private void setPhotoTexture()
	{

        int curStep = APICoordinator.getInstance().getStep();

        PhotoMakingStateBasic.ePhotoSubstate current = (curStep == 0) ? PhotoMakingStateBasic.ePhotoSubstate.E_SUBSTATE_FRONT : PhotoMakingStateBasic.ePhotoSubstate.E_SUBSTATE_PROFILE;

        Texture2D imageTex = TexturesUtilities.getPhotoByStage ((int)current);
		Sprite fromCam = null;

		mCancelUploadButton.gameObject.SetActive(true);

		if (null != imageTex)
		{
			fromCam = Sprite.Create(imageTex, new Rect(0, 0, imageTex.width, imageTex.height), new Vector2(0.5F, 0.5F));
			mCurrentPhotoImage.sprite = fromCam;
			mCurrentPhotoImage.gameObject.SetActive(true);
			mCurrentPhotoBack.SetActive(true);
            mCurrentPhotoImage.GetComponent<AspectRatioFitter>().aspectRatio = mCurrentPhotoImage.sprite.texture.width / (float)mCurrentPhotoImage.sprite.texture.height;

            mCurrentContourImage.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
            mCurrentContourImage.GetComponent<RectTransform>().sizeDelta = new Vector2(fromCam.texture.width, fromCam.texture.height);
            mCurrentContourImage.GetComponent<AspectRatioFitter>().aspectRatio = (float)fromCam.texture.width / fromCam.texture.height;
            mCurrentContourImage.sprite = fromCam;
        }
	}


    protected override void deactivateLoadingPopUp()
	{
        mLoadingImage.gameObject.SetActive(false);
        mPhotoButton.gameObject.SetActive(true);

        //	base.deactivateLoadingPopUp ();
    }

	private void saveParams(JSONObject CVServerParsedResult)
	{
		saveDataCallback (CVServerParsedResult);
	}

	private void saveDataCallback(JSONObject collectedData)
	{
		if(null != collectedData)
		{
			mPhotoMakingState.setCollectedData ((int)PhotoMakingState.ePhotoMakingSubState.E_PHOTO_MAKING, collectedData.ToString());
		}
		else
		{
			MessageDialogsManager.createNotRecognitionDialogWindow (()=>{reset();});
		}
	}

	private void processStepState(ePhotoSubstate currentSubState)
	{
            resetSubState();
    }

    protected void showContourElements()
    {
    }

    void connectionCallback(bool isConnectionExist)
    {
        if (!isConnectionExist)
        {
            Debug.Log("no connection");
            new MobileNativeMessage(StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TITLE"), StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TEXT"));
        }
        else
        {
            
        }
    }
    void callbackFromAPI(bool iswithouterror, string result) //right now for test
    {
        
    //    Debug.Log("CALLBACK ANSWER " + result);
        if (!iswithouterror) {/* Debug.Log("errors : " + !iswithouterror); */ resetAPI2(); }
        else
        {
            mPreloader.gameObject.SetActive(true);
            mPreloader.GetComponentInChildren<Text>().text = "Success";
            mPreloader.show(5f, "Success");
            Debug.Log("SAVED DATA " + AppData.getInstance().getData<JSONObject>(eFlowState.E_SHARED, DataAttributes.Shared.mAPIv2CompleteAnswerJSON, null, true).ToString());
            mUIControllerBasic.setNewFlowState(eFlowState.E_STATE_RESULTS, 0); // GOTO RESULT SCENE 
        }
    }

    void resetAPI2()
    {
        APICoordinator.getInstance().setStep(0);
        StartCoroutine(setHintsPatch());
        mPreloader.hide(0.2f);      
        updateVisualState();

    }

    protected void onNextBtnClicked()
    {
        if (isAPIv2)
        {

            if (mCurrentStepAPI2==2) //final state
            {
                //     NetworkHelper.checkConnection((bool isConnected) =>
                {
                    //          if (!isConnected)
                    {
                        //           new MobileNativeMessage(StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TITLE"), StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TEXT")); return;
                    }
                    //       else
                    {
                        APICoordinator.getInstance().sendData(callbackFromAPI);  //START TO SEND METADATA, PHOTOS AND WAIT FOR RESPONSE
                        mPreloader.gameObject.SetActive(true);

                        APICoordinator.getInstance().setStep(mCurrentStepAPI2);
                        APICoordinator.getInstance().switchHints(mCurrentStepAPI2);
                        APICoordinator.getInstance().switchPostPhotoObj(false);
                        mCurrentStepAPI2++;
                        updateVisualState();
                        setBlurEnabled(false);
                        mPhotoButton.gameObject.SetActive(true);
                        mPhotoButton.interactable = true;
                        mIsNextBtnClicked = true;
                        mPreloader.show(7f, "");

                        //
                    }
                }
          //      });


            }
            else
            {
                //IF NOT FINAL BLOCK
                APICoordinator.getInstance().setStep(mCurrentStepAPI2);
                APICoordinator.getInstance().switchHints(mCurrentStepAPI2);
                APICoordinator.getInstance().switchPostPhotoObj(false);
                mCurrentStepAPI2++;
                updateVisualState();
                setBlurEnabled(false);
                mPhotoButton.gameObject.SetActive(true);
                mPhotoButton.interactable = true;
                mIsNextBtnClicked = true;
            }
        }
        else
        {
            // API v1
        
        }
    }

    protected void onRetakePhotoClicked()
    {
        mCounturObjectsContainer.gameObject.SetActive(false);
        resetSubState();
        mContourNextStepBtn.gameObject.SetActive(false);
        mCountourRetakePhoto.gameObject.SetActive(false);

    }

    protected void resetSubState()
    {
        mCounturObjectsContainer.gameObject.SetActive(false);
        updateVisualState();
        setInteracteble(true);
        setBlurEnabled(false);
        mPhotoButton.interactable = true;
        deactivateLoadingPopUp();
    }



    private void processErrorFlow(eErrorID errorID, ePhotoSubstate currentSubState, string msg = "")
	{
		switch (errorID)
		{
		case eErrorID.E_ERROR_NOT_RECOGNIZED:
			{
				processNotRecognizedError (currentSubState, msg);
                    AnalyticHandler.sendEvent(AnalyticHandler.aTopicErrors, new Dictionary<string, object> {
                        { AnalyticHandler.aPhotoError, AnalyticHandler.aPhotoErrorCantRecognize + " at substate " + currentSubState.ToString()  }
                    });
				break;
			}
		case eErrorID.E_ERROR_NO_CONNECTION:
			{
				MessageDialogsManager.createNotConnectionDialogWindow(()=>{onDialogWindowClose(); OnCancelUploadButtonClicked();});
				break;
			}
		case eErrorID.E_ERROR_IMAGE_UPLOADING:
			{
				MobileNativeMessage error = new MobileNativeMessage (StringsManager.getString("CANT_UPLOAD_IMAGE_DIALOG_WINDOW_TITLE"), StringsManager.getString("CANT_UPLOAD_IMAGE_DIALOG_WINDOW_TEXT"));

                    AnalyticHandler.sendEvent(AnalyticHandler.aTopicErrors, new Dictionary<string, object> {
                        { AnalyticHandler.aPhotoError, AnalyticHandler.aPhotoErrorErrorWhileUpload + " at substate " + currentSubState.ToString()  }
                    });

                    error.OnComplete += resetlabels;
				break;
			}
		case eErrorID.E_ERROR_UNHANDLED:
		default:
			{
				MessageDialogsManager.createUnhandeledErrorDialogWindow(()=>{onDialogWindowClose(); reset();});

                    AnalyticHandler.sendEvent(AnalyticHandler.aTopicErrors, new Dictionary<string, object> {
                        { AnalyticHandler.aPhotoError, AnalyticHandler.aPhotoErrorUnhandled + " at substate " + currentSubState.ToString()  }
                    });

				MessageDialogsManager.createUnhandeledErrorDialogWindow(onDialogWindowClose);

                    break;
			}
		}
	}

    private void processNotRecognizedError(ePhotoSubstate currentSubState, string msg = "")
	{
        //WE DIDNT USE THIS METHOD IN API v2
		onDialogWindowClose ();
        //show message
        mModelCreationText.gameObject.SetActive(true);

        mModelCreationText.text = StringsManager.getString ("PHOTO_PRELOADER_TUNRECOGNIZED_TEXT");
	//	mPhotoButton.gameObject.SetActive (false);

		// createCUstomMenuDialog (msg);
		//setBlurEnabled (false);
	}

	private void createCUstomMenuDialog(string msg = "")
	{
		MessageDialogsManager.createUseCustomParamsInput((result)=>
			{
				onDialogWindowClose();
				if(MNDialogResult.YES == result)
					openCustomMenu();
			}, msg);
	}

	protected override void switchPreSubstateHints()
	{
		foreach (var hint in mPreSubstateHints)
			hint.gameObject.SetActive(false);

        int curStep = APICoordinator.getInstance().getStep();

        PhotoMakingStateBasic.ePhotoSubstate current = (curStep == 0) ? PhotoMakingStateBasic.ePhotoSubstate.E_SUBSTATE_FRONT : PhotoMakingStateBasic.ePhotoSubstate.E_SUBSTATE_PROFILE;

        mPreSubstateHints[curStep].SetActive(true);


        if (true == mMapedHints.ContainsKey(current))
            mMapedHints[current].gameObject.SetActive(true);
		else
			mPreSubstateHints[0].SetActive(true);
	}

	private void setBlurEnabled(bool isEnabled)
	{
		mBlurImage.gameObject.SetActive (isEnabled);
		mBlurImage.material = (true == isEnabled) ? mBlurMaterial : null;
	}



	private void openCustomMenu()
	{

	}

	private void initStrings()
	{
		mMapedHints [ePhotoSubstate.E_SUBSTATE_FRONT].GetComponentInChildren<Text> ().text = StringsManager.getString ("PHOTO_STATE_HINT_UP_FRONT");
		mMapedHints [ePhotoSubstate.E_SUBSTATE_PROFILE].GetComponentInChildren<Text> ().text = StringsManager.getString ("PHOTO_STATE_HINT_UP_PROFILE");
		mMapedHints [ePhotoSubstate.E_SUBSTATE_FACE].GetComponentInChildren<Text> ().text = StringsManager.getString ("PHOTO_STATE_HINT_UP_FACE");
	}

    private void removeLastUploadPhotoRequest()
    {
        DataUploader.removeAllUploaders();
    }

    private void OnCancelUploadButtonClicked()
    {
		TexturesUtilities.textureFlush(mCurrentPhotoImage);
        removeLastUploadPhotoRequest();


		mCancelUploadButton.gameObject.SetActive(false);
		mCurrentPhotoImage.gameObject.SetActive(false);
		mCurrentPhotoBack.SetActive (false);
		mModelCreationText.gameObject.SetActive (false);
		mPhotoButton.gameObject.SetActive (true);
        resetSubState();
    }


    //GYRO LOGIC ADDON - CHECK FOR AXIS YOU NEED - roll or pitch.

    bool isPhoneInCorrectAngle()
    {
        float mAllowableAngleError = 7;

        Quaternion referenceRotation = Quaternion.identity;
        Quaternion deviceRotation = DeviceRotation.Get();
        Quaternion eliminationOfXY = Quaternion.Inverse(Quaternion.FromToRotation(referenceRotation * Vector3.forward, deviceRotation * Vector3.forward));
        Quaternion eliminationOfYZ = Quaternion.Inverse(Quaternion.FromToRotation(referenceRotation * Vector3.left, deviceRotation * Vector3.left));
        Quaternion rotationZ = eliminationOfXY * deviceRotation;
        Quaternion rotationX = eliminationOfYZ * deviceRotation;
        //float rollZ = rotationZ.eulerAngles.z;
        float mGyroscopeAngle = rotationX.eulerAngles.x - 10;
        //showing ANGLE

        return (mGyroscopeAngle > 360.0f - mAllowableAngleError || mGyroscopeAngle < mAllowableAngleError) && (mGyroscopeAngle > 360.0f - mAllowableAngleError || mGyroscopeAngle < mAllowableAngleError);
    }

    float getPhoneAngle()
    {
        Quaternion referenceRotation = Quaternion.identity;
        Quaternion deviceRotation = DeviceRotation.Get();
        //Quaternion eliminationOfXY = Quaternion.Inverse(Quaternion.FromToRotation(referenceRotation * Vector3.forward, deviceRotation * Vector3.forward));
        Quaternion eliminationOfYZ = Quaternion.Inverse(Quaternion.FromToRotation(referenceRotation * Vector3.left, deviceRotation * Vector3.left));
        //Quaternion rotationZ = eliminationOfXY * deviceRotation;
        Quaternion rotationX = eliminationOfYZ * deviceRotation;
        //float rollZ = rotationZ.eulerAngles.z;
        //   return  rotationX.eulerAngles.x - 10;
        return rotationX.eulerAngles.x;
    }

    private void GyroReset()
    {

        DeviceRotation.IniReset();
        //	Debug.Log ("Angle is " + mGyroscopeAngle.ToString("F2"));
        Invoke("GyroReset", 3f);
    }

    private void Update()
    {

        mFromGalleryPhotoBtn.gameObject.SetActive(mPhotoButton.gameObject.activeSelf);
        mFromGalleryPhotoBtn.interactable = mPhotoButton.interactable;

        if (true == mLoadingImage.gameObject.activeSelf)
            mLoadingImage.gameObject.transform.Rotate(Vector3.forward, -Time.deltaTime * 400);


        //check only if photo button is active and interactable
        if ((mPhotoButton.IsInteractable()) && (mPhotoButton.isActiveAndEnabled))
        {
            mGyroLevelerAll.gameObject.SetActive(true);
            float angle = getPhoneAngle();
            if (angle > 180) angle = 0 - (360 - angle);
            //    mGyroHintText.GetComponentInChildren<Text>().text = angle.ToString() + "   " + getPhoneAngle();
            float positionBall = Mathf.Clamp(angle, -25, 25);  // 7 degrees delta and -10 correction for vertical position
            float percentage = 0.5f + (positionBall) / 50.0f;
            mGyroLevelerBall.GetComponent<RectTransform>().localPosition = Vector2.Lerp(mGyroBallMin.GetComponent<RectTransform>().localPosition, mGyroBallMax.GetComponent<RectTransform>().localPosition, percentage);

            if (isPhoneInCorrectAngle())
            {
                //       mPhotoButton.GetComponent<Image>().color = Color.white;
                mPhotoButton.GetComponent<Image>().raycastTarget = true;
                mGyroHintText.gameObject.SetActive(false);
            }
            else
            {

                //       mPhotoButton.GetComponent<Image>().color = new Color(1, 0, 0, .3f);
                mPhotoButton.GetComponent<Image>().raycastTarget = false;
                mGyroHintText.gameObject.SetActive(true);

            }
        }
        else
        //hide all it
        {
            mGyroHintText.gameObject.SetActive(false);
            mGyroLevelerAll.gameObject.SetActive(false);

        }

    }

    //


    //

}
