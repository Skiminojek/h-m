﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CameraUsageSubState : FlowSubState
{

	[SerializeField]
	PhotoMakingLogic mPhotoMakingLogic;

	/*===============================================================================================
	* 
	* FlowSubState implementation
	* 
	* =============================================================================================*/
	public override void setInteracteble(bool isInteracteble)
	{
	}

	public override void reset()
	{
		mPhotoMakingLogic.reset ();
	}

	public override void setEnabled(bool isActive)
	{
		mPhotoMakingLogic.setFlowStateInterface (mListener);
		gameObject.SetActive (isActive);

		if (true == isActive)
			mPhotoMakingLogic.updateVisualState ();
	}
	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/
}
