﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class BodyResponseData : MonoBehaviour 
{
	public JSONObject mTexturesData;
	public JSONObject mUserParamsData;
}
