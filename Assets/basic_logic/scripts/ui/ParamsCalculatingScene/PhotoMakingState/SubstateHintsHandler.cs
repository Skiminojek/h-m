﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SubstateHintsHandler : MonoBehaviour {

    [SerializeField]
    Text mFrontHintHeader;

    [SerializeField]
    Text mProfileHintHeader;

    [SerializeField]
    Text mFaceHintHeader;

    [SerializeField]
    List<Text> mFrontHints;

    [SerializeField]
    List<Text> mProfileHints;

    [SerializeField]
    List<Text> mFaceHints;

    // Use this for initialization
    void Start () {

        // GET STRINGS FOR HINT TEXTS

        mFrontHintHeader.text = StringsManager.getString("PHOTO_STATE_HINT_UP_FRONT");
        mProfileHintHeader.text = StringsManager.getString("PHOTO_STATE_HINT_UP_PROFILE");
        mFaceHintHeader.text = StringsManager.getString("PHOTO_STATE_HINT_UP_FACE");

        // Descriptions part processing

        for(int i=0;i<1;i++)
        
        {
            int j = i + 1; //there, in strings, we starts from 1, not from zero
            string stringFront = "PHOTO_STATE_HINT_UP_FRONT_" + j.ToString() + "RAW";
            string stringProfile = "PHOTO_STATE_HINT_UP_PROFILE_" + j.ToString() + "RAW";
            string stringFace = "PHOTO_STATE_HINT_UP_FACE_" + j.ToString() + "RAW";
            mFrontHints[i].text = StringsManager.getString(stringFront);
            mProfileHints[i].text = StringsManager.getString(stringProfile);
            mFaceHints[i].text = StringsManager.getString(stringFace);

        }
        //DONE
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
