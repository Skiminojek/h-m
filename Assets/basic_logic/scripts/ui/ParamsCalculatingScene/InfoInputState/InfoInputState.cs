﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InfoInputState : FlowUnitBasic, IFlowState
{
    [SerializeField]
    soundsHandler mSoundHandler;

	[SerializeField]
	Text mHintText;

	[SerializeField]
	InputField mHeightInputFieldCM, mHeightInputFieldFT, mHeightInputFieldIN;

	[SerializeField]
	Text mHeightInputDescription;

	[SerializeField]
	Button mDoneButton;

	//================================Gender toggle=========================================
	[SerializeField]
    MyToggleGroup mGenderToggleGroup;

	[SerializeField]
	Toggle mMenToggleButton;

	[SerializeField]
	Toggle mWomanToggleButton;
	//=====================================================================================

	//================================Measurements toggle==================================
	//[SerializeField]
    //MyToggleGroup mMeasurmentsToggleGroup;

	[SerializeField]
	Toggle mMeasurementsToggle;

	[SerializeField]
	Text mLeftLAbel, mRightLabel;

    //[SerializeField]
    //Toggle m_CM_KG_Toggle;

    //[SerializeField]
    //Toggle m_IN_IBS_Toggle;
    //=====================================================================================

    public enum eGenderType {E_MALE, E_FEMALE };


    eGenderType mGenderType;

    public enum eMeasurementsType { E_MEASUREMENTS_IN_LBS, E_MEASUREMENTS_CM_KG };
    eMeasurementsType mMeasurementsType;

    private Toggle mLastActiveGenderToggle;
    //private Toggle mLastActiveMeasureToggle;
	private bool mLastMeasureToggleState;

//	private InputParams.eGenderType mGenderType;
//	private InputParamsFitness.eMeasurementsType mMeasurementsType;

	private Color mBaseMeasurementsColor = new Color(0.582f, 0.582f, 0.582f);
	private Color mActiveMeasurementsColor = new Color(0.543f, 0.492f, 0.726f);

	[SerializeField]
	UIController mUIController;

    private float mUserHeightCM, mUserWeightKG;
    private float mMinHeight, mMaxHeight;

    private string mPrevHeightString;

	void Awake()
	{
		mStateType = eFlowState.E_STATE_INFO_INPUT;

		mMeasurementsToggle.onValueChanged.AddListener (onMeasurementsToggleChanged);
		mLastMeasureToggleState = mMeasurementsToggle.isOn;
        //mMeasurmentsToggleGroup.OnChange += OnMeasurementsToggleChanged;
        mGenderToggleGroup.OnChange += onGenderToggleChanged;
		mDoneButton.onClick.AddListener (OnDoneButtonClicked);

        //Listener height field
		mHeightInputFieldCM.onValidateInput += delegate (string input, int charIndex, char addedChar) { return heightValidate(charIndex, addedChar); };
		mHeightInputFieldCM.onValueChanged.AddListener(onHeightValueChanged);


    }

	void OnDestroy()
	{
		//mMeasurmentsToggleGroup.OnChange -= OnMeasurementsToggleChanged;
		mGenderToggleGroup.OnChange -= onGenderToggleChanged;
	}

	void OnEnable()
	{
		mHeightInputFieldCM.text = "";
	}

	// Use this for initialization
	void Start () 
	{
		initTextsFields ();

        // BORDERS FOR PARAMS
        mMinHeight = 150;
        mMaxHeight = 250;
	}

	public override void setEnabled (bool isenabled, int substate)
	{
		gameObject.SetActive (isenabled);

		mMenToggleButton.interactable = isenabled;
		mWomanToggleButton.interactable = isenabled;
		mMeasurementsToggle.interactable = isenabled;
		mHeightInputFieldCM.interactable = isenabled;

		if (true == isenabled) 
		{
			GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;

			string measurements = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.Shared.mMeasurementsAttribute, DataAttributes.Shared.mMeasurements_IN_LBS);
			mMeasurementsToggle.isOn = DataAttributes.Shared.mMeasurements_IN_LBS == measurements;

			string gender = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, DataAttributes.Shared.mGenderFemale);
			mGenderType = (gender == DataAttributes.Shared.mGenderMale) ? eGenderType.E_MALE : eGenderType.E_FEMALE;
			mWomanToggleButton.isOn = eGenderType.E_MALE != mGenderType;
			mMenToggleButton.isOn = eGenderType.E_MALE == mGenderType;

			mHeightInputFieldCM.gameObject.SetActive (!mMeasurementsToggle.isOn);
			mHeightInputFieldFT.gameObject.SetActive (mMeasurementsToggle.isOn);
			mHeightInputFieldIN.gameObject.SetActive (mMeasurementsToggle.isOn);
		}

		if (true == isenabled) 
		{
			setSubState (substate);
		}

		mLastMeasureToggleState = mMeasurementsToggle.isOn;
		mLeftLAbel.color = mLastMeasureToggleState ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mRightLabel.color = !mLastMeasureToggleState ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mMeasurementsType = mMeasurementsToggle.isOn ? eMeasurementsType.E_MEASUREMENTS_IN_LBS : eMeasurementsType.E_MEASUREMENTS_CM_KG;
	}

	public override void reset()
	{
	}

	private void initTextsFields()
	{
		mHintText.text = StringsManager.getString ("INFO_INPUT_STATE_DESCRIPTION");

		mMenToggleButton.GetComponentInChildren<Text> ().text = StringsManager.getString ("INFO_INPUT_STATE_GENDER_TOGGLE_MAN");
		mWomanToggleButton.GetComponentInChildren<Text> ().text = StringsManager.getString ("INFO_INPUT_STATE_GENDER_TOGGLE_WOMAN");

		mLeftLAbel.text = StringsManager.getString ("INFO_INPUT_STATE_HEIGHT_INPUT_IN");
		mRightLabel.text = StringsManager.getString ("INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_CM_KG");

		mDoneButton.GetComponentInChildren<Text> ().text = StringsManager.getString ("INFO_INPUT_STATE_DONE_BUTTON");

		mHeightInputDescription.text = StringsManager.getString ("INFO_INPUT_STATE_HEIGHT_PLACEHOLDER");
		mHeightInputFieldCM.placeholder.GetComponent<Text>().text = StringsManager.getString("INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_CM_KG");
		mHeightInputFieldFT.placeholder.GetComponent<Text>().text = StringsManager.getString("INFO_INPUT_STATE_HEIGHT_INPUT_FT");
		mHeightInputFieldIN.placeholder.GetComponent<Text>().text = StringsManager.getString("INFO_INPUT_STATE_HEIGHT_INPUT_IN");

		mMenToggleButton.GetComponentInChildren<Text> ().color = mMenToggleButton.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mWomanToggleButton.GetComponentInChildren<Text> ().color = mWomanToggleButton.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;
	}

    private void onGenderToggleChanged(Toggle activeToggle)
    {

        mLastActiveGenderToggle = activeToggle;

		if (activeToggle == mMenToggleButton) 
		{
			mGenderType = eGenderType.E_MALE;
		}
		else
		{
			mGenderType = eGenderType.E_FEMALE;
		}

		mMenToggleButton.GetComponentInChildren<Text> ().color = mMenToggleButton.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mWomanToggleButton.GetComponentInChildren<Text> ().color = mWomanToggleButton.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;
    }

	private void onMeasurementsToggleChanged(bool isOn)
	{

		mLastMeasureToggleState = isOn;
		mMeasurementsType = isOn ? eMeasurementsType.E_MEASUREMENTS_IN_LBS : eMeasurementsType.E_MEASUREMENTS_CM_KG;

		mLeftLAbel.color = mLastMeasureToggleState ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mRightLabel.color = !mLastMeasureToggleState ? mActiveMeasurementsColor : mBaseMeasurementsColor;

		mHeightInputFieldCM.gameObject.SetActive (!isOn);
		mHeightInputFieldFT.gameObject.SetActive (isOn);
		mHeightInputFieldIN.gameObject.SetActive (isOn);

		recalculateParams ();
	}

	private void OnDoneButtonClicked()
	{
        mSoundHandler.beepClick();

        if (false == isStringValid (mUserHeightCM.ToString()))
		{
			new MobileNativeMessage (StringsManager.getString ("INFO_INPUT_STATE_WRONG_HEIGHT_FIELD_TITLE"), StringsManager.getString ("INFO_INPUT_STATE_WRONG_HEIGHT_FIELD_TEXT"));
			return;
		}

        //Checking is exists written params in CM and KG
		if (mMeasurementsType == eMeasurementsType.E_MEASUREMENTS_CM_KG)
        {
			if (mHeightInputFieldCM.text != "")
				mUserHeightCM = float.Parse(mHeightInputFieldCM.text);
             else mUserHeightCM = -1;

        }
        else
        {
			string FTString = mHeightInputFieldFT.text.Replace("'", "");
			string formattedString = string.Format ("{0}.{1}", mHeightInputFieldFT.text.Replace ("'", ""), mHeightInputFieldIN.text.Replace ("'", ""));

			mUserHeightCM = (float)MeasurementsTranslator.FeetsInchesToCM(formattedString);
        }

        // END




        // CHECK ADEQUATE PARAMETERS
        if (!checkAllParams()) return;

		JSONObject collectedData = new JSONObject ();
		collectedData.AddField (DataAttributes.UserParams.mHeightAttribute, mUserHeightCM);
		collectedData.AddField (DataAttributes.FBDataGrabber.mGenderAttribute, (mGenderType == eGenderType.E_MALE) ? DataAttributes.Shared.mGenderMale : DataAttributes.Shared.mGenderFemale);
		collectedData.AddField (DataAttributes.Shared.mMeasurementsAttribute, (mMeasurementsType == eMeasurementsType.E_MEASUREMENTS_CM_KG) ? DataAttributes.Shared.mMeasurements_CM_KG : DataAttributes.Shared.mMeasurements_IN_LBS);

		setCollectedData (0, collectedData.ToString ());

        Debug.Log("[INPUTSTATE] : metrical params is - height :" + mUserHeightCM.ToString() + " weight : " + mUserWeightKG.ToString());

		swapStates ();
    }

    private bool checkAllParams()
    {
        // Checking ranges of fields

        if ((mUserHeightCM < mMinHeight) || (mUserHeightCM > mMaxHeight))
        {
            new MobileNativeMessage(StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TITLE"), StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TEXT"));
            Debug.Log(StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TEXT"));
            return false;
        }
            
        return true;
    }



    private void recalculateParams()
    {
		if (eMeasurementsType.E_MEASUREMENTS_CM_KG == mMeasurementsType) 
		{
			string heightString = mHeightInputFieldFT.text.Length > 0 ? string.Format ("{0}.{1}", mHeightInputFieldFT.text, mHeightInputFieldIN.text) : "";
            
           if (MeasurementsTranslator.FeetsInchesToCM(heightString) !=-1.0)
				mHeightInputFieldCM.text = MeasurementsTranslator.FeetsInchesToCM(heightString).ToString();
            float newHeight = MeasurementsTranslator.FeetsInchesToCM(heightString);
           
            if (((newHeight < mMinHeight)||(newHeight > mMaxHeight))&&(heightString!=""))
            {
                new MobileNativeMessage(StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TITLE"), StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TEXT"));
				mHeightInputFieldCM.text = "";
            }
		}
		else 
		{
			string heightString = mHeightInputFieldCM.transform.Find ("Text").GetComponent<Text> ().text;
           
            if (true == isStringValid(heightString)) 
			{
				float heightValue = float.Parse (heightString);
                //Remember CM 
                mUserHeightCM = heightValue;
                float newHeight = heightValue;

			    heightValue = MeasurementsTranslator.CMToIN (heightValue);

				string FTINString = MeasurementsTranslator.INToFeetsInches (heightValue);
				string[] splitted = FTINString.Split ('\'');
				mHeightInputFieldFT.text = string.Format("{0}", splitted[0]);
				mHeightInputFieldIN.text = string.Format("{0}", splitted[1]);

                if (((newHeight < mMinHeight) || (newHeight > mMaxHeight)) && (heightString != ""))
                {
                    new MobileNativeMessage(StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TITLE"), StringsManager.getString("INFO_INPUT_STATE_HEIGHT_OUT_OF_RANGE_TEXT"));
					mHeightInputFieldCM.text = "";
                }


            }
		}
    }

	private bool isStringValid(string stringForValidation)
	{
		try
		{
			float.Parse(stringForValidation);
		}
		catch 
		{
			return false;
		}

		return true;
    }

    private void onHeightValueChanged(string inputString)
    {
        // for uniformed styles of empty field even when returning zero value
        float nonvalid = -1.0f;
		if (inputString == nonvalid.ToString()) mHeightInputFieldCM.text = "";
    }

    private char heightValidate(int charIndex, char toValidate)
    {

		if (mMeasurementsType == eMeasurementsType.E_MEASUREMENTS_IN_LBS)
        {

            if ((charIndex == 0) && (!char.IsDigit(toValidate))) return '\0';
			if ((charIndex == 1) && ((toValidate != '\'') && (toValidate != '.') && (toValidate != ','))) return '\0';
            if ((charIndex > 1) && (!char.IsDigit(toValidate))) return '\0';

            return toValidate;
        }
		if (mMeasurementsType == eMeasurementsType.E_MEASUREMENTS_CM_KG)
        {

            if (!char.IsDigit(toValidate))
                return '\0';
            else
                return toValidate;
        }
        return toValidate;

    }

    private void swapStates()
	{
        //ANALYTIC BLOCK
        AnalyticHandler.sendEvent(AnalyticHandler.aTopicUser, new Dictionary<string, object>{
            { "height", mUserHeightCM },
            { "gender", mGenderType.ToString() }
        });
        //
        mMenToggleButton.interactable = false;
		mWomanToggleButton.interactable = false;
		//m_CM_KG_Toggle.interactable = false;
		//m_CM_KG_Toggle.interactable = false;
		mMeasurementsToggle.interactable = false;
		mHeightInputFieldCM.interactable = false;

		setNextFlowState ();
	}

	private void setNextFlowState()
	{
        bool alreadyWatched = AppData.getInstance().getData<bool>(eFlowState.E_SHARED, DataAttributes.Shared.mAlreadyWatchVideoDescription, false, false);
        bool isPlatformNeedsVideo = false;
        if (Application.platform == RuntimePlatform.IPhonePlayer) isPlatformNeedsVideo = true;

        if ((isPlatformNeedsVideo)&&(!alreadyWatched))
		    mUIController.setNewFlowState (eFlowState.E_STATE_VIDEO_DESCRIPTION, 0);
        else
            mUIController.setNewFlowState(eFlowState.E_STATE_PHOTO_MAKING, 0);
    }

	/*===============================================================================================
    * 
    * IFlowState implementation
    * 
    * =============================================================================================*/
	public void setSubState (int subState)
	{
	}

	public void setCollectedData (int subState, string JSONData)
	{
		Dictionary<int, JSONObject> objectsToSave = new Dictionary<int, JSONObject> ();
		objectsToSave.Add (subState, new JSONObject (JSONData));

		AppData.getInstance ().saveData (eFlowState.E_STATE_INFO_INPUT, objectsToSave);
	}

	public string getCollectedData (int substate)
	{
		return "";
	}

	//Should be removed in the future
	public void setGlobalState(eFlowStateBase flowState, int substate)
	{
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/
}
