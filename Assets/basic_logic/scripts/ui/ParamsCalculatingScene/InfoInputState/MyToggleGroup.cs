﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MyToggleGroup : ToggleGroup 
{
    public delegate void ChangedEventHandler(Toggle newActive);

    public event ChangedEventHandler OnChange;

    public void Start() 
    {
        foreach (Transform transformToggle in gameObject.transform) 
        {
            var toggle = transformToggle.gameObject.GetComponent<Toggle>();

			if (null != toggle) 
			{
				toggle.onValueChanged.AddListener((isSelected) =>
					{
						if (!isSelected)
						{
							return;
						}
						var activeToggle = Active();
						DoOnChange(activeToggle);
					});
			}
        }

        DoOnChange(this.Active());
    }
    public Toggle Active() 
    {
        return ActiveToggles().FirstOrDefault();
    }

	public void setInteracteble(bool isInteracteble)
	{
		foreach (Transform transformToggle in gameObject.transform)
		{
			var toggle = transformToggle.gameObject.GetComponent<Toggle> ();
			if (null != toggle)
				toggle.interactable = isInteracteble;
		}
	}

    protected virtual void DoOnChange(Toggle newactive)
    {
        var handler = OnChange;

        if (handler != null)
        {
            handler(newactive);
        }
    }
}
