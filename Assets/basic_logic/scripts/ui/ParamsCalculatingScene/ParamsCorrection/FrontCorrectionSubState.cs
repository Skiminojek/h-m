﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FrontCorrectionSubState : CorrectionSubStateBasic
{

    /*===============================================================================================
    * 
    * Internal logic
    * 
    * =============================================================================================*/
	protected override void initTexts()
    {
        mTitle.text = StringsManager.getString("PARAMS_CORRECTION_STATE_TITLE");
        mSendCorrectedParamsBtn.GetComponentInChildren<Text>().text = StringsManager.getString("PARAMS_CORRECTION_STATE_SEND_DATA_BTN_TEXT");
    }

	protected override void onSendDataBtnClicked()
    {
		JSONObject data = mLandMarkCorrector.extractCorrectedLandmarks ();
		mListener.setCollectedData ((int)ParamsCorrectionState.eSubState.E_FRONT_CORRECTION, data.ToString());
    }

	protected override string getSubstateType()
	{
		return "front";
	}

	protected override string getImagePhotoName()
	{
		return "stage_1.png";
	}

	protected override void onBackButtonClicked()
	{
		base.onBackButtonClicked ();
		mListener.setGlobalState (eFlowState.E_STATE_PHOTO_MAKING, (int)PhotoMakingState.ePhotoMakingSubState.E_PHOTO_MAKING);
	}
    /*===============================================================================================
    * 
    * FlowSubState implementation
    * 
    * =============================================================================================*/
}
