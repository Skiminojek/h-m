﻿using UnityEngine;
using System.Collections;
using StansAssets.Animation;

public class Zoomer : MonoBehaviour
{
	private float scale_factor= 0.1f;
	private float MAXSCALE = 10.0f, MIN_SCALE = 1.0f; // zoom-in and zoom-out limits
	private bool isMousePressed;
	private Vector2 prevDist = new Vector2(0,0);
	private Vector2 curDist = new Vector2(0,0);
	private Vector2 midPoint = new Vector2(0,0);
	private Vector2 ScreenSize;
	private Vector3 originalPos;
	public float mStartScale{ get; set;}
	private bool mIsOnMoveing = false;
	Canvas mCanvas;
	float mMaxVecLength;
	bool mIsInteracteble = true;

	//private GameObject parentObject;
	void Start ()
	{
		mCanvas = GameObject.Find("Canvas").GetComponent<Canvas>(); 
		ScreenSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height));
		originalPos = transform.position;
		MIN_SCALE = GetComponent<RectTransform> ().localScale.x;
		isMousePressed = false;


		Vector2 halfSize = mCanvas.GetComponent<RectTransform> ().sizeDelta / 2;
		mMaxVecLength = Mathf.Sqrt(halfSize.x * halfSize.x + halfSize.y * halfSize.y);

		prevPos = new Vector2 (Screen.width / 2, Screen.height / 2);
	}
	void Update ()
	{
		if (true == mIsOnMoveing || false == mIsInteracteble)
			return;
		
		if(Input.GetMouseButtonDown(0))
			isMousePressed = true;
		else if(Input.GetMouseButtonUp(0))
			isMousePressed = false;
		// These lines of code will pan/drag the object around untill the edge of the image
		if(isMousePressed && Input.touchCount==1 && Input.GetTouch(0).phase == TouchPhase.Moved && (transform.localScale.x > MIN_SCALE || transform.localScale.y > MIN_SCALE))
		{
			Touch touch = Input.GetTouch(0);
			Vector3 diff = touch.deltaPosition;
			Vector3 pos = transform.position + diff;
			transform.position = pos;
		}
		// On double tap image will be set at original position and scale
		else if(Input.touchCount==1 && Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(0).tapCount==2)
		{
			transform.localScale = new Vector3(MIN_SCALE, MIN_SCALE, MIN_SCALE);
			transform.position = new Vector3(originalPos.x*-1, originalPos.y*-1, originalPos.z);
			transform.position = originalPos;
		}
		checkForMultiTouch();
	}
	// Following method check multi touch
	private void checkForMultiTouch()
	{
		// These lines of code will take the distance between two touches and zoom in - zoom out at middle point between them
		if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
		{
			midPoint = getMidPoint ();
			curDist = Input.GetTouch(0).position - Input.GetTouch(1).position; //current distance between finger touches
			prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition)); //difference in previous locations using delta positions
			float touchDelta = curDist.magnitude - prevDist.magnitude;
			// Zoom out
			if(touchDelta>0)
			{
				if(transform.localScale.x < MAXSCALE && transform.localScale.y < MAXSCALE)
				{
					Vector3 scale = new Vector3(transform.localScale.x + scale_factor * transform.localScale.x, transform.localScale.y + scale_factor * transform.localScale.x, 1);
					scale.x = (scale.x > MAXSCALE) ? MAXSCALE : scale.x;
					scale.y = (scale.y > MAXSCALE) ? MAXSCALE : scale.y;
					scaleFromPosition(scale,midPoint, touchDelta);
				}
			}
			//Zoom in
			else if(touchDelta<0)
			{
				if(transform.localScale.x > MIN_SCALE && transform.localScale.y > MIN_SCALE)
				{
					Vector3 scale = new Vector3(transform.localScale.x + scale_factor*-1 * transform.localScale.x, transform.localScale.y + scale_factor*-1 * transform.localScale.x, 1);
					scale.x = (scale.x < MIN_SCALE) ? MIN_SCALE : scale.x;
					scale.y = (scale.y < MIN_SCALE) ? MIN_SCALE : scale.y;
					scaleFromPosition(scale,midPoint, touchDelta);
				}
			}
		}
		// On touch end just check whether image is within screen or not
		else if (Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled || Input.GetTouch(1).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Canceled))
		{
			if(transform.localScale.x == MIN_SCALE || transform.localScale.y == MIN_SCALE)
			{
				transform.position = originalPos;
			}
		}
	}
	//Following method scales the gameobject from particular position
	static Vector3 prevPos = Vector3.zero;
	private void scaleFromPosition(Vector3 scale, Vector3 fromPos, float touchDelta)
	{
		Vector3 direction = fromPos *= -1.0f;
		Vector3 normalized = direction.normalized;
		float length = direction.magnitude / mMaxVecLength;

		Vector3 shift = normalized * (30 * length * scale.x / MIN_SCALE);

		if (touchDelta > 0.0f) 
		{
			transform.position += shift;
		}
		else 
		{
			transform.position -= shift;
		}
		
		Debug.LogError (string.Format("DIFF - {0}, DIR - {1}, POS - {2}", shift, normalized, fromPos)); 

		transform.localScale = scale;
		prevPos = fromPos;
	}

	public bool getIsOnMoveing()
	{
		return mIsOnMoveing;
	}

	public void shiftTo(Vector3 to)
	{
		mIsOnMoveing = true;
		iTween.MoveTo (gameObject, iTween.Hash("x", to.x, "y", to.y, "z", to.z,  "time", 0.125f, "oncomplete", "onMoveComplete", "oncompletetarget", gameObject));
	}

	private Vector2 getMidPoint()
	{
		Vector2 midScreenPoint = new Vector2(((Input.GetTouch(0).position.x + Input.GetTouch(1).position.x)/2), ((Input.GetTouch(0).position.y + Input.GetTouch(1).position.y)/2));
		Vector2 midCanvas;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(mCanvas.transform as RectTransform, midScreenPoint, mCanvas.worldCamera, out midCanvas);
		return midCanvas;
	}

	private void onMoveComplete()
	{
		mIsOnMoveing = false;
	}

	public void setInteracteble(bool isInteracteble)
	{
		mIsInteracteble = isInteracteble;
	}
}