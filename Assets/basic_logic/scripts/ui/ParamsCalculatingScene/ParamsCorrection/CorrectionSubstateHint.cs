﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CorrectionSubstateHint : MonoBehaviour 
{
	[SerializeField]
	Button mCloseButton;

	[SerializeField]
	Text mTitle, mChest, mWaist, mHips, mBottom;

	[SerializeField]
	Image mManImage, mWomanImage;

	private string mType;

	[SerializeField]
	float mManChestPos, mManWaistPos, mManHipsPos, mWomanChestPos, mWomanWaistPos, mWomanHipsPos;

	void Awake()
	{
		mCloseButton.onClick.AddListener (onCloseButtonClicked);
	}

	public void setEnabled(bool isEnabled, string type)
	{
		gameObject.SetActive (isEnabled);

		if (true == isEnabled)
		{
			mType = type;
			initTexts (mType);

			string gender = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, DataAttributes.Shared.mGenderMale);

			mManImage.gameObject.SetActive (gender == DataAttributes.Shared.mGenderMale);
			mWomanImage.gameObject.SetActive (gender != DataAttributes.Shared.mGenderMale);
		}
	}

	public void onCloseButtonClicked()
	{
		setEnabled (false, mType);
	}

	private void initTexts(string state)
	{
		string gender = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, DataAttributes.Shared.mGenderMale);

		switch (state) 
		{
			case "front":
			case "side":
			default:
			{
				mTitle.text = StringsManager.getString ("CORRECTION_HINT_TITLE");
				mChest.text = StringsManager.getString ("CORRECTION_HINT_CHEST");
				mWaist.text = StringsManager.getString ("CORRECTION_HINT_WAIST");
				mHips.text = StringsManager.getString ("CORRECTION_HINT_HIPS");
				mBottom.text = StringsManager.getString ("CORRECTION_HINT_BOTTOM_TEXT");
				mCloseButton.GetComponentInChildren<Text>().text = StringsManager.getString("CORRECTION_HINT_CLOSE_BUTTON_TEXT");

				Vector2 pos = mChest.GetComponent<RectTransform> ().anchoredPosition;
				mChest.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (pos.x, gender == DataAttributes.Shared.mGenderMale ? mManChestPos : mWomanChestPos);

				pos = mWaist.GetComponent<RectTransform> ().anchoredPosition;
				mWaist.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (pos.x, gender == DataAttributes.Shared.mGenderMale ? mManWaistPos : mWomanWaistPos);

				pos = mHips.GetComponent<RectTransform> ().anchoredPosition;
				mHips.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (pos.x, gender == DataAttributes.Shared.mGenderMale ? mManHipsPos : mWomanHipsPos);

				break;
			}
		}
	}
}
