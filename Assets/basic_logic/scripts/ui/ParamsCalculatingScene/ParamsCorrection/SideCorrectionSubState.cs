﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SideCorrectionSubState : CorrectionSubStateBasic
{
    /*===============================================================================================
    * 
    * Internal logic
    * 
    * =============================================================================================*/
	protected override void initTexts()
    {
        mTitle.text = StringsManager.getString("PARAMS_CORRECTION_STATE_TITLE");
        mSendCorrectedParamsBtn.GetComponentInChildren<Text>().text = StringsManager.getString("PARAMS_CORRECTION_STATE_SEND_DATA_BTN_TEXT");
    }

	protected override void onSendDataBtnClicked()
    {
		JSONObject data = mLandMarkCorrector.extractCorrectedLandmarks ();
		mListener.setCollectedData ((int)ParamsCorrectionState.eSubState.E_SIDE_CORRECTION, data.ToString());
    }

	protected override string getSubstateType()
	{
		return "side";
	}

	protected override string getImagePhotoName()
	{
		return "stage_2.png";
	}

	protected override void onBackButtonClicked()
	{
		base.onBackButtonClicked ();
		mListener.setSubState ((int)ParamsCorrectionState.eSubState.E_FRONT_CORRECTION);
	}
    /*===============================================================================================
    * 
    * FlowSubState implementation
    * 
    * =============================================================================================*/
}
