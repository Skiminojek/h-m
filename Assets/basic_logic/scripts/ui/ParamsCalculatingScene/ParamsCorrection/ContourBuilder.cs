﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class ContourBuilder : MonoBehaviour {

    [SerializeField]
    GameObject mRefPoint;

    List<GameObject> mPointsList;
    UILineRenderer mLine;

    float[] mXpoint, mYpoint;

    private void Awake()
    {
        mPointsList = new List<GameObject>();
        mLine = new GameObject().AddComponent<UILineRenderer>();
        mLine.gameObject.transform.SetParent(transform, false);
        mLine.transform.localScale = Vector3.one;
        mLine.transform.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
    }


    public void buildPointsCloud(JSONObject pointsCoord, GameObject photo)
    {
        mPointsList.ForEach(obj => GameObject.DestroyImmediate(obj));

        mPointsList.Clear();
        Rect rect = photo.GetComponent<RectTransform>().rect;

        mXpoint = new float[pointsCoord.list.Count];
        mYpoint = new float[pointsCoord.list.Count];
        //      mControlPointsArray = new GameObject[allCoordsFront.list.Count];  // debug points array
        //      mTotalPoints = allCoordsFront.list.Count;

        if (pointsCoord.IsArray)
        {
            List<JSONObject> coordsElement = pointsCoord.list;


            for (int i = 0; i < pointsCoord.list.Count + 1; i++)
            {
                if (i == pointsCoord.list.Count)
                {
                    mPointsList.Add(mPointsList[0]);
                    break;
                }
                // In JSON from CVServer first element is Y and X is second one
                mXpoint[i] = coordsElement[i].list[1].f;
                mYpoint[i] = coordsElement[i].list[0].f;

                GameObject newPoint = GameObject.Instantiate(mRefPoint);
                mPointsList.Add(newPoint);
                newPoint.transform.SetParent(photo.transform);
                newPoint.SetActive(true);
                
                newPoint.GetComponent<RectTransform>().sizeDelta = new Vector2(4, 4);
                newPoint.GetComponent<RectTransform>().localScale = Vector3.one;
                Vector3 newPointPosition = fromPhotoToRectTransform(new Vector2(mYpoint[i], mXpoint[i]), rect);
                newPoint.GetComponent<RectTransform>().localPosition = newPointPosition;
            }

            mLine.Points = mPointsList.Select(obj => obj.GetComponent<RectTransform>().anchoredPosition).ToArray();
            mLine.raycastTarget = false;
            mLine.color = Color.white;
            mLine.LineThickness = 2;
            mLine.SetAllDirty();
        }
    }

    private Vector2 fromPhotoToRectTransform(Vector2 photoCoords, Rect rect)
    {
        Vector2 startPoint = new Vector2(-rect.width / 2, rect.height / 2);
		Vector2 result = startPoint + new Vector2(photoCoords.x, -rect.height + photoCoords.y);
        return result;
    }

    void Update()
    {
        mLine.SetAllDirty();
    }

}
