﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public abstract class CorrectionSubStateBasic : FlowSubState 
{
	[SerializeField]
	protected Text mTitle;

	[SerializeField]
	protected Button mSendCorrectedParamsBtn;

	[SerializeField]
	protected Button mBackButton;

	protected LandMarkCorrector mLandMarkCorrector;

	protected Image mPhoto;

	public override void init()
	{
		mBackButton.onClick.AddListener (onBackButtonClicked);

		GameObject lmCorrector = new GameObject ("LandmarkCorrector");
		mLandMarkCorrector = lmCorrector.AddComponent<LandMarkCorrector> ();
		mLandMarkCorrector.transform.SetParent(transform, false);
		mLandMarkCorrector.init ();

		mTitle.transform.SetParent (null, false);
		mTitle.transform.SetParent (transform, false);

		mSendCorrectedParamsBtn.transform.SetParent (null, false);
		mSendCorrectedParamsBtn.transform.SetParent (transform, false);

		mSendCorrectedParamsBtn.onClick.AddListener(onSendDataBtnClicked);
		initTexts();
	}

	protected virtual void onSendDataBtnClicked()
	{
	}

	protected virtual void initTexts()
	{
	}

	public override void setInteracteble(bool isInteracteble)
	{
		mSendCorrectedParamsBtn.interactable = isInteracteble;
		mLandMarkCorrector.setInteracteble (isInteracteble);
	}

	public override void reset()
	{
	}

	public override void setEnabled(bool isEnabled)
	{
		gameObject.SetActive (isEnabled);

		if (true == isEnabled)
		{
			if (null == mPhoto) 
			{
				StartCoroutine(createBackground (()=>
					{
						initLandmarks();
                        mBackButton.gameObject.transform.SetParent(null, false);
                        mBackButton.gameObject.transform.SetParent(transform, false);
                    }));
			}
			else 
			{
				mLandMarkCorrector.fadeTo (0.0f, 0.0f);
			}
		}
	}

	public void destroyPhoto()
	{
		if (null != mPhoto) 
		{
			GameObject.DestroyImmediate (mPhoto.gameObject);
			mPhoto = null;
		}
	}

	private void initLandmarks()
	{
		mLandMarkCorrector.clear ();

		mPhoto.transform.SetParent(mLandMarkCorrector.transform, false);
		mLandMarkCorrector.updatePhoto (mPhoto);

		mLandMarkCorrector.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		mLandMarkCorrector.GetComponent<RectTransform> ().localScale = Vector3.one;

		JSONObject allpoints = AppData.getInstance ().getData<JSONObject> (eFlowState.E_STATE_PHOTO_MAKING, "points", null, true);
		JSONObject specifiedPoints = allpoints.GetField (getSubstateType());

		mLandMarkCorrector.addLandMark (LandMarkCorrector.eLandMarkType.E_CHEST, getCoords("chest", specifiedPoints));
		mLandMarkCorrector.addLandMark (LandMarkCorrector.eLandMarkType.E_WAIST, getCoords("waist", specifiedPoints));
		mLandMarkCorrector.addLandMark (LandMarkCorrector.eLandMarkType.E_HIPS, getCoords("hips", specifiedPoints));

		mLandMarkCorrector.rescale ();
	}

	private KeyValuePair<Vector2, Vector2> getCoords(string type, JSONObject points)
	{
		try
		{
			List<JSONObject> data = points.GetField (type).list;
			KeyValuePair<Vector2, Vector2> result = new KeyValuePair<Vector2, Vector2> (new Vector2(data[0].GetField("x").f, data[0].GetField("y").f), new Vector2(data[1].GetField("x").f, data[1].GetField("y").f));

			return result;
		}
		catch
		{
			Debug.LogError ("[SideCorrectionSubState][getCoords] Error in points JSON");
			return new KeyValuePair<Vector2, Vector2> (Vector2.zero, Vector2.zero);
		}

		return new KeyValuePair<Vector2, Vector2> (Vector2.zero, Vector2.zero);
	}

	protected virtual void onBackButtonClicked()
	{
		/*JSONObject points = mLandMarkCorrector.extractCorrectedLandmarks ();
		points.AddField (getSubstateType(), points);

		Dictionary<int, JSONObject> objectsToSave = new Dictionary<int, JSONObject> ();
		objectsToSave.Add((int)(getSubstateType() == "front" ? ParamsCorrectionState.eSubState.E_FRONT_CORRECTION : ParamsCorrectionState.eSubState.E_SIDE_CORRECTION), points);
		AppData.getInstance().saveData(eFlowState.E_STATE_CORRECTION, objectsToSave);*/
	}

	void OnDestroy()
	{
		if (null != mPhoto)
		{
			if (null != mPhoto.sprite && null != mPhoto.sprite.texture && mPhoto.sprite.texture.name != "whiteBack")
				GameObject.DestroyImmediate (mPhoto.sprite.texture, true);
		}
	}

	private IEnumerator createBackground (Action callback)
	{
		Texture2D tex;
		tex = new Texture2D(4, 4, TextureFormat.RGB24, false);
		WWW www = new WWW("file:///" + Application.persistentDataPath + "/photos/" + getImagePhotoName());
		yield return www;
		www.LoadImageIntoTexture(tex);

		if (null != mPhoto)
		{
			GameObject.DestroyImmediate (mPhoto.gameObject, true);
		}
		
		GameObject imageObj = new GameObject (getImagePhotoName());
		mPhoto = imageObj.AddComponent<Image> ();
		mPhoto.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
		mPhoto.GetComponent<RectTransform> ().sizeDelta = new Vector2 (tex.width, tex.height);
		mPhoto.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		mPhoto.GetComponent<RectTransform> ().localScale = Vector3.one;
		mPhoto.GetComponent<RectTransform> ().rotation = Quaternion.Euler (0.0f, 0.0f, 270.0f);
		mPhoto.transform.SetParent(transform, false);

		if(null!= callback)
			callback();
	}

	protected abstract string getSubstateType ();
	protected abstract string getImagePhotoName ();
}
