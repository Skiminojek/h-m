﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LandMarkCorrector : MonoBehaviour 
{
	public enum eLandMarkType
	{
		E_CHEST,
		E_WAIST,
		E_HIPS,
	}

	Dictionary<eLandMarkType, LandMark> mLandMarks;

	Canvas mCanvas;
	private EventTrigger mEventTrigger;
	Image mPhoto;
	static Rect mPhotoRect;
	RectTransform mRectTransform;
	Vector2 mPhotoSize;
	Vector3 mPointOffset = Vector3.zero;
	Zoomer mZoomer;

	bool mIsInteracteble = true;

	public void init()
	{
		mLandMarks = new Dictionary<eLandMarkType, LandMark> ();
		gameObject.AddComponent<RectTransform> ();
		GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		gameObject.AddComponent<BoxCollider2D> ();

		mCanvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();
	}

	public void updatePhoto(Image photo)
	{
		mPhoto = photo;
		mPhotoRect = mPhoto.GetComponent<RectTransform> ().rect;
		mPhotoSize = new Vector2 (mPhotoRect.height, mPhotoRect.width);
		mRectTransform = GetComponent<RectTransform> ();
		mRectTransform.sizeDelta = mPhotoSize;
		mZoomer = gameObject.AddComponent<Zoomer> ();
	}

	public void rescale()
	{
		float factor = 1080 / mPhotoRect.height;
		GetComponent<RectTransform> ().localScale *= factor;
		mZoomer.mStartScale = GetComponent<RectTransform> ().localScale.x;

		fadeTo (0.0f, 0.0f);
	}

	public void fadeTo(float to, float time)
	{
		foreach(var lm in mLandMarks)
		{
				lm.Value.fadeTo (to, time);
		}
	}

	public void addLandMark(eLandMarkType type, KeyValuePair<Vector2, Vector2> coords)
	{
		GameObject lnObj = new GameObject(type.ToString() + "_landmark");
		LandMark ln = lnObj.AddComponent<LandMark> ();
		ln.transform.SetParent (transform, false);
		ln.init (fromPhotoToRectTransform(coords.Key), fromPhotoToRectTransform(coords.Value), mZoomer);
		ln.GetComponent<RectTransform> ().localScale = Vector3.one;

		mLandMarks.Add (type, ln);
	}

	public void clear()
	{
		foreach (var mark in mLandMarks)
			GameObject.DestroyImmediate (mark.Value);

		mLandMarks.Clear ();
		GetComponent<RectTransform> ().localScale = Vector2.one;
	}
		
	public JSONObject extractCorrectedLandmarks()
	{
		JSONObject result = new JSONObject ();
		foreach (var landmark in mLandMarks) 
		{
			KeyValuePair<Vector2, Vector2> coords = landmark.Value.getCoords ();
			Vector2 firstPhotoCoord = fromRectTransformToPhoto (coords.Key);
			Vector2 secondPhotoCoord = fromRectTransformToPhoto (coords.Value);

			JSONObject first = new JSONObject ();
			first.AddField ("x", firstPhotoCoord.x);
			first.AddField ("y", firstPhotoCoord.y);

			JSONObject second = new JSONObject ();
			second.AddField ("x", secondPhotoCoord.x);
			second.AddField ("y", secondPhotoCoord.y);

			JSONObject lm = new JSONObject();
			lm.Add(first);
			lm.Add(second);

			result.AddField (getStringIDByLandmarkType (landmark.Key), lm);
		}

		return result;
	}

	public static Vector2 fromPhotoToRectTransform(Vector2 photoCoords)
	{
		Vector2 startPoint = new Vector2 (-mPhotoRect.height / 2, mPhotoRect.width / 2);
		Vector2 result = startPoint + new Vector2(photoCoords.x, -photoCoords.y );
		return result;




	}

	public static Vector2 fromRectTransformToPhoto(Vector2 rectTransformCoords)
	{
		Vector2 startPoint = new Vector2 (-mPhotoRect.height / 2, mPhotoRect.width / 2);
		Vector2 result = rectTransformCoords - startPoint;
		result =  new Vector2 (result.x, result.y * (-1));
		return result;
	}

	private string getStringIDByLandmarkType(eLandMarkType type)
	{
		switch (type) 
		{
		case eLandMarkType.E_CHEST:
			return "chest";
		case eLandMarkType.E_WAIST:
			return "waist";
		case eLandMarkType.E_HIPS:
			return "hips";
		default:
			Debug.LogError ("[LandMarkCorrector][getStringIDByLandmarkType] Unhandeled landmark type - " + type.ToString ());
			return "";
		}
	}

	/*void OnMouseDrag()
	{
		Vector2 pos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(mCanvas.transform as RectTransform, Input.mousePosition + mPointOffset , mCanvas.worldCamera, out pos);

		Vector2 scaledSize = mRectTransform.sizeDelta * mRectTransform.localScale.x;

		transform.position = mCanvas.transform.TransformPoint(pos);
	}

	void OnMouseDown()
	{
		mPointOffset = transform.position - Input.mousePosition;
	}*/

	public void setInteracteble(bool isInteracteble)
	{
		mIsInteracteble = isInteracteble;
	}
}
