﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;


public class LandMark : MonoBehaviour 
{
	
	LandMarkPoint mFirstPoint, mSecondPoint;
	RectTransform mFirstPointRT, mSecondPointRT;
	UILineRenderer mLineRenderer;
	Canvas mCanvas;
	Vector2 mLineOffset;

	public void init(Vector2 firstPos, Vector2 secondPos, Zoomer zoomer)
	{
		gameObject.AddComponent<RectTransform> ();
		gameObject.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		mLineRenderer = gameObject.AddComponent<UILineRenderer> ();
		mLineRenderer.Points = new Vector2[2];
		mLineRenderer.raycastTarget = false;
		mLineRenderer.color = Color.white;

		GameObject gm = new GameObject ("FirstPoint");
		mFirstPoint = gm.AddComponent<LandMarkPoint> ();
		mFirstPoint.init (firstPos, zoomer);
		mFirstPoint.transform.SetParent(transform, false);
		mFirstPointRT = mFirstPoint.GetComponent<RectTransform> ();
		mFirstPointRT.anchoredPosition = firstPos;

		gm = new GameObject ("SecondPoint");
		mSecondPoint = gm.AddComponent<LandMarkPoint> ();
		mSecondPoint.init (secondPos, zoomer);
		mSecondPoint.transform.SetParent(transform, false);
		mSecondPointRT = mSecondPoint.GetComponent<RectTransform> ();
		mSecondPointRT.anchoredPosition = secondPos;

		mCanvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();
		mLineOffset = mFirstPoint.GetComponent<RectTransform> ().sizeDelta / 2;

		mLineRenderer.LineThickness = 2;

		mFirstPoint.fadeTo (0.0f, 0.5f);
		mSecondPoint.fadeTo (0.0f, 0.5f);
	}

	public KeyValuePair<Vector2, Vector2> getCoords()
	{
		return new KeyValuePair<Vector2, Vector2> (mFirstPointRT.anchoredPosition, mSecondPointRT.anchoredPosition);
	}

	void Update()
	{
		mLineRenderer.Points[0] = mFirstPointRT.anchoredPosition + mLineOffset;
		mLineRenderer.Points[1] = mSecondPointRT.anchoredPosition + mLineOffset;
		mLineRenderer.SetAllDirty ();
	}

	public void fadeTo(float to, float time)
	{
		mFirstPoint.fadeTo (to, time);
		mSecondPoint.fadeTo (to, time);
	}
}
