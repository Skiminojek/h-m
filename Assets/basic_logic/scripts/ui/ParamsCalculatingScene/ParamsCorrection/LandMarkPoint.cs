﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LandMarkPoint : MonoBehaviour 
{
	private Image mPoint, mPin, mFakeMouseListener;
	private EventTrigger mEventTrigger;
	private Canvas mCanvas;
	private Vector3 mPointOffset = new Vector3(-0.0f, 0.0f, 0.0f);
	private Vector3 mStandartShift = new Vector3 (0.0f, 100.0f, 0.0f);
	private Vector3 mCurrentShift;
	private Vector3 mStartPos;

	bool mIsInteracteble = true;
	bool mIsMouseWasDowned = false;

	Zoomer mZoomer;

	public void init(Vector2 startPos, Zoomer zommer)
	{
		gameObject.AddComponent<RectTransform> ();

		mEventTrigger = gameObject.AddComponent<EventTrigger> ();
		Texture2D tex = Resources.Load ("LandMarkImages/point_circle") as Texture2D;
		Texture2D pinTex = Resources.Load ("LandMarkImages/Point_Control") as Texture2D;

		GameObject point = new GameObject ("Point");
		mPoint = point.AddComponent<Image> ();

		point.transform.SetParent(transform, false);
		mPoint.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
		mPoint.GetComponent<RectTransform>().sizeDelta = new Vector2 (10.0f, 10.0f);
		mPoint.color = Color.white;

		GameObject pin = new GameObject ("Pin");
		mPin = pin.AddComponent<Image> ();
		mPin.GetComponent<RectTransform> ().sizeDelta = new Vector2 (pinTex.width, pinTex.height) * 0.3f;
		mPin.sprite = Sprite.Create (pinTex, new Rect (0, 0, pinTex.width, pinTex.height), new Vector2 (0.5f, 0.5f));
		mPin.transform.SetParent(transform, false);
		mPin.GetComponent<RectTransform> ().anchoredPosition += new Vector2 (0.0f, -28.0f);
		mStandartShift = new Vector3 (0.0f, mPin.mainTexture.height, 0.0f);

		GameObject fakeListener = new GameObject ("FakeListener");
		mFakeMouseListener = fakeListener.AddComponent<Image> ();
		mFakeMouseListener.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
		mFakeMouseListener.GetComponent<RectTransform> ().sizeDelta = new Vector2 (50.0f, 50.0f);
		mFakeMouseListener.color = new Color (1.0f, 1.0f, 1.0f, 0.0f);
		mFakeMouseListener.transform.SetParent(transform, false);

		transform.position = startPos;

		EventTrigger.Entry onDownEntry = new EventTrigger.Entry ();
		EventTrigger.Entry onUpEntry = new EventTrigger.Entry ();
		EventTrigger.Entry onDragEntry = new EventTrigger.Entry ();

		onDragEntry.eventID = EventTriggerType.Drag;
		onDragEntry.callback.AddListener((eventData)=>{OnMouseDrag();});
		mEventTrigger.triggers.Add (onDragEntry);

		onDownEntry.eventID = EventTriggerType.PointerDown;
		onDownEntry.callback.AddListener((eventData)=>{OnMouseDown();});
		mEventTrigger.triggers.Add (onDownEntry);

		onUpEntry.eventID = EventTriggerType.PointerUp;
		onUpEntry.callback.AddListener((eventData)=>{OnMouseUp();});
		mEventTrigger.triggers.Add (onUpEntry);

		mCanvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();
		mZoomer = zommer;
	}

	void OnMouseDown()
	{
		if (Input.touchCount > 1 || true == mZoomer.getIsOnMoveing() || false == mIsInteracteble)
			return;

		mIsMouseWasDowned = true;
		fadeTo (1.0f, 0.25f);
		mPointOffset = transform.position - Input.mousePosition;

		Vector3 zoomerScale = mZoomer.GetComponent<RectTransform> ().localScale;
		float scaleRelation = zoomerScale.x / mZoomer.mStartScale;
		mCurrentShift = new Vector3(0.0f, mPin.GetComponent<RectTransform>().sizeDelta.y * scaleRelation, 0.0f);
		mStartPos = mZoomer.transform.position;
		mZoomer.shiftTo (mZoomer.transform.position + (-mPointOffset + mCurrentShift));
	}

	void OnMouseDrag()
	{
		if (Input.touchCount > 1 || true == mZoomer.getIsOnMoveing() || false == mIsInteracteble || false == mIsMouseWasDowned)
			return;
		
		Vector2 pos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(mCanvas.transform as RectTransform, Input.mousePosition + mPointOffset + (-mPointOffset + mCurrentShift) , mCanvas.worldCamera, out pos);
		transform.position = mCanvas.transform.TransformPoint(pos);
	}

	void OnMouseUp()
	{
		if (false == mIsInteracteble || false == mIsMouseWasDowned)
			return;

		mIsMouseWasDowned = false;

		fadeTo (0.0f, 0.25f);
		mZoomer.shiftTo (mStartPos);
	}

	public void fadeTo(float to, float time)
	{
		mPin.CrossFadeAlpha (to, time, true);
		mPin.raycastTarget = to != 0.0f;
	}

	public void setInteracteble(bool isInteracteble)
	{
		mIsInteracteble = isInteracteble;
	}
}
