﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParamsCorrectionState : FlowUnitBasic, IFlowState
{
    public enum eSubState
    {
        E_FRONT_CORRECTION,
		E_SIDE_CORRECTION,
    }

    Dictionary<eSubState, FlowSubState> mSubStates;

    [SerializeField]
    UIController mUIController;

    [SerializeField]
    soundsHandler mSoundHandler;

	List<string> mCorrectionTypes = new List<string>{ "chest", "waist", "hips" };

	[SerializeField]
	CorrectionSubstateHint mHint;

	[SerializeField]
	GameObject mPreloader;

    /*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/
    void Awake()
    {
    }

    /*===============================================================================================
	* 
	* IFlowState implementation
	* 
	* =============================================================================================*/

    public override void setEnabled(bool isenabled, int substate)
    {
        if (false == isenabled)
        {
			foreach (var sstate in mSubStates) 
			{
				sstate.Value.setEnabled (false);

				CorrectionSubStateBasic basic = sstate.Value as CorrectionSubStateBasic;

				if(null != basic)
					basic.destroyPhoto ();
			}
        }
		else
		{
			mPreloader.SetActive (false);
		}
			
        gameObject.SetActive(isenabled);
        setInteracteble(isenabled);

		if(true == isenabled)
			setSubState (substate);
    }

    public override void reset()
    {
    }

    public override void init()
    {
        mStateType = eFlowState.E_STATE_CORRECTION;

        mSubStates = new Dictionary<eSubState, FlowSubState>();
		mSubStates.Add(eSubState.E_FRONT_CORRECTION, transform.Find("FrontCorrectionSubState").gameObject.GetComponent<FlowSubState>());
		mSubStates.Add(eSubState.E_SIDE_CORRECTION, transform.Find("SideCorrectionSubState").gameObject.GetComponent<FlowSubState>());

		foreach (var substate in mSubStates) 
		{
			substate.Value.setListener (this);
			substate.Value.init ();
		}

        initStrings();
    }

    /*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/


    /*===============================================================================================
    * 
    * IFlowState implementation
    * 
    * =============================================================================================*/

    public void setSubState(int subState)
    {
        foreach (var sstate in mSubStates)
        {
            if ((int)sstate.Key == subState)
                sstate.Value.setEnabled(true);
            else
                sstate.Value.setEnabled(false);
        }

		mHint.setEnabled (true, (subState == (int)eSubState.E_FRONT_CORRECTION) ? "front" : "side");
    }

    public void setCollectedData(int subState, string stringData)
    {
        Dictionary<int, JSONObject> objectsToSave = new Dictionary<int, JSONObject>();
		JSONObject JSONData = new JSONObject ();

		switch (subState) 
		{
		case (int)eSubState.E_FRONT_CORRECTION:
			JSONData.AddField ("front", new JSONObject (stringData));
			objectsToSave.Add(subState, JSONData);
			AppData.getInstance().saveData(eFlowState.E_STATE_CORRECTION, objectsToSave);

			setSubState ((int)ParamsCorrectionState.eSubState.E_SIDE_CORRECTION);

			/*JSONObject points = AppData.getInstance ().getData<JSONObject> (eFlowState.E_STATE_PHOTO_MAKING, "points", null, true);
			JSONObject sideData = points.GetField ("side");

			JSONData.AddField ("side", sideData);
			onCorrectionComplete();*/
			break;
		case (int)eSubState.E_SIDE_CORRECTION:
			JSONData.AddField ("side", new JSONObject (stringData));
			objectsToSave.Add(subState, JSONData);
			AppData.getInstance().saveData(eFlowState.E_STATE_CORRECTION, objectsToSave);

			onCorrectionComplete ();
			break;
		default:
			Debug.LogError ("[ParamsCorrectionState][setCollectedData] Got unhandeled substate - " + ((ParamsCorrectionState.eSubState)subState).ToString ());
			onCorrectionComplete ();
			break;
		}
    }

    public string getCollectedData(int substate)
    {
        return "";
    }

    //Should be removed in the future
    public void setGlobalState(eFlowStateBase flowState, int substate)
    {
		mUIController.setNewFlowState (flowState, substate);
    }

    /*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

    /*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

    private void initStrings()
    {
    }

    private void setInteracteble(bool isInteracteble)
    {
		foreach (var sstate in mSubStates)
		{
			sstate.Value.setInteracteble(isInteracteble);
		}
    }

	private void onCorrectionComplete()
	{
	}

	private bool isLandmarksWereChanged()
	{
		JSONObject points = AppData.getInstance ().getData<JSONObject> (eFlowState.E_STATE_PHOTO_MAKING, "points", null, true);

		try
		{
			JSONObject frontCorrectedData = AppData.getInstance().getData<JSONObject>(eFlowState.E_STATE_CORRECTION, "front", null, true);
			JSONObject sideCorrectedData = AppData.getInstance().getData<JSONObject>(eFlowState.E_STATE_CORRECTION, "side", null, true);

			JSONObject frontPersistedData = points.GetField ("front");
			JSONObject sidePersistedData = points.GetField ("side");

			int index = 0;
            bool hasBeenChanged = false;
			foreach(string type in mCorrectionTypes)
			{
				if (false == isEqual(type, frontPersistedData[type], frontCorrectedData[type]))
				{
					hasBeenChanged = true;
					AnalyticHandler.sendEvent(AnalyticHandler.aTopicPhoto, new Dictionary<string, object> {
						{"Front correction" , "Corrected point in : " + type }
                	});
				}

				if (false == isEqual(type, sidePersistedData[type], sideCorrectedData[type]))
				{
					hasBeenChanged = true;
					AnalyticHandler.sendEvent(AnalyticHandler.aTopicPhoto, new Dictionary<string, object> {
						{"Side correction" , "Corrected point in : " + type }
						});
				}

                ++index;
			}
            
			return hasBeenChanged;
		}
		catch
		{
			Debug.LogError ("[SideCorrectionSubState][getCoords] Error in points JSON");
		}

		return false;
	}

	private bool isEqual(string type, JSONObject persistedData, JSONObject correctedData)
	{
		List<JSONObject> persistedObj = persistedData.list;
		List<JSONObject> correctedObj = correctedData.list;

		for (int i = 0; i < persistedObj.Count; ++i) 
		{
			bool xCoordEquals = isFloatsEqual(persistedObj[i].GetField("x").f, correctedObj[i].GetField("x").f);
			bool yCoordEquals = isFloatsEqual(persistedObj[i].GetField("y").f, correctedObj[i].GetField("y").f);

			if (false == xCoordEquals || false == yCoordEquals)
				return false;
		}

		return true;
	}

	private bool isFloatsEqual(float first, float second)
	{
		float diff = Mathf.Abs (first - second);

		if (diff < 0.01)
			return true;

		return false;
	}

	private void resaveParams(string parameters, string starsInfo)
	{

	}
    /*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
