﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsState : FlowUnitBasic, IFlowState
{
	public enum eSubState
	{
		E_RESULTS_PREVIEW,
		E_MODEL_SHOWING_SUB_STATE,
	}

	Dictionary<eSubState, FlowSubState> mSubStates;

	[SerializeField]
	UIController mUIController;

    [SerializeField]
    soundsHandler mSoundHandler;

	[SerializeField]
	Button mMenuBtn;

	[SerializeField]
	Text mTitle;

	[SerializeField]
	innerMenu mInnerMenu;

	int mCurrentSubstate = -1;

	public override void setEnabled (bool isenabled, int substate)
	{
        if (false == isenabled)
        {
            foreach (var sstate in mSubStates)
                sstate.Value.setEnabled(false);
        }
        else
        {
            setSubState(substate);
        }

		gameObject.SetActive (isenabled);
		setInteracteble (isenabled);
	}

	public override void reset ()
	{
	}

	public override void init ()
	{
		mStateType = eFlowState.E_STATE_RESULTS;

		mMenuBtn.onClick.AddListener (onMenuBtnClicked);

		mSubStates = new Dictionary<eSubState, FlowSubState> ();
		mSubStates.Add(eSubState.E_MODEL_SHOWING_SUB_STATE, transform.Find("ModelShowingSubState").gameObject.GetComponent<FlowSubState>());
        mSubStates.Add(eSubState.E_RESULTS_PREVIEW, transform.Find("ResultsPreviewSubState").gameObject.GetComponent<ResultsPreviewSubState>());


        foreach (var substate in mSubStates) 
		{
			substate.Value.setListener (this);
			substate.Value.init ();
		}

		initStrings ();
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/


	/*===============================================================================================
    * 
    * IFlowState implementation
    * 
    * =============================================================================================*/

	public void setSubState (int subState)
	{
		if (subState != mCurrentSubstate) 
		{
			mCurrentSubstate = subState;
			foreach (var sstate in mSubStates) 
			{
				if ((int)sstate.Key == subState)
					sstate.Value.setEnabled (true);
				else
					sstate.Value.setEnabled (false);
			}
		}
	}

	public void setCollectedData (int subState, string JSONData)
	{
		Dictionary<int, JSONObject> objectsToSave = new Dictionary<int, JSONObject> ();
		objectsToSave.Add (subState, new JSONObject (JSONData));

		AppData.getInstance ().saveData (eFlowState.E_STATE_INFO_INPUT, objectsToSave);
	}

	public string getCollectedData (int substate)
	{
		return "";
	}

	//Should be removed in the future
	public void setGlobalState(eFlowStateBase flowState, int substate)
	{
        mUIController.setNewFlowState(flowState, substate);
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

	private void initStrings()
	{
		mTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_TITLE");
	}

	private void onMenuBtnClicked()
	{
        mSoundHandler.beepClick();
        mInnerMenu.setEnabled (true);
	}

	private void setInteracteble(bool isInteracteble)
	{
		mMenuBtn.interactable = isInteracteble;
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
