﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class ResultsPreviewSubState : FlowSubState
{
    struct Item
    {
        public string value;
    }
    // ============================================================BODY PART================================================================
    [SerializeField]
    soundsHandler mSounds;

    [SerializeField]
	Text mParameters, mHeight, mBodyShape, mBodyMoreDescription;

	[SerializeField]
	List<GameObject> mBodyTypesImagesTop;

    [SerializeField]
    List<GameObject> mBodyTypesImagesBottom;

    [SerializeField]
    Button mBodyLearnMoreButton;
 
    
    [SerializeField]
	Text mSizeTitle, mSize, mMoreDescription;

    // =====================================================================================================================================


    [SerializeField]
    Image mBestMatchPreloader, mClothesPreloader;

    [SerializeField]
    Text mPoloSize, mJacketSize, mTrousersSize;
	// =====================================================================================================================================

    bool mIsInitted = false;
    bool mImagesLoaded = false;

    /*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/

    void Awake()
	{
		mIsInitted = false;
        mImagesLoaded = false;
    }

    private void Update()
    {

    }

    public override void setEnabled(bool isEnabled)
	{
		gameObject.SetActive (isEnabled);

		if (true == isEnabled) 
		{
            if(false == mIsInitted)
            {
                // get saved jsons
                JSONObject photoStateResult = AppData.getInstance().getData<JSONObject>(eFlowState.E_SHARED, DataAttributes.Shared.mAPIv2CompleteAnswerJSON, null, true);
             //   Debug.Log(photoStateResult.ToString());
                mParameters.text = photoStateResult.GetField("volume_params").GetField("body_model").str;
                //load obj file
                StartCoroutine(loadOBJ());
               
                //
                mIsInitted = true;
            }


		}
	}

    private IEnumerator loadOBJ()
    {
        string modelAddress = mParameters.text;
        WWW objWWW = new WWW(modelAddress);
        yield return objWWW;

        string meshPath = Application.persistentDataPath + "/modelOBJ.obj";
        Debug.Log("OBJ saved in " + meshPath);

        System.IO.File.WriteAllBytes(meshPath, objWWW.bytes);

        mHeight.text = "OBJ saved in " + meshPath;
       
    }

	public override void setInteracteble (bool isInteracteble)
	{
	}

	public override void reset ()
	{
	}

	public override void init()
	{
	}



    private void OnDestroy()
    {
      
    }

}
