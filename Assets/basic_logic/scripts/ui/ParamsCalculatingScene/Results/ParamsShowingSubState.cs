﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ParamsShowingSubState : FlowSubState
{

	private struct Params
	{
		public string mBodyType;
		public float mBodyLength;
		public float mLegsLength;

		public float mShouldersFrontText;
		public float mShouldersSideText;	
		public float mShouldersVolumeText;
		public string mShouldersSizeText;	

		public float mChestFrontText;		
		public float mChestSideText; 		
		public float mChestVolumeText; 	
		public string mChestSizeText; 		
		
		public float mWaistFrontText;		
		public float mWaistSideText;		
		public float mWaistVolumeText;	
		public string mWasitSizeText;		
		
		public float mHipsFrontText; 		
		public float mHipsSideText; 		
		public float mHipsVolumeText;		
		public string mHipsSizeText; 		
	}
		
	//Text titles
	[SerializeField]
	Text mTypeTitle, mFrontTitle, mSideTitle, mVolumeTitle, mSizeTitle, mWaistTitle, mChestTitle, mHipsTitle, mShouldersTitle;

	[SerializeField]
	Text mBodyType, mBodyLength, mLegsLength;

	[SerializeField]
	Text mWaistFrontText, mWaistSideText, mWaistVolumeText, mWasitSizeText, mChestFrontText, mChestSideText, mChestVolumeText, mChestSizeText, mHipsFrontText, mHipsSideText, mHipsVolumeText, mHipsSizeText,
	mShouldersFrontText, mShouldersSideText, mShouldersVolumeText, mShouldersSizeText;

	Params mParams;

	string mMeasurements;
	/*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/

	void Awake()
	{
		//fadeTo (0.0f, 0.0f);
	}

	public override void setEnabled(bool isEnabled)
	{
		fadeTo ((true == isEnabled) ? 1.0f : 0.0f, 0.0f);
		mMeasurements = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.Shared.mMeasurementsAttribute, DataAttributes.Shared.mMeasurements_CM_KG);

		if (true == isEnabled) 
		{
			readParams ();

			if (mMeasurements == DataAttributes.Shared.mMeasurements_IN_LBS)
				recalculateMeasurements ();

			initTitles ();
			initParams ();
		}
	}

	public override void setInteracteble (bool isInteracteble)
	{
	}

	public override void reset ()
	{
	}

	public override void init()
	{
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/
	private void initTexts()
	{

	}

	private void initTitles()
	{
		mTypeTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_TYPE_TITLE");
		mWaistTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_WAIST_TITLE");
		mChestTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_CHEST_TITLE");
		mHipsTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_HIPS_TITLE");
		mShouldersTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SHOULDERS_TITLE");
		mFrontTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_FRONT_TITLE");
		mSideTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIDE_TITLE");
		mVolumeTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_VOLUME_TITLE");
		mSizeTitle.text = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_SIZE_TITLE");
	}

	private void initParams()
	{
		string bodyTypeBaseString = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_BODY_TYPE_TITLE");
		mBodyType.text = string.Format ("{0} {1}", bodyTypeBaseString, mParams.mBodyType);

		string bodyHeightBaseString = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_BODY_LENGTH_TITLE");
		mBodyLength.text = string.Format ("{0} {1}", bodyHeightBaseString, (mParams.mShouldersFrontText > 0.0f) ? mParams.mBodyLength.ToString("0.00") : "-");

		string legsHeightBaseString = StringsManager.getString ("RESULT_SHOWING_STATE_PARAMS_SUB_STATE_BODY_LEGS_LENGTH_TITLE");
		mLegsLength.text = string.Format ("{0} {1}", legsHeightBaseString, (mParams.mShouldersFrontText > 0.0f) ? mParams.mLegsLength.ToString("0.00") : "-");

		mShouldersFrontText.text = (mParams.mShouldersFrontText > 0.0f) ? mParams.mShouldersFrontText.ToString("0.00") : "-";
		mShouldersSideText.text = (mParams.mShouldersSideText > 0.0f) ? mParams.mShouldersSideText.ToString("0.00") : "-";
		mShouldersVolumeText.text = (mParams.mShouldersVolumeText > 0.0f) ? mParams.mShouldersVolumeText.ToString("0.00") : "-";
		mShouldersSizeText.text = mParams.mShouldersSizeText;
	 	
		mChestFrontText.text = (mParams.mChestFrontText > 0.0f) ? mParams.mChestFrontText.ToString("0.00") : "-";
		mChestSideText.text = (mParams.mChestSideText > 0.0f) ? mParams.mChestSideText.ToString("0.00") : "-";
		mChestVolumeText.text = (mParams.mChestVolumeText > 0.0f) ? mParams.mChestVolumeText.ToString("0.00") : "-";
		mChestSizeText.text =  mParams.mChestSizeText;
	 	
		mWaistFrontText.text = (mParams.mWaistFrontText > 0.0f) ? mParams.mWaistFrontText.ToString("0.00") : "-";
		mWaistSideText.text = (mParams.mWaistSideText > 0.0f) ? mParams.mWaistSideText.ToString("0.00") : "-";
		mWaistVolumeText.text = (mParams.mWaistVolumeText > 0.0f) ? mParams.mWaistVolumeText.ToString("0.00") : "-";
		mWasitSizeText.text = mParams.mWasitSizeText;
	 	
		mHipsFrontText.text = (mParams.mHipsFrontText > 0.0f) ? mParams.mHipsFrontText.ToString("0.00") : "-";
		mHipsSideText.text = (mParams.mHipsSideText > 0.0f) ? mParams.mHipsSideText.ToString("0.00") : "-";
		mHipsVolumeText.text = (mParams.mHipsVolumeText > 0.0f) ? mParams.mHipsVolumeText.ToString("0.00") : "-";
		mHipsSizeText.text = mParams.mHipsSizeText;
	}

	private void selectSizeLabel()
	{

	}

	private void readParams()
	{
		mParams = new Params ();

	}

	private void fadeTo(float to, float time)
	{
		Graphic [] graphics = gameObject.GetComponentsInChildren<Graphic> ();

		foreach(Graphic graphic in graphics)
			graphic.CrossFadeAlpha(to, time, true);
	}

	private void recalculateMeasurements()
	{
		mParams.mBodyLength = MeasurementsTranslator.CMToIN (mParams.mBodyLength);
		mParams.mLegsLength = MeasurementsTranslator.CMToIN (mParams.mLegsLength);

		mParams.mShouldersFrontText = MeasurementsTranslator.CMToIN (mParams.mShouldersFrontText);
		mParams.mShouldersSideText = MeasurementsTranslator.CMToIN (mParams.mShouldersSideText);
		mParams.mShouldersVolumeText = MeasurementsTranslator.CMToIN (mParams.mShouldersVolumeText);

		mParams.mChestFrontText = MeasurementsTranslator.CMToIN (mParams.mChestFrontText);
		mParams.mChestSideText = MeasurementsTranslator.CMToIN (mParams.mChestSideText);
		mParams.mChestVolumeText = MeasurementsTranslator.CMToIN (mParams.mChestVolumeText);

		mParams.mWaistFrontText = MeasurementsTranslator.CMToIN (mParams.mWaistFrontText);
		mParams.mWaistSideText = MeasurementsTranslator.CMToIN (mParams.mWaistSideText);
		mParams.mWaistVolumeText = MeasurementsTranslator.CMToIN (mParams.mWaistVolumeText);

		mParams.mHipsFrontText = MeasurementsTranslator.CMToIN (mParams.mHipsFrontText);
		mParams.mHipsSideText = MeasurementsTranslator.CMToIN (mParams.mHipsSideText);
		mParams.mHipsVolumeText = MeasurementsTranslator.CMToIN (mParams.mHipsVolumeText);
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
