﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelShowingSubState : FlowSubState
{
	/*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/

	[SerializeField]
	Image mCurrentModel;

	void Awake()
	{
		//fadeTo (0.0f, 0.0f);
	}

	public override void setEnabled(bool isEnabled)
	{
		fadeTo ((true == isEnabled) ? 1.0f : 0.0f, 0.15f);

		if (true == isEnabled) 
		{
			string gender = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, DataAttributes.Shared.mGenderMale, true);
			string bodytype = AppData.getInstance ().getData<string> (eFlowState.E_STATE_PHOTO_MAKING, "", "");
			string imageName = getImageNameFromBodyType (gender, bodytype);

            AnalyticHandler.sendEvent(AnalyticHandler.aTopicUser, new Dictionary<string, object>
        {
            { "gender", gender },
            {  "bodyshape", bodytype }
        });

            Texture2D tex = Resources.Load (string.Format ("BodyTypes/{0}", imageName)) as Texture2D;
			if (null != tex)
			{
				Rect rect = new Rect (0, 0, tex.width, tex.height);
				mCurrentModel.GetComponent<Image> ().sprite = Sprite.Create (tex, rect, new Vector2 (0.5f, 0.5f));
			} 
			else
			{
				tex = Resources.Load ("BodyTypes/whiteBack") as Texture2D;

				if (null != tex) 
				{
					Rect rect = new Rect (0, 0, tex.width, tex.height);
					mCurrentModel.GetComponent<Image> ().sprite = Sprite.Create (tex, rect, new Vector2 (0.5f, 0.5f));
				}

				Debug.Log (string.Format ("Image with path - {0} not found", imageName));
			}
		}
	}

	public override void setInteracteble (bool isInteracteble)
	{
	}

	public override void reset ()
	{
	}

	public override void init()
	{
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/
	private void fadeTo(float to, float time)
	{
		Graphic [] graphics = gameObject.GetComponentsInChildren<Graphic> ();

		foreach(Graphic graphic in graphics)
			graphic.CrossFadeAlpha(to, time, true);
	}

	private string getImageNameFromBodyType(string gender, string bodyType)
	{
		string basic = "";
		switch (bodyType) 
		{
		case "Apple":
			basic = "Apple";
			break;
		case "Triangle":
			basic = "Triangle";
			break;
		case "Inverted triangle":
			basic = "InvertedTriangle";
			break;
		case "Rectangle":
			basic = "Rectangle";
			break;
		case "Hourglass":
			basic = "Hourglass";
			break;
		}

		if ("" == basic) 
		{
			Debug.LogError (string.Format ("[ModelShowingSubState][getImageNameFromBodyType]Body type - {0} not found", bodyType));
			return "whiteBack";
		}

		basic = string.Format ("{0}{1}", (DataAttributes.Shared.mGenderMale == gender) ? "Man" : "Woman", basic);

		return basic;
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
