﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VideoDescriptionState : FlowUnitBasic, IFlowState
{
	[SerializeField]
	MediaPlayerCtrl mMediaPlayer;

	[SerializeField]
	UIController mUIController;

	[SerializeField]
	Button mSkipBtn;

	[SerializeField]
	Button mSetFullScreenBtn;

	[SerializeField]
	Button mSetPartScreenBtn;

	[SerializeField]
	Vector2 mSkipBtnFullScreenPos;

	[SerializeField]
	Vector2 mSkipBtnPartScreenPos;

	[SerializeField]
	RectTransform mVideoStream;

	[SerializeField]
	Image mSetPartScreenBtnImage;

    [SerializeField]
    soundsHandler mSoundHandler;

	private bool mIsFullScreen = false;

	/*===============================================================================================
	* 
	* MonoBehaviour implementation
	* 
	* =============================================================================================*/

	void Awake()
	{
		mStateType = eFlowState.E_STATE_VIDEO_DESCRIPTION;

		mSkipBtn.interactable = false;
		mSetFullScreenBtn.GetComponentInChildren<Text> ().text = StringsManager.getString ("VIDEO_DESCRIPTION_STATE_FULL_SCREEN_BTN_TEXT");

		mSetFullScreenBtn.onClick.AddListener (onSetFullScreenBtnClicked);
		mSetPartScreenBtn.onClick.AddListener (onSetPartScrenBtnClicked);

		mSkipBtn.GetComponentInChildren<Text> ().text = StringsManager.getString ("VIDEO_DESCRIPTION_STATE_SKIP_TEXT");
		mSkipBtn.onClick.AddListener (() => 
			{
                mSoundHandler.beepClick();
                JSONObject toPersist = new JSONObject();
				toPersist.AddField(DataAttributes.Authorization.mIsVideoDescriptionShowed, true);
				setCollectedData(0, toPersist.ToString());
				mUIController.setNewFlowState(eFlowState.E_STATE_PHOTO_MAKING, 0);
			});


		MediaPlayerCtrl.OnEnd += onVideoEnd;
		MediaPlayerCtrl.OnVideoError += onVideoError;
		MediaPlayerCtrl.OnReady += onVideoReady;

	}

	void OnDestroy()
	{
		MediaPlayerCtrl.OnEnd -= onVideoEnd;
		MediaPlayerCtrl.OnVideoError -= onVideoError;
		MediaPlayerCtrl.OnReady -= onVideoReady;
	}

	private void onVideoEnd()
	{
		mMediaPlayer.UnLoad();

		JSONObject toPersist = new JSONObject();
		toPersist.AddField(DataAttributes.Authorization.mIsVideoDescriptionShowed, true);
		setCollectedData(0, toPersist.ToString());
		mUIController.setNewFlowState(eFlowState.E_STATE_PHOTO_MAKING, 0);
	}

	private void onVideoError(MediaPlayerCtrl.MEDIAPLAYER_ERROR error, MediaPlayerCtrl.MEDIAPLAYER_ERROR extra)
	{
		MessageDialogsManager.createVideoProcessingError(()=>
			{
				onVideoEnd ();
			}, 0.0f);
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/


	/*===============================================================================================
	* 
	* FlowUnitBasic implementation
	* 
	* =============================================================================================*/
	public override void setEnabled (bool isenabled, int substate)
	{
		mSkipBtn.interactable = false;
		gameObject.SetActive (isenabled);

		if (true == isenabled) 
		{
			mIsFullScreen = false;
			setScreenMode (mIsFullScreen);

			mMediaPlayer.Load("VideoDescription.mp4");
		}
	}

	public override void reset ()
	{
	}

	public override void init ()
	{
	}

	public void setSubState (int subState)
	{
	}

	public void setCollectedData (int subState, string JSONData)
	{
		Dictionary<int, JSONObject> data = new Dictionary<int, JSONObject> ();
		data.Add (subState, new JSONObject(JSONData));
		AppData.getInstance ().saveData (eFlowState.E_STATE_VIDEO_DESCRIPTION, data);
	}

	public string getCollectedData (int substate)
	{
		return "";
	}

	public void setGlobalState(eFlowStateBase flowState, int substate)
	{
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/


	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/
	private void setScreenMode(bool isFullScreen)
	{
		if (isFullScreen) 
		{
			mSetPartScreenBtn.gameObject.SetActive (true);
			mSetPartScreenBtnImage.CrossFadeAlpha (0.0f, 2.0f, true);
			mSetFullScreenBtn.gameObject.SetActive (false);

			mVideoStream.Rotate(new Vector3 (0.0f, 0.0f, 90.0f));
			mVideoStream.sizeDelta = new Vector2 (1920, 0);
			mSkipBtn.GetComponent<RectTransform>().Rotate(new Vector3 (0.0f, 0.0f, 90.0f));

			mSkipBtn.GetComponent<RectTransform> ().anchoredPosition = mSkipBtnFullScreenPos;
		}
		else
		{
			mSetPartScreenBtnImage.CrossFadeAlpha (1.0f, 0.0f, false);
			mSetPartScreenBtn.gameObject.SetActive (false);
			mSetFullScreenBtn.gameObject.SetActive (true);

			mVideoStream.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 0.0f);
			mVideoStream.sizeDelta = new Vector2 (1080, 0);
			mSkipBtn.GetComponent<RectTransform>().rotation = new Quaternion (0.0f, 0.0f, 0.0f, 0.0f);

			mSkipBtn.GetComponent<RectTransform> ().anchoredPosition = mSkipBtnPartScreenPos;
		}
	}

	private void onSetFullScreenBtnClicked()
	{
        mSoundHandler.beepClick();
		setScreenMode (true);
	}

	private void onSetPartScrenBtnClicked()
	{
        mSoundHandler.beepClick();
        setScreenMode (false);
	}

	private void setSkipInteracteble()
	{
		mSkipBtn.interactable = true;
	}

	private void onVideoReady()
	{

        JSONObject forWriting = new JSONObject();
        forWriting.AddField(DataAttributes.Shared.mAlreadyWatchVideoDescription, true);
        AppData.getInstance().saveDataShared("", "already watch video", forWriting);

        Invoke("setSkipInteracteble", 0.5f);
		mMediaPlayer.ForceReplay ();
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
