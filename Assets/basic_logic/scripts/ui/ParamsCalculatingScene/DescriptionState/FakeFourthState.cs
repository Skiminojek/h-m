﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FakeFourthState : MonoBehaviour
{
	[SerializeField]
	Toggle mMeasurementsToggle, mGenderMan, mGenderWoman;

	[SerializeField]
	Text mHeightInputDescription;

	[SerializeField]
	Text mLeftText, mRightText;

	[SerializeField]
	Button mDoneButton;

	[SerializeField]
	Text mHintText;

	[SerializeField]
	InputField mHeightInputFieldCM, mHeightInputFieldFT, mHeightInputFieldIN;

	private Color mBaseMeasurementsColor = new Color(0.582f, 0.582f, 0.582f);
	private Color mActiveMeasurementsColor = new Color(0.543f, 0.492f, 0.726f);

	void Awake()
	{
		string measurements = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.Shared.mMeasurementsAttribute, DataAttributes.Shared.mMeasurements_IN_LBS);
		mMeasurementsToggle.isOn = DataAttributes.Shared.mMeasurements_IN_LBS == measurements;
		mMeasurementsToggle.interactable = false;
		mGenderMan.interactable = false;
		mGenderWoman.interactable = false;
		mDoneButton.interactable = false;

		mLeftText.color = mMeasurementsToggle.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mRightText.color = !mMeasurementsToggle.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;

		mHeightInputFieldCM.gameObject.SetActive (!mMeasurementsToggle.isOn);
		mHeightInputFieldFT.gameObject.SetActive (mMeasurementsToggle.isOn);
		mHeightInputFieldIN.gameObject.SetActive (mMeasurementsToggle.isOn);

		mHeightInputFieldCM.interactable = false;
		mHeightInputFieldFT.interactable = false;
		mHeightInputFieldIN.interactable = false;

		string gender = AppData.getInstance ().getData<string> (eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, DataAttributes.Shared.mGenderMale);
        InfoInputState.eGenderType mGenderType = (gender == DataAttributes.Shared.mGenderMale) ? InfoInputState.eGenderType.E_MALE : InfoInputState.eGenderType.E_FEMALE;
		mGenderWoman.isOn = InfoInputState.eGenderType.E_MALE != mGenderType;
		mGenderMan.isOn = InfoInputState.eGenderType.E_MALE == mGenderType;

		mGenderMan.GetComponentInChildren<Text> ().color = mGenderMan.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;
		mGenderWoman.GetComponentInChildren<Text> ().color = mGenderWoman.isOn ? mActiveMeasurementsColor : mBaseMeasurementsColor;

		initTexts ();
	}

	private void initTexts()
	{
		mHintText.text = StringsManager.getString ("INFO_INPUT_STATE_DESCRIPTION");

		mHeightInputDescription.text = StringsManager.getString ("INFO_INPUT_STATE_HEIGHT_PLACEHOLDER");
		mHeightInputFieldCM.placeholder.GetComponent<Text>().text = StringsManager.getString("INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_CM_KG");
		mHeightInputFieldFT.placeholder.GetComponent<Text>().text = StringsManager.getString("INFO_INPUT_STATE_HEIGHT_INPUT_FT");
		mHeightInputFieldIN.placeholder.GetComponent<Text>().text = StringsManager.getString("INFO_INPUT_STATE_HEIGHT_INPUT_IN");

		mGenderMan.GetComponentInChildren<Text> ().text = StringsManager.getString ("INFO_INPUT_STATE_GENDER_TOGGLE_MAN");
		mGenderWoman.GetComponentInChildren<Text> ().text = StringsManager.getString ("INFO_INPUT_STATE_GENDER_TOGGLE_WOMAN");

		mLeftText.text = StringsManager.getString ("INFO_INPUT_STATE_HEIGHT_INPUT_IN");
		mRightText.text = StringsManager.getString ("INFO_INPUT_STATE_MEASUREMENTS_TOGGLE_CM_KG");

		mDoneButton.GetComponentInChildren<Text> ().text = StringsManager.getString ("INFO_INPUT_STATE_DONE_BUTTON");
	}
}
