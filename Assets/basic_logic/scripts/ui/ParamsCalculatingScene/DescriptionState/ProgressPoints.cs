﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ProgressPoints : MonoBehaviour 
{
	private Vector2 mFixedPosition;
	private RectTransform mRectTransform;

	[SerializeField]
	private List<Image> mPoints;

	// Use this for initialization
	void Start () 
	{
		mRectTransform = GetComponent<RectTransform> ();
		mFixedPosition = mRectTransform.anchoredPosition;
	}

	void LateUpdate()
	{
		mRectTransform.anchoredPosition = mFixedPosition;
	}

	public void setState(int state)
	{
		for (int i = 0; i < mPoints.Count; ++i) 
		{
			mPoints [i].CrossFadeAlpha (i == state ? 1.0f : 0.0f, 0.3f, true);
		}
	}
}
