﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class DescriptionsDragger : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[SerializeField]
	RectTransform mTopLevelTransform;

	[SerializeField]
	UIController mUIController;

	//Ьщ
	private bool mIsInMoveing;
	private float mStartCorrectionTime;
	private float mWholeCorrectionTime = 0.5f;
	private bool mIsInteracteble = true;

	[SerializeField]
	private List<GameObject> mHints;
	Vector2 mHintSize;

	private float mXMinPos;
	private float mXMaxPos;
	private float mDragOffset;

	Vector2 mStartTouchPos;
	Vector2 mDirection;
	Vector2 mSpeedPos = Vector2.zero;
	float mCurrentSpeed = 0.0f;
	float speedCheckInterval = 0.05f;
	float speedDecreasValue = 1.0f;

	private CanvasGroup mCanvasGroup;

	private RectTransform mRectTransform;

	private List<Vector2> mControlPoints;

	private Vector2 mEndDragPos;

	private Vector2 mAutoCorrectionPos;

	[SerializeField]
	ProgressPoints mProgressPoints;

	private float mOffsetRatio;
	private float mMinRatio = 1.33333f;

	/*===============================================================================================
    * 
    * MonoBehaviour implementation
    * 
    * =============================================================================================*/

	void Start () 
	{
		float aRatio = mMinRatio / (Screen.height / (float)Screen.width);
		mOffsetRatio = 1 - ( (1 - aRatio) / 2);

		mHintSize = new Vector2 (mHints [0].transform.Find ("Image").GetComponent<RectTransform> ().rect.height, mHints [0].transform.Find ("Image").GetComponent<RectTransform> ().rect.width);
		mXMinPos = 0.0f;
		mXMaxPos = mHintSize.y * (mOffsetRatio - (1 - mOffsetRatio)) * mHints.Count;

		mCanvasGroup = GetComponent<CanvasGroup>();
		mIsInMoveing = false;

		mRectTransform = GetComponent<RectTransform> ();

		initDragger ();
		placeHints ();
	}

	void Update ()
	{
		if (true == mIsInMoveing)
		{
			//float t = (Time.time - mStartCorrectionTime) / mWholeCorrectionTime;
			//mRectTransform.anchoredPosition = Vector2.Lerp(mEndDragPos, mAutoCorrectionPos, 1 - Mathf.Pow(1 - t, 2.0f));
			mRectTransform.anchoredPosition += new Vector2(mCurrentSpeed * Time.deltaTime, 0.0f);
			mCurrentSpeed *= 0.95f;

			float clamped = Mathf.Clamp (mRectTransform.anchoredPosition.x, Mathf.Min (mDragOffset + mStartTouchPos.x, mAutoCorrectionPos.x), Mathf.Max (mDragOffset + mStartTouchPos.x, mAutoCorrectionPos.x));
			mRectTransform.anchoredPosition = new Vector2 (clamped, 0.0f);
			if (mRectTransform.anchoredPosition == mAutoCorrectionPos)
			{
				mRectTransform.anchoredPosition = mAutoCorrectionPos;
				mIsInMoveing = false;

				if (Mathf.Round(mAutoCorrectionPos.x) == Mathf.Round(mXMinPos)) 
				{
					JSONObject data = new JSONObject ();
					data.AddField ("is_description_showed", 1);
					AppData.getInstance ().saveDataGlobal ("", "DescriptionDragger", data);
					mUIController.setNewFlowState (eFlowState.E_STATE_INFO_INPUT, 0);
				}
			}
		}
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Draggers interfaces implementation
	* 
	* =============================================================================================*/

	public void OnBeginDrag(PointerEventData eventData)
	{
		checkSpeed ();

		if (false == mIsInteracteble)
			return;

		mIsInMoveing = false;

		Vector2 startTouchPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(mTopLevelTransform, eventData.position, eventData.pressEventCamera, out startTouchPos);

		mStartTouchPos = startTouchPos;
		mDragOffset = mRectTransform.anchoredPosition.x - startTouchPos.x;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (false == mIsInteracteble)
			return;

		Vector2 touchPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(mTopLevelTransform, eventData.position, eventData.pressEventCamera, out touchPos);

		float xPos = Mathf.Clamp (mDragOffset + touchPos.x, mXMinPos, mXMaxPos);

		Vector2 reff = Vector2.zero;
		Vector2 newPos = new Vector2 (xPos, 0.0f);
		mDirection = (newPos - mRectTransform.anchoredPosition).normalized;
		mRectTransform.anchoredPosition = Vector2.SmoothDamp (mRectTransform.anchoredPosition, newPos, ref reff, 0.015f, 10000, 1.0f);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		CancelInvoke ("checkSpeed");

		if (false == mIsInteracteble)
			return;

		mStartCorrectionTime = Time.time;
		mIsInMoveing = true;

		mEndDragPos = mRectTransform.anchoredPosition;
		mAutoCorrectionPos = getNearestControlPoint (mDirection, 0.0f);
	}

	void checkSpeed()
	{
		mCurrentSpeed =  ((mRectTransform.anchoredPosition.x - mSpeedPos.x) / speedCheckInterval);
		mSpeedPos = mRectTransform.anchoredPosition;
		Invoke ("checkSpeed", speedCheckInterval);
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Opened interface
	* 
	* =============================================================================================*/

	public void setInteracteble(bool isInteracteble)
	{
		mIsInteracteble = isInteracteble;
	}

	public GameObject getDescription(int index)
	{
		if (index >= mHints.Count)
			return null;

		return mHints [index];
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

	private void placeHints()
	{
		float startXPos = (-((mHints.Count / 2.0f) * mHintSize.y)) + (mHintSize.y / 2);
		for (int i = 0; i < mHints.Count; ++i) 
		{
			mHints[i].GetComponent<Mask>().rectTransform.sizeDelta = new Vector2(mHintSize.y * (mOffsetRatio - (1 - mOffsetRatio)), mHintSize.x);
			mHints [i].GetComponent<RectTransform> ().anchoredPosition = new Vector2 (((mHintSize.y * (mOffsetRatio - (1 - mOffsetRatio))) * i) + startXPos, 0);
		}
	}

	private void initDragger()
	{
		float rectXPos = ((mHintSize.y * mHints.Count) / 2.0f) - (mHintSize.y / 2);
		mRectTransform.sizeDelta = new Vector2 ((mHintSize.y * mHints.Count), mHintSize.x);
		mRectTransform.anchoredPosition = new Vector2 (rectXPos, 0.0f);

		mXMaxPos = mRectTransform.anchoredPosition.x;
		mXMinPos = mXMaxPos - (mHintSize.y * (mOffsetRatio - (1 - mOffsetRatio))) * (mHints.Count - 1);

		mControlPoints = new List<Vector2> (mHints.Count);

		for (int i = 0; i < mHints.Count; ++i) 
		{
			mControlPoints.Add (mRectTransform.anchoredPosition - (i * new Vector2(mHintSize.y * (mOffsetRatio - (1 - mOffsetRatio)), 0)));
		}
	}

	private Vector2 getNearestControlPoint(Vector2 direction, float speed)
	{
		Vector2 nearestPoint = mControlPoints [0];
		float nearestDistance = 100000.0f;
		int nearestIndex = 0;
		for (int i = 1; i < mControlPoints.Count; ++i) 
		{
			float currentDistance = Vector2.Distance (mRectTransform.anchoredPosition, mControlPoints [i]);
			if (currentDistance < nearestDistance) 
			{
				if (direction.x > 0) 
				{
					if (mControlPoints [i].x > mRectTransform.anchoredPosition.x) 
					{
						nearestPoint = mControlPoints [i];
						nearestDistance = currentDistance;
						nearestIndex = i;
					}
				} 
				else if(direction.x < 0)
				{
					if (mControlPoints [i].x < mRectTransform.anchoredPosition.x) 
					{
						nearestPoint = mControlPoints [i];
						nearestDistance = currentDistance;
						nearestIndex = i;
					}
				}
				else
				{
					nearestPoint = mEndDragPos;
				}
			}
		}

		mProgressPoints.setState (nearestIndex);

		return nearestPoint;
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
