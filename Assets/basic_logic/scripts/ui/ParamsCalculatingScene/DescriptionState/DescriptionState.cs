﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class DescriptionState : FlowUnitBasic
{
	/*===============================================================================================
    * 
    * MonoBehaviuor implementation
    * 
    * =============================================================================================*/

	private DescriptionsDragger mDescriptonDragger;


	[SerializeField]
	ProgressPoints mProgressPoints;

	[SerializeField]
	UIController mUIController;

    [SerializeField]
    soundsHandler mSoundHandler;

	void Awake()
	{
		mStateType = eFlowState.E_STATE_DESCRIPTION;

		mDescriptonDragger = GetComponent<DescriptionsDragger> ();
		initTexts ();
		initSkipButtonsCallbacks ();

    }

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/


	/*===============================================================================================
	* 
	* FlowUnitBasic implementation
	* 
	* =============================================================================================*/

	public override void setEnabled (bool isenabled, int substate)
	{
		gameObject.SetActive (isenabled);
		mProgressPoints.gameObject.SetActive (isenabled);
		mProgressPoints.setState (0);
	}

	public override void reset()
	{
	}

	/*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/
	private void initTexts()
	{
		GameObject description = mDescriptonDragger.getDescription (0).transform.Find("Image").gameObject;

		if (null != description) 
		{
			description.transform.Find ("Title").GetComponent<Text> ().text = StringsManager.getString ("DESCRIPTION_STATE_FIRST_PAGE_TITLE");
			description.transform.Find ("Description").GetComponent<Text> ().text = StringsManager.getString ("DESCRIPTION_STATE_FIRST_PAGE_DESCRIPTION");
		}

		description = mDescriptonDragger.getDescription (1).transform.Find("Image").gameObject;

		if (null != description)
		{
			description.transform.Find ("Title").GetComponent<Text> ().text = StringsManager.getString ("DESCRIPTION_STATE_SECOND_PAGE_TITLE");
			description.transform.Find ("Description").GetComponent<Text> ().text = StringsManager.getString ("DESCRIPTION_STATE_SECOND_PAGE_DESCRIPTION");
		}

		description = mDescriptonDragger.getDescription (2).transform.Find ("Image").gameObject;

		if (null != description)
		{
			description.transform.Find ("Title").GetComponent<Text> ().text = StringsManager.getString ("DESCRIPTION_STATE_THIRD_PAGE_TITLE");
			description.transform.Find ("Description").GetComponent<Text> ().text = StringsManager.getString ("DESCRIPTION_STATE_THIRD_PAGE_DESCRIPTION");
		}
	}

	private void initSkipButtonsCallbacks ()
	{
		mDescriptonDragger.getDescription (0).GetComponentInChildren<Button> ().onClick.AddListener (onSkipButtonClicked);
		mDescriptonDragger.getDescription (1).GetComponentInChildren<Button> ().onClick.AddListener (onSkipButtonClicked);
		mDescriptonDragger.getDescription (2).GetComponentInChildren<Button> ().onClick.AddListener (onSkipButtonClicked);
	}

	private void onSkipButtonClicked()
	{
		JSONObject data = new JSONObject ();
		data.AddField ("is_description_showed", 1);
		AppData.getInstance ().saveDataGlobal ("", "DescriptionDragger", data);

		mSoundHandler.beepClick();
		mUIController.setNewFlowState (eFlowState.E_STATE_INFO_INPUT, 0);        
	}
	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
