﻿using UnityEngine;
using System.Collections;

public class ScreenShotMaker : MonoBehaviour
{
	public delegate void ScreenshotMakeCalback(Texture2D tex);
    private static ScreenshotMakeCalback mScrrenCallback;

    private bool mIsScreenShot = false;

    private int resWidth = 1080;
    private int resHeight = 1920;

    [SerializeField]
    Camera mCamera;

    [SerializeField]
    TextMesh mText;

    [SerializeField]
    SpriteRenderer mSprite;

    /*===============================================================================================
    * 
    * OPENED INTERFACE
    * 
    * =============================================================================================*/

    void Awake()
    {
        mCamera = GetComponent<Camera>();
        mCamera.gameObject.SetActive(false);
     //   mText.text = StringsManager.getString("SHARING_TEXT");
		mText.text = StringParseHelper.replaceStar (mText.text, "\n");
    }

    public void getScreenShot(Texture2D texture, JSONObject description, ScreenshotMakeCalback callback)
	{
        mCamera.gameObject.SetActive(true);
        mSprite.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        mScrrenCallback = callback;
		mIsScreenShot = true;
	}
    /*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/

    /*===============================================================================================
    * 
    * Internal logic
    * 
    * =============================================================================================*/
	void LateUpdate ()
	{
		if (true == mIsScreenShot) 
		{
            RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
            mCamera.targetTexture = rt;
            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            mCamera.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            screenShot.Apply();
            mCamera.targetTexture = null;
            RenderTexture.active = null;
            Destroy(rt);

            if (null != mScrrenCallback)
                mScrrenCallback(screenShot);
            else
                DestroyImmediate(screenShot, true);

            mCamera.gameObject.SetActive(false);
            mIsScreenShot = false;
        }
	}
    /*===============================================================================================
    * 
    * END OF BLOCK
    * 
    * =============================================================================================*/
}
