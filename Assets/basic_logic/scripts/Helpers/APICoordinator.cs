﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class APICoordinator : MonoBehaviour {

    private static APICoordinator mInstance = null;

    private Texture2D mLastPhoto;

    private int mCurrentStep =0;
    private Texture2D mCurrentUserPhoto;
    private List<Image> mTestPhotos;

    private int mStepInt = 1;
    List<GameObject> mPostPhotoObjects;
    List<GameObject> mSubstateHintsObjects;
    List<GameObject> mPhotoButtons;
    List<Image> mInPhotoHintsObjects;

    const string mSendingMetaDate = "Sending metadata...";
    const string mSendingPhotos = "Sending photos...";
    const string mWaitingForResponse = "Waiting for response...";

    private Text mStateNotifierText;

    Action finalCallback;

    public delegate void APIUploadCallback(bool isWithoutError, string result);


    // API 3DLOOK PARAMS
    private string apiv2Url = "https://saia.3dlook.me/api/v2/persons/?head_build=false";
  //  private string apiv2Url = "https://saia.3dlook.me/api/v2/persons/"; //if we need head in obj
    private string apiv2Key = "ef54446744e2cf33c0d035cc2e933ccf666415dc";

    //singleton 
    public static APICoordinator getInstance()
    {
        if (null == mInstance)
        {
            GameObject singleton = new GameObject();
            singleton.name = "APICoordinator";
            mInstance = singleton.AddComponent<APICoordinator>();
            mInstance.mCurrentStep = 0;
        }

        return mInstance;
    }

    public void setNotifierText(Text notifier) { mStateNotifierText = notifier; }

    //main processing start - metadata sending, result url uses for photos sending
    public void sendData(APIUploadCallback finalCallback)
    {
        int userHeight = AppData.getInstance().getData<int>(eFlowState.E_STATE_INFO_INPUT, DataAttributes.UserParams.mHeightAttribute, 0, true);
        string gender = AppData.getInstance().getData<string>(eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, "male", false);

        JSONObject jsonSend = new JSONObject();
        jsonSend.AddField("gender", gender);
        jsonSend.AddField("height", userHeight);
        jsonSend.AddField("APIKey", apiv2Key);  //in datauploader method it will placed in header field

        mStateNotifierText.text = mSendingMetaDate;
        // SET 
        DataUploader.uploadTextData(apiv2Url, "Feedback", jsonSend, DataUploader.eMethodType.E_POST, (bool isWithoutError, string result) =>
        {
            if (true == isWithoutError && null != result && "" != result)
            {
                Debug.Log("send");
                Debug.Log(result);

                JSONObject resultJSON = new JSONObject(result);

                string mPhotosUrl = resultJSON.GetField("url").str;
                Debug.Log("received url is " + mPhotosUrl);
                StartCoroutine(processingPhotos(mPhotosUrl + "?head_build=false", finalCallback));
              //  StartCoroutine(processingPhotos(mPhotosUrl, finalCallback)); //if we need head in obj


            }
            else
            {
                //error
                Debug.Log("Cant get data : " + result);
                mStateNotifierText.text = "Metadata - Error :" + result;

            }

        });

        
    }
    //photos send and processing
    private IEnumerator processingPhotos(string url, APIUploadCallback callback)
    {
        Debug.Log("Start : " + url);
        Debug.Log(Application.persistentDataPath + "/photos/stage_1.png");

        mStateNotifierText.text = mSendingPhotos;

        if (!File.Exists(Application.persistentDataPath + "/photos/stage_1.png")) Debug.Log("file not found");

        byte[] frontPhoto = File.ReadAllBytes(Application.persistentDataPath + "/photos/stage_1.png");
        byte[] sidePhoto = File.ReadAllBytes(Application.persistentDataPath + "/photos/stage_2.png");

        WWWForm form = new WWWForm();
        form.AddBinaryData("front_image", frontPhoto, "front.jpg",  "image/jpeg");
        form.AddBinaryData("side_image", sidePhoto, "side.jpg", "image/jpeg");


        Dictionary<string, string> headers = form.headers;
        string outString;
        headers.TryGetValue("Content-Type", out outString); 
        Debug.Log(outString);

        headers.Add("X-HTTP-Method-Override", "PUT");
        headers.Add("Authorization", "ApiKey " + apiv2Key);

        WWW www = new WWW(url, form.data, headers);

        yield return www;
        Debug.Log(www.ToString());
        if (www.error == null)
        {

            JSONObject resultJSON = new JSONObject(www.text);

            Debug.Log("success");
            JSONObject forwriting = new JSONObject();

            string urlLocation;
            www.responseHeaders.TryGetValue("location", out urlLocation);
            StartCoroutine(pendingResult(urlLocation, callback));

        }
        else
        {
            callback(false, www.error);
            new MobileNativeMessage("ERROR", "Photos and data hasnt be saved");
            Debug.Log(www.error);
            mStateNotifierText.text = "Error : " + www.error ;

        }


    }
    //waiting for final result
    IEnumerator pendingResult(string url, APIUploadCallback callback)
    {
        mStateNotifierText.text = mWaitingForResponse;
        bool isReady = false;
        bool isSuccess = false;
        bool isFinalAnswer = false;

        while (!isFinalAnswer)
        {
            DataUploader.removeAllUploaders();
            Debug.Log(url);
            JSONObject apikey = new JSONObject();
            apikey.AddField("APIKey", apiv2Key);

            // MAIN BLOCK
            DataUploader.uploadTextData(url, "pending result", apikey, DataUploader.eMethodType.E_GET, (bool iswithouterror, string result) => 
            {
                Debug.Log(result);
                JSONObject resultJSON = new JSONObject(result);

                if (!resultJSON.HasField("is_ready")) { mStateNotifierText.text = "success"; saveCompleteJSONAnswer(resultJSON); isReady = true; isFinalAnswer = true; callback(true, result); }  // callback returns to method in PhotoMakingLogic - callbackFromAPI

                if (resultJSON.HasField("is_ready")) { isReady = resultJSON.GetField("is_ready").b; isSuccess = resultJSON.GetField("is_successful").b; }

                if (isReady && !isSuccess) { isFinalAnswer = true; callback(isSuccess, result); }

            });

            yield return new WaitForSeconds(2f);
          
        }
    }
    //SAVING RESULT JSON
    private void saveCompleteJSONAnswer(JSONObject resultJSON)
    {
        JSONObject toWrite = new JSONObject();
        toWrite.AddField(DataAttributes.Shared.mAPIv2CompleteAnswerJSON, resultJSON);
        AppData.getInstance().saveDataShared("", "apiv2CompleteJSON", toWrite);
    }

    public void setStep(int i) { mCurrentStep = i; }
    public int getStep() { return mCurrentStep; }
    public void setPostPhotoObjects(List<GameObject> obj) { mPostPhotoObjects = obj; }
    public void setSubstateHintsObjects(List<GameObject> obj) { mSubstateHintsObjects = obj; }
    public void setInPhotoHintsObjects(List<Image> obj) { mInPhotoHintsObjects = obj; }
    public void setPhotoBtns(List<GameObject> obj) { mPhotoButtons = obj; }

    public Texture2D getCurrentUserPhoto() { return mCurrentUserPhoto; }

    //SAVING CURRENT STEP PHOTO
    public bool savePhotoFromStep(Texture2D photo, int step)
    {
        mCurrentUserPhoto = photo;
        string name = (step == 1) ? "stage_1.png" : "stage_2.png";
        if (null != mLastPhoto && false == Debug.isDebugBuild)
            GameObject.DestroyImmediate(mLastPhoto, true);

        mLastPhoto = photo; //if we dont need rotation comment next line
        mLastPhoto = TexturesUtilities.rotateTexture(mLastPhoto, true);

        if (false == Directory.Exists(Application.persistentDataPath + "/photos/"))
            Directory.CreateDirectory(Application.persistentDataPath + "/photos/");

        string write_path = Application.persistentDataPath + "/photos/" + name;
        Debug.Log("Save to - " + write_path);
        System.IO.File.WriteAllBytes(write_path, mLastPhoto.EncodeToJPG());
        return true;
       
    }
    //UI SWITCHING
    public void switchHints(int step)

    {
        Debug.Log("step received is " + step.ToString());
        foreach (GameObject el in mSubstateHintsObjects) { el.SetActive(false); }
        mSubstateHintsObjects[step].SetActive(true);
        foreach (Image el in mInPhotoHintsObjects) { el.gameObject.SetActive(false); }
        mInPhotoHintsObjects[step].gameObject.SetActive(true);
    }

    public void switchPostPhotoObj(bool isenable)
    {
        foreach (GameObject el in mPostPhotoObjects) el.SetActive(isenable);
    }

    public void setPhotoTexture(Image targetImage, Texture2D usedTexture)
    {

        Texture2D imageTex = usedTexture;
        Sprite fromCam = null;

        if (null != imageTex)
        {
            fromCam = Sprite.Create(imageTex, new Rect(0, 0, imageTex.width, imageTex.height), new Vector2(0.5F, 0.5F));
            targetImage.sprite = fromCam;
            targetImage.gameObject.SetActive(true);

            targetImage.GetComponent<AspectRatioFitter>().aspectRatio = targetImage.sprite.texture.width / (float)targetImage.sprite.texture.height;

            targetImage.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

        }
    }
    // FOR DEBUG PURPOSES IN EDITOR MODE
    public void setTestTextures(List<Image> testTextures)
    {
        mTestPhotos = testTextures;
    }
    //PROCESSING PHOTO from camera or mobile device gallery
    public Texture2D passScreen(Texture2D photo)
    {

            // gallery check block 
            string pathGallery = AppData.getInstance().getData<string>(eFlowState.E_SHARED, DataAttributes.Shared.mCurrentPhotoFromGallery, "");
            if (pathGallery != "")
            {

                string pathLast = pathGallery;
                //flush path
                JSONObject savePath = new JSONObject();
                savePath.AddField(DataAttributes.Shared.mCurrentPhotoFromGallery, "");
                AppData.getInstance().saveDataShared("", "galleryPhoto", savePath);
                //

                Texture2D texture = NativeGallery.LoadImageAtPath(pathLast, 1024, false, true, false);

                // ROTATE PART for gallery photos
                texture = TexturesUtilities.rotateTexture(texture, false);

            return texture;

            }
            //
            else
            if (true == Debug.isDebugBuild)
            {
                Debug.Log("we check it here");
                Texture2D tex = mTestPhotos[mCurrentStep].mainTexture as Texture2D;

            return tex;

            }
            else
            {
            return photo;
            }
       
    }

}
