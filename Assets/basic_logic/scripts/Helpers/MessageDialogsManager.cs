﻿using UnityEngine;
using System.Collections;
using System;

public static class MessageDialogsManager
{
	public delegate void DialogMassageCallback();

	public static void createNotRecognitionDialogWindow(Action callback, string msg = "", float angle = 0.0f)
	{
		bool isCustomText = msg != "";
		createWindow ("INCORRECT_PHOTO_DIALOG_WINDOW_TITLE", ((false == isCustomText) ? "INCORRECT_PHOTO_DIALOG_WINDOW_TEXT" : msg), callback, isCustomText, angle);
	}

	public static  void createNotConnectionDialogWindow(Action callback, float angle = 0.0f)
	{
		createWindow ("NOT_CONNECTION_DIALOG_WINDOW_TITLE", "NOT_CONNECTION_DIALOG_WINDOW_TEXT", callback, false, angle);
	}

	public static  void createUseCustomParamsInput( Action<MNDialogResult> callback, string msg = "", float angle = 0.0f)
	{
		bool isCustomText = msg != "";
		createYesNoDialog ("CUSTOM_PARAMS_INPUT_MENU_SUGGESTION_TITLE", ((false == isCustomText) ? "CUSTOM_PARAMS_INPUT_MENU_SUGGESTION_TEXT" : msg), callback, "YES_NO_DIALOG_MANUALY_TEXT", "YES_NO_DIALOG_TRY_AGAIN_TEXT", isCustomText, angle);
	}

	public static  void createUnhandeledErrorDialogWindow(Action callback, float angle = 0.0f)
	{
		createWindow ("NOT_HANDLED_ERROR_MSG_TITLE", "NOT_HANDLED_ERROR_MSG_TEXT", callback, false, angle);
	}

	public static  void createSocialNetworkLoginError(Action callback, float angle = 0.0f)
	{
		createWindow ("SOCIAL_ETWORK_LOGIN_ERROR_TITLE", "SOCIAL_ETWORK_LOGIN_ERROR_TEXT", callback, false, angle);
	}

	public static  void createNotConditionsAccepted(Action callback, float angle = 0.0f)
	{
		createWindow ("AUTHORIZATION_STATE_CONDITIONS_CHECKBOX_NOT_SWITCHED_TITLE", "AUTHORIZATION_STATE_CONDITIONS_CHECKBOX_NOT_SWITCHED_TEXT", callback, false, angle);
	}

	public static  void createUploadErrorWindow(Action callback, float angle = 0.0f)
	{
		createWindow ("UPLOAD_ERROR_MSG_TITLE", "UPLOAD_ERROR_MSG_TEXT", callback, false,  angle);
	}

	public static  void createPasswordsMatchError(Action callback, float angle = 0.0f)
	{
		createWindow ("CREATE_ACCOUNT_STATE_PASSWORDS_NOT_MATCH_TITLE", "CREATE_ACCOUNT_STATE_PASSWORDS_NOT_MATCH_TEXT", callback, false, angle);
	}




	public static  void createEmailAlreadyUsesOnServerError(Action callback, float angle = 0.0f)
	{
		createWindow ("EMAIL_ALREADY_USES_ON_THE_SERVER_TITLE", "EMAIL_ALREADY_USES_ON_THE_SERVER_TEXT", callback, false, angle);
	}

	public static  void createEmailFormatError(Action callback, float angle = 0.0f)
	{
		createWindow ("EMAIL_FORMAT_ERROR_TITLE", "EMAIL_FORMAT_ERROR_TEXT", callback, false, angle);
	}

	public static  void createEmptyUsernameFieldError(Action callback, float angle = 0.0f)
	{
		createWindow ("USER_FIELD_EMPTY_ERROR_TITLE", "USER_FIELD_EMPTY_ERROR_TEXT", callback, false, angle);
	}

	public static  void createEmptyPasswordFieldError(Action callback, float angle = 0.0f)
	{
		createWindow ("USER_FIELD_EMPTY_PASSWORD_TITLE", "USER_FIELD_EMPTY_PASSWORD_TEXT", callback, false, angle);
	}

	public static  void createWrongUserNameOrPasswordError(Action callback, float angle = 0.0f)
	{
		createWindow ("WRONG_USERNAME_OR_PASSWORD_ERROR_TITLE", "WRONG_USERNAME_OR_PASSWORD_ERROR_TEXT", callback, false, angle);
	}

	public static void createSkipCurrentTrainingFreeVersion(Action<MNDialogResult> callback, float angle = 0.0f)
	{
		createYesNoDialog ("TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FREE_DIALOG_TITLE", "TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FREE_DIALOG_TEXT", callback, "", "", false, angle);
	}

	public static void createSkipCurrentTrainingFullVersion(Action<MNDialogResult> callback, float angle = 0.0f)
	{
		createYesNoDialog ("TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FULL_DIALOG_TITLE", "TRAINING_STATE_EXERSISES_SUBSTATE_SKIP_FULL_DIALOG_TEXT", callback, "", "", false, angle);
	}

	public static void createApsentVideoError(Action callback, float angle)
	{
		createWindow ("APSENT_VIDEO_ERROR_TITLE", "APSENT_VIDEO_ERROR_TEXT", callback, false, angle);
	}

	public static void createVideoProcessingError(Action callback, float angle)
	{
		createWindow ("VIDEO_PROCESSING_ERROR_TITLE", "VIDEO_PROCESSING_ERROR_TEXT", callback, false, angle);
	}
		
	private static void createWindow(string title, string text, Action callback, bool isCustomText = false, float angle = 0.0f)
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
		{
			MobileNativeMessage dialog = new MobileNativeMessage(StringsManager.getString(title), ((false == isCustomText) ? StringsManager.getString(text) : text));
			dialog.OnComplete += callback;
		}
		else
		{
			Debug.Log (((false == isCustomText) ? StringsManager.getString(text) : text));
			callback();
		}
	}

	private static void createYesNoDialog(string title, string text, Action<MNDialogResult> callback, string customYesStringKey  = "",  string customNoStringKey = "", bool isCustomMsg = false, float angle = 0.0f)
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
		{
			if ("" == customYesStringKey) 
			{
				customYesStringKey = "YES_NO_DIALOG_YES_TEXT";
			}

			if ("" == customNoStringKey) 
			{
				customNoStringKey = "YES_NO_DIALOG_NO_TEXT";
			}

			MobileNativeDialog dialog = new MobileNativeDialog(StringsManager.getString(title), ((false == isCustomMsg) ? StringsManager.getString(text) : text),
				StringsManager.getString(customYesStringKey), StringsManager.getString(customNoStringKey));
			dialog.OnComplete += callback;
		}
		else
		{
			Debug.Log (((false == isCustomMsg) ? StringsManager.getString(text) : text));
			callback(MNDialogResult.YES);
		}
	}
}
