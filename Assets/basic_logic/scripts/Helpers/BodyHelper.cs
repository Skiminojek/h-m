﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BodyHelper
{
	public enum eBodyPart
	{
		E_HEAD,
		E_NECK,
		E_SHOULDERS,
		E_BREAST,
		E_VOLUME_PELVIS,
		E_VOLUME_WAIST
	}

	static public KeyValuePair<int, int> getParamRange(eBodyPart bodyPart)
	{
		switch (bodyPart) 
		{
		case eBodyPart.E_HEAD:
			{
				return new KeyValuePair<int, int> (0, 1000);
			}
		case eBodyPart.E_NECK:
			{
				return new KeyValuePair<int, int> (0, 1000);
			}
		case eBodyPart.E_SHOULDERS:
			{
				return new KeyValuePair<int, int> (0, 1000);
			}
		case eBodyPart.E_BREAST:
			{
				return new KeyValuePair<int, int> (65, 150);
			}
		case eBodyPart.E_VOLUME_PELVIS:
			{
				return new KeyValuePair<int, int> (65, 150);
			}
		case eBodyPart.E_VOLUME_WAIST:
			{
				return new KeyValuePair<int, int> (45, 150);
			}
		default:
			{
				Debug.LogError ("[BodyHelper] Got unhandeled body type - " + bodyPart);
				return new KeyValuePair<int, int> (0, 1000);
			}
		}
	}
}
