﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundsHandler : MonoBehaviour {

    [SerializeField]
    AudioSource mClickSound;

    [SerializeField]
    AudioSource mShutterSound;

    [SerializeField]
    AudioClip mClick;

    [SerializeField]
    AudioClip mShutter;


    public void beepClick()
    {
        mClickSound.PlayOneShot(mClick);
    }

    public void beepShutter()
    {
        mShutterSound.PlayOneShot(mShutter);
    }



}
