﻿using UnityEngine;
using System.Collections;

namespace DataAttributes
{
	public static class Authorization
	{


        public const string mCVDataAttribute = "cv_data";
        public const string mSideHashId = "side_photo_hash";
        public const string mFrontHashID = "front_photo_hash";

		public const string mServerRequestIDAttribute = "code";
		public const string mIsVideoDescriptionShowed = "is_video_description_showed";

		public const string mPlayerPrefsAttribute = "player_prefs";

		public const string mPlayerPrefsVersion = "player_prefs_version";
	}

	public static class UserParams
	{

		//Inputed height
		public const string mHeightAttribute = "inputted_height";

		//Inputted weight
		public const string mWeightAttribute = "inputted_weight";

		//Inputted age
		public const string mAgeAttribute = "inputted_age";

		//Body part sizes which will be got from server
		public const string mHeadAttribute = "HeadHeight";
		public const string mNeckAttribute = "NeckHeight";
		public const string mShouldersAttribute = "shoulders";
		public const string mBreastAttribute = "chest";
		public const string mPelvisAttribute = "hips";
		public const string mWaistAttribute = "waist";
		public const string mCurrentProgramVersionAttribute = "version";
		public const string mLastProgramVersionAttribute = "last_version";

		public const string mSizeGridJSONData = "size_grid_json";

	}

	public static class FBDataGrabber
	{

		public const string mAgeRangeAttribute = "age_range";
		public const string mGenderAttribute = "gender";
	}

	public static class Shared
	{

		//Selected measurements type
		public const string mMeasurementsAttribute = "measurements";
		public const string mMeasurements_CM_KG = "measurements_cm_kg";
		public const string mMeasurements_IN_LBS = "measurements_in_lbs";

		//attribute in JSON file which contains JSON object wih body part sizes
		public const string mParameters = "parameters";


		public const string mGenderMale = "male";
		public const string mGenderFemale = "female";
		public const string mFirstName = "first_name";
		public const string mLastName = "last_name";


        public const string mAlreadyWatchVideoDescription = "skip_video";

        public const string mCurrentPhotoFromGallery = "current_photo_from_gallery";

        public const string mAPIv2CompleteAnswerJSON = "apiv2_completeJSON";

    }

	public static class UIControllerAttributes
	{
		public const string mLastStateAttribute = "last_state_attrib";
		public const string mPreviousStateAttribute = "previous_state_attrib";
		public const string mLastSubStateAttribute = "last_substate_attrib";
		public const string mLastSceneAttribute = "last_scene_attrib";
		public const string mIsInStartOfApplicationAttribute = "is_on_start_of_application";
		public const string mLastApplicationFocusState = "last_application_focusState";
	}

	public static class DateAttributes
	{
		public const string mDateRequestAttribute = "https://backend.3dlook.me";
		public const string mEndSessionTimeAttribute = "end_session_time";
		public const string mStartSessionTimeAttribute = "time";

		public const string mYearAttribute = "year";
		public const string mMonthAttribute = "month";
		public const string mDayAttribute = "day";
		public const string mHourAttribute = "hour";
		public const string mMinuteAttribute = "minute";
		public const string mSecondAttribute = "second";
		public const string mStartApplicationDateAttribute = "start_date";
	}
}