﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class brandSizesHelper  {


    public static JSONObject getBrandsSizes()
    {
        string userGender = AppData.getInstance().getData<string>(eFlowState.E_STATE_INFO_INPUT, DataAttributes.FBDataGrabber.mGenderAttribute, DataAttributes.Shared.mGenderMale, true);
        JSONObject sizes = new JSONObject();


        JSONObject sizeGridJSON = AppData.getInstance().getData<JSONObject>(eFlowState.E_STATE_PHOTO_MAKING, DataAttributes.UserParams.mSizeGridJSONData, null, true);

        if (sizeGridJSON != null && sizeGridJSON.IsArray)
        {
            

            foreach (JSONObject element in sizeGridJSON.list)
            {
                // checking gender 

                string currentGender = element.GetField("gender").str;
                if (currentGender == userGender)
                {
                    sizes.Add(element);
                }
            }
        }
            return sizes;

    }

}
