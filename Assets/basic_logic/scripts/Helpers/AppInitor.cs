﻿using UnityEngine;
using System.Collections;

public class AppInitor : MonoBehaviour 
{
	public delegate void OnStringsUpdated();
	public static event OnStringsUpdated StringsUpdated;
	public static string version = "1.0.0";

	void Awake()
	{
		AppData.getInstance ().setCurrentUserID ("-1");
		updateStrings ();
		DontDestroyOnLoad (gameObject);
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	/*===============================================================================================
	* 
	* Opened interface
	* 
	* =============================================================================================*/



	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

	private void updateStrings()
	{

	}
/*
	private IEnumerator downloadStrings()
	{
		string strURL = DataAttributes.Authorization.mStringsPath + getstringPostfix();
		strURL = strURL.Trim();


		WWW downloader = new WWW(strURL);

		yield return downloader;

		if (string.IsNullOrEmpty(downloader.error))
		{
			string basePath = StringsManager.getPath(Application.systemLanguage);
			basePath = basePath.Remove (basePath.LastIndexOf ('_'));

			string write_path = string.Format("Assets/FitnesAppAssets/Resources/{0}{1}", basePath, getstringPostfix());
			System.IO.File.WriteAllBytes(write_path, downloader.bytes);
		}
		else
		{
			Debug.LogError ("[AppInitor.downloadStrings]ERROR - " + downloader.error);
		}

		if(null != StringsUpdated)
		{
			StringsUpdated();
		}
	}
    */
	private string getstringPostfix()
	{
		int index = (StringsManager.getPath(Application.systemLanguage)).LastIndexOf('_');
		string postfix = StringsManager.getPath (Application.systemLanguage).Substring (index);
		return postfix + ".txt";
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
