﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TexturesUtilities {

    public static void textureFlush(Image target, string basicTextureName = "whiteBack")
    {
        if (null != target && null != target.sprite && null != target.overrideSprite.texture && target.overrideSprite.texture.name != "whiteBack")
            GameObject.DestroyImmediate(target.overrideSprite.texture, true);

        if (null != target.sprite && null != target.sprite.texture && target.sprite.texture.name != basicTextureName)
            GameObject.DestroyImmediate(target.sprite.texture, true);
    }

    public static Texture2D getPhotoByStage(int stage)
    {
        
        string path = Application.persistentDataPath + "/photos/" + "stage_" + stage.ToString() + ".png";
        Debug.Log("PATH IS " + path);
		if (System.IO.File.Exists (path))
		
		{
			byte[] imageData = System.IO.File.ReadAllBytes (path);
			Texture2D imageTex = new Texture2D (2, 2, TextureFormat.RGB24, false);
			imageTex.LoadImage (imageData);
			return imageTex;
		}
		else
		{	Debug.Log ("Texture file not found");
			return null;
		}
    }


    public static Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }
}
