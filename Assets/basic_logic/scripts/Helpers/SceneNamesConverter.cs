﻿using UnityEngine;
using System.Collections;

public static class SceneNamesConverter
{
	public static string getSceneNameFromEnumType(UIController.eSceneType sceneType)
	{
		switch (sceneType) 
		{
		case UIController.eSceneType.E_SCENE_PARAMETERS_CALCULATING:
			{
				return "ParamsCalculatingScene";
			}
		default:
			{
				Debug.LogError ("[ScneNamesConverter] Got unhadeled scene type - " + sceneType);
				return "AuthorizationState";
			}
		}
	}
}
