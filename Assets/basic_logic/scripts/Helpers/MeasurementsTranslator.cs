﻿using UnityEngine;
using System.Collections;
using System;

public static class MeasurementsTranslator 
{
	public static float INToCM(float inch)
	{
		float result = inch / 0.393701f;
		result = (float)Math.Round(result, 1);
		return result;
	}

	public static float CMToIN(float cm)
	{
		float result = cm * 0.393701f;
		result = (float)Math.Round(result, 1);
		return result;
	}

	public static float KGToLBS(float lbs)
	{
		float result = lbs / 0.453592f;
		result = (float)Math.Round(result, 1);
		return result;
	}

	public static float LBSToKG(float lbs)
	{
		float result = lbs * 0.453592f;
		result = (float)Math.Round(result, 1);
		return result;
	}


    public static float FeetsInchesToCM(string heightString)
    {
        //FEETS TO CM
        int feetsHeight;
        int inchesHeight;
        float nonValid = -1.0f ;
      
        char[] charIncomming = heightString.ToCharArray();

        if (charIncomming.Length == 0) return nonValid;
        if ((charIncomming.Length < 3)&&(charIncomming.Length>0))
        {
            try
            {
                feetsHeight = int.Parse(charIncomming[0].ToString());
                inchesHeight = 0;
            }
            catch
            {
                return nonValid;
            }
        }
        else
        {
            try
            {
                char[] delimiterChars = { ' ', ',', '.', ':', '\t', '\'', '|' };
                string[] splittedString = heightString.Split(delimiterChars);
               feetsHeight =  int.Parse(splittedString[0]);
                inchesHeight = int.Parse(splittedString[1]);

            }
            catch
            {
                return nonValid;
            }

        }

             float heightValue = Mathf.Round(MeasurementsTranslator.INToCM(feetsHeight * 12 + inchesHeight));


        return heightValue;
    }

    public static string INToFeetsInches(float heightInIN)
    {
        // CM TO FEETS ' INCHES

        if (INToCM(heightInIN) > 302.0f) { return ""; }

        else
        {

            int feetsPart = Mathf.FloorToInt(heightInIN / 12);
            int inchPart = Mathf.RoundToInt(heightInIN - (feetsPart * 12));
            if (inchPart == 12) { inchPart = 0; feetsPart++; }

            string result = feetsPart.ToString() + "\'" + inchPart.ToString();
            return result;

        }
    }

	public static string convertSecondsToString(int timeInSeconds)
	{
		string currentTime = string.Format("00:00");

		if (timeInSeconds < 60) 
		{
			currentTime = (timeInSeconds < 10) ? string.Format ("00:0{0}", timeInSeconds) : currentTime = string.Format ("00:{0}", timeInSeconds);
		} 
		else if (timeInSeconds < 3600) 
		{
			int minutes = timeInSeconds / 60;
			int seconds = timeInSeconds - minutes * 60;

			string secondsString = (seconds < 10) ? string.Format ("0{0}", seconds) : string.Format ("{0}", seconds);
			string minutesString = (minutes < 10) ? string.Format ("0{0}", minutes) : string.Format ("{0}", minutes);

			currentTime = string.Format ("{0}:{1}", minutesString, secondsString);
		}
		else if (timeInSeconds >= 3600) 
		{
			int hours = timeInSeconds / 3600;
			int minutes = (timeInSeconds - (hours * 3600)) / 60;
			int seconds = timeInSeconds - (minutes + hours * 60) * 60;

			string secondsString = (seconds < 10) ? string.Format ("0{0}", seconds) : string.Format ("{0}", seconds);
			string minutesString = (minutes < 10) ? string.Format ("0{0}", minutes) : string.Format ("{0}", minutes);
			string hoursString = (hours < 10) ? string.Format ("0{0}", hours) : string.Format ("{0}", hours);

			currentTime =  string.Format ("{0}:{1}:{2}", hoursString, minutesString, secondsString);
		}

		return currentTime;
	}

}
