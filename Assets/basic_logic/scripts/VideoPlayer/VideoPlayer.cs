﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VideoPlayer : MonoBehaviour 
{
	[SerializeField]
	private MediaPlayerCtrl mVideoManager;

	[SerializeField]
	private RawImage mVideoStream;

    Vector2 mVideoStreamStartPos;
    Vector2 mVideoStreamStartSize;

    bool mIsFullScreenMode = false;

	/*===============================================================================================
	* 
	* MonoBehaviour implementation
	* 
	* =============================================================================================*/

	void Start () 
	{
        mVideoStreamStartPos = mVideoStream.GetComponent<RectTransform>().anchoredPosition;
        mVideoStreamStartSize = mVideoStream.GetComponent<RectTransform>().sizeDelta;
        mVideoStream.GetComponent<AspectRatioFitter>().aspectRatio = mVideoStreamStartSize.x / mVideoStreamStartSize.y;
	}

	void LateUpdate () 
	{
		mVideoStream.texture = mVideoManager.GetComponent<MeshRenderer> ().sharedMaterial.mainTexture;
	}

	/*===============================================================================================
	* 
	* END OFBLOCK
	* 
	* =============================================================================================*/


	/*===============================================================================================
	* 
	* Opened interface
	* 
	* =============================================================================================*/
    public void setFullScreenMode()
    {
        float factor = 1.3194f;
        mVideoStream.GetComponent<RectTransform>().sizeDelta = new Vector2(1920.0f, 1440.0f);
		mVideoStream.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero + new Vector2(0, -this.GetComponent<RectTransform> ().anchoredPosition.y);
        mVideoStream.GetComponent<RectTransform>().Rotate(new Vector3(0.0f, 0.0f, 90.0f));

        mIsFullScreenMode = true;
    }

    public void setPartScreenMode()
    {
        mVideoStream.GetComponent<RectTransform>().anchoredPosition = mVideoStreamStartPos;
        mVideoStream.GetComponent<RectTransform>().sizeDelta = mVideoStreamStartSize;
        mVideoStream.GetComponent<RectTransform>().rotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);

        mIsFullScreenMode = false;
    }

    public bool isFullScreenMode()
    {
        return mIsFullScreenMode;
    }
	/*===============================================================================================
	* 
	* END OFBLOCK
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* Internal logic
	* 
	* =============================================================================================*/

	/*===============================================================================================
	* 
	* END OFBLOCK
	* 
	* =============================================================================================*/
}
