﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class DataUploader : MonoBehaviour 
{
	static protected Dictionary<string, UploaderImplementation> mUploadersList = new Dictionary<string, UploaderImplementation> ();
	public delegate void UploadCallback(bool isWithoutError, string result);

	public enum eMethodType
	{
		E_POST,
		E_POST_STRING,
		E_GET,
		E_PUT
	}

	public static string uploadTextData(string uploadURL, string callerName, JSONObject data, eMethodType methodType, UploadCallback callback, bool isAuthTokenNeeded = true)
	{
		GameObject obj = new GameObject ();
		UploaderImplementation uploader = obj.AddComponent<UploaderImplementation> ();
		string id = Guid.NewGuid ().ToString ();
		uploader.name = callerName + "_uploader_" + id;

		uploader.setId (id);
		mUploadersList.Add (id, uploader);
		uploader.uploadTextData(uploadURL, callerName, data, methodType, callback, removeUploader, isAuthTokenNeeded);

		return id;
	}

	public static string uploadBinaryData(string uploadURL, string callerName, byte[] data, string fieldName, string fileName, string mimeType, UploadCallback callback, bool isAuthTokenNeeded = true, Dictionary<string, string> additionalParams = null)
	{
		GameObject obj = new GameObject ();
		UploaderImplementation uploader = obj.AddComponent<UploaderImplementation> ();
		string id = Guid.NewGuid ().ToString ();
		uploader.name = callerName + "_uploader_" + id;

		uploader.setId (id);
		mUploadersList.Add (id, uploader);
		uploader.uploadBinaryData(uploadURL, callerName, data, fieldName, fileName, mimeType, callback, removeUploader, isAuthTokenNeeded, additionalParams);

		return id;
	}

	public static void removeUploader(string id)
	{
		if (mUploadersList.ContainsKey (id)) 
		{
			GameObject.DestroyImmediate (mUploadersList [id]);
			mUploadersList.Remove (id);
		}
	}

    public static void removeAllUploaders()
    {
        foreach (var uploader in mUploadersList)
            GameObject.DestroyImmediate(uploader.Value);

        mUploadersList.Clear();
    }




	/*===============================================================================================
	* 
	* INNER IMPLEMENTATION
	* 
	* =============================================================================================*/

	protected class UploaderImplementation : MonoBehaviour 
	{
		private string mId;

		static DataUploader mInstance;

		public void uploadTextData(string uploadURL, string callerName, JSONObject data, DataUploader.eMethodType methodType, UploadCallback callback, Action<string> deletingCallback, bool isAuthTokenNeeded = true)
		{
			StartCoroutine (uploadTextDataCo(uploadURL, callerName, data, methodType, callback, deletingCallback, isAuthTokenNeeded));
		}

		public void uploadBinaryData(string uploadURL, string callerName, byte[] data, string fieldName, string fileName, string mimeType, UploadCallback callback, Action<string> deletingCallback, bool isAuthTokenNeeded = true, Dictionary<string, string> additionalParams = null)
		{
			StartCoroutine (uploadBinaryDataCo(uploadURL, callerName, data, fieldName, fileName, mimeType, callback, deletingCallback, isAuthTokenNeeded, additionalParams));
        }

		public void setId(string id)
		{
			mId = id;
		}

		private IEnumerator uploadTextDataCo (string uploadURL, string callerName, JSONObject data, DataUploader.eMethodType methodType, UploadCallback callback, Action<string> deletingCallback, bool isAuthTokenNeeded = true)
		{
            Debug.Log (string.Format("[{0}] For upload ==> link - {1}", callerName, uploadURL));

			WWW upload = null;

			switch (methodType) 
			{
			case DataUploader.eMethodType.E_POST:
				{

                        WWWForm uploadForm = null;
					fillTextForm (callerName, data, out uploadForm);

					Dictionary<string, string> headers = uploadForm.headers;

                        if (data.HasField("APIKey"))
                        {
                            headers.Add("Authorization", "ApiKey " + data.GetField("APIKey").str); //check auth. key in input json
                        }

                        //REMOVED FOR MEWA
                        //	if (true == isAuthTokenNeeded)
                        //		headers.Add ("Authorization", "Token " + AppData.getInstance ().getCurrentAuthKey());

                        //MEWA TOKEN
                        

                        upload = new WWW (uploadURL, uploadForm.data, headers);

					break;
				}
			case DataUploader.eMethodType.E_GET:
				{
					if (true == isAuthTokenNeeded) 
					{
						Dictionary<string, string> headers = new Dictionary<string, string>();
						headers.Add ("Content-Type", "application/x-www-form-urlencoded");
                            //	headers.Add ("Authorization", "Token " + AppData.getInstance ().getCurrentAuthKey());
                            //   headers.Add("Authorization", "Token " + "cbab26c04414288d81a33a9ad2ac41dc6746b7b2");
                            if (data.HasField("APIKey"))
                            {
                                headers.Add("Authorization", "ApiKey " + data.GetField("APIKey").str); //check auth. key in input json
                                Debug.Log("Received api key" + data.GetField("APIKey").str);
                            }

                            Debug.Log (string.Format ("To upload by GET request ==> {0}", uploadURL));
						upload = new WWW (uploadURL, null, headers);
					}
					else
					{
						Debug.Log (string.Format ("To upload by GET request ==> {0}", uploadURL));
						upload = new WWW (uploadURL);
					}
					break;
				}
			case DataUploader.eMethodType.E_POST_STRING:
				{
					Dictionary<string, string> headers = new Dictionary<string, string> ();
					headers.Add ("Content-Type", "application/json");
                    headers.Add("Authorization", "Token " + "cbab26c04414288d81a33a9ad2ac41dc6746b7b2");
                        string asString = data.ToString ();
					asString = asString.Replace ("{{", "{").Replace ("}}", "}").Replace("\"{", "{").Replace("}\"", "}");
					byte [] rawData = Encoding.UTF8.GetBytes (asString);
					upload = new WWW (uploadURL, rawData, headers);
					Debug.Log (string.Format ("To upload by PUT request ==> {0}", asString));

					break;
				}
			case DataUploader.eMethodType.E_PUT:
				{
					Dictionary<string, string> headers = new Dictionary<string, string> ();
					headers.Add ("Content-Type", "application/json");
					headers.Add ("X-HTTP-Method-Override", "PUT");

					if (true == isAuthTokenNeeded)
                            //	headers.Add ("Authorization", "Token " + AppData.getInstance ().getCurrentAuthKey());
                            headers.Add("Authorization", "Token " + "cbab26c04414288d81a33a9ad2ac41dc6746b7b2");

                        string asString = data.ToString ();
					asString = asString.Replace ("{{", "{").Replace ("}}", "}");
					byte [] rawData = Encoding.UTF8.GetBytes (asString);
					upload = new WWW (uploadURL, rawData, headers);
					Debug.Log (string.Format ("To upload by PUT request ==> {0}", asString));
					break;
				}
			default:
				{
					Debug.LogError ("[DataUploader.uploadTextData]Got unhandeled upload type - " + methodType);
					break;
				}
			}

			yield return upload;
            processUploadResult (upload, callerName, callback);

			if (Application.platform != RuntimePlatform.OSXEditor && Application.platform != RuntimePlatform.WindowsEditor)
				upload.Dispose ();

			if (null != deletingCallback) 
			{
				deletingCallback (mId);
			}
		}

		private void fillTextForm(string callerName, JSONObject data, out WWWForm uploadForm)
		{
			uploadForm = new WWWForm ();

			foreach (string key in data.keys) 
			{
				JSONObject obj = data.GetField (key);

				if (null != obj) 
				{
					switch (obj.type) 
					{
					case JSONObject.Type.BOOL:
						{
							uploadForm.AddField (key, obj.b.ToString());
							Debug.Log (string.Format("[{0}] To upload(text data) ==> <attribute - {1}, value - {2}>", callerName, key, obj.b));
							break;
						}
					case JSONObject.Type.NUMBER:
						{
							uploadForm.AddField (key, obj.n.ToString());
							Debug.Log (string.Format("[{0}] To upload(text data) ==> <attribute - {1}, value - {2}>", callerName, key, obj.n));
							break;
						}
					case JSONObject.Type.STRING:
						{
							uploadForm.AddField (key, obj.str);
							Debug.Log (string.Format("[{0}] To upload(text data) ==> <attribute - {1}, value - {2}>", callerName, key, obj.str));
							break;
						}
					case JSONObject.Type.OBJECT:
						{
							uploadForm.AddField (key, obj.ToString());
							Debug.Log (string.Format("[{0}] To upload(text data) ==> <attribute - {1}, value - {2}>", callerName, key, obj.str));
							break;
						}
					default:
						{
							Debug.LogError (string.Format ("[{0}](text data)Unhandeled data type - {1}", callerName, obj.type));
							break;
						}
					}
				}
				else
				{
					Debug.LogError (string.Format ("[{0}](text data)data not contains field - {1}", callerName, key));
					break;
				}
			}
		}

        private void Update()
        {
            if(null != up)
            {
                //Debug.Log("progress - " + (float)up.progress);

                if (up.progress >= 1.0f)
                    up = null;
            }
        }

        WWW up;

        private IEnumerator uploadBinaryDataCo(string uploadURL, string callerName, byte[] data, string fieldName, string fileName, string mimeType, UploadCallback callback, Action<string> deletingCallback, bool isAuthTokenNeeded = true, Dictionary<string, string> additionalParams = null)
		{
			Debug.Log (string.Format("[{0}] To upload(binary data) ==> (uploadurl - {1})", callerName, uploadURL));
			WWWForm uploadForm = null;
			fillBinaryForm (callerName, data, fieldName, fileName, mimeType, out uploadForm, additionalParams);

			Dictionary<string, string> headers = uploadForm.headers;

            //adding for new server
            headers.Add("Content-Disposition",  "attachment; filename=" + fileName);
            //

            if (true == isAuthTokenNeeded)
                //	headers.Add ("Authorization", "Token " + AppData.getInstance ().getCurrentAuthKey());
                headers.Add("Authorization", "APIKey " + "cbab26c04414288d81a33a9ad2ac41dc6746b7b2");

            WWW upload = new WWW (uploadURL, uploadForm.data, headers);
            up = upload;
            yield return upload;
			processUploadResult (upload, callerName, callback);

			if (Application.platform != RuntimePlatform.OSXEditor && Application.platform != RuntimePlatform.WindowsEditor)
				upload.Dispose ();

			if (null != deletingCallback) 
			{
				deletingCallback (mId);
			}
		}

		private void fillBinaryForm(string callerName, byte[] data, string fieldName, string fileName, string mimeType, out WWWForm uploadForm, Dictionary<string, string> additionalParams = null)
		{
			uploadForm = new WWWForm ();
			Debug.Log (string.Format("[{0}] To upload(binary data) ==> (fieldName - {1}, fileName - {2}, mimeType - {3})", callerName, fieldName, fileName, mimeType));
			uploadForm.AddBinaryData(fieldName, data, fileName, mimeType);
	
			if (null != additionalParams) 
			{
				foreach (var param in additionalParams) 
				{
					uploadForm.AddField (param.Key, param.Value);
					Debug.Log (string.Format("[{0}] To upload(Additional paramsa) ==> (key - {1}, value - {2})", callerName , param.Key, param.Value));
				}
			}
		}

		private void processUploadResult(WWW upload, string callerName, UploadCallback callback)
		{
			if (upload.error == null)
			{
				Debug.Log(string.Format("[{0}] upload done!", callerName));
				Debug.Log (string.Format("[{0}] Got JSON string  = {1}", callerName, upload.text));
				callback (true, upload.text);
			}
			else
			{
				Debug.Log(string.Format("[{0}]Error during upload: {1}", callerName, upload.error));
				Debug.Log(string.Format("[{0}]MSG: {1}", callerName, upload.text));
				callback (false, upload.text);
			}

			GameObject.Destroy (gameObject);
		}
	}

	/*===============================================================================================
	* 
	* END OF BLOCK
	* 
	* =============================================================================================*/
}
