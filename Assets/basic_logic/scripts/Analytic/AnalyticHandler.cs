﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticHandler {

    public static string aTopicStart = "Start";
    public static string aTopicEvent = "Event";
    public static string aTopicUser= "User";
    public static string aTopicFlow = "Flow event";
    public static string aTopicNavigation = "Navigation";
    public static string aTopicErrors = "Errors";
    public static string aTopicPhoto = "Photo";

    public static string aFlowMode = "Selected flow";
    public static string aNavClothes = "Clothes";
    public static string aFLowSelected = "Selected flow type";
    public static string aPhotoStatus = "Photo passed";
    public static string aManualInput = "Manual input";
    public static string aPhotoError = "Photo error";
    public static string aPhotoErrorCantRecognize = "Cant recognize";
    public static string aPhotoErrorErrorWhileUpload = "Error while upload";
    public static string aPhotoErrorUnhandled = "Unhandled error";

    public static void sendEvent(string eventName, Dictionary<string, object> eventData)
    {
        Analytics.CustomEvent(eventName, eventData);
    }


    


}
