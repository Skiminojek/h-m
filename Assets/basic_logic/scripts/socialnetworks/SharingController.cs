﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SharingController : MonoBehaviour
{
    [SerializeField]
    ScreenShotMaker mScreenShotMaker;

    [SerializeField]
    SharingManager mSharingManager;

    Action mShareCompleteCallback;
    SharingManager.eSharingType mShareType;
    SharingManager.eActionType mActionType;

    public void shareTexture(SharingManager.eSharingType shareTo, Texture2D texture, JSONObject additionalInfo, Action onSahreComplete)
    {
        mShareCompleteCallback = onSahreComplete;
        mShareType = shareTo;
        mActionType = SharingManager.eActionType.E_ACTION_PHOTO;
        TextureScale.Bilinear(texture, 1080, 1920);
        mScreenShotMaker.getScreenShot(texture, additionalInfo, (t)=> 
        {
            onScreenShotReady(t);
            GameObject.DestroyImmediate(texture, true);
        } );
    }

    public bool isInstalled(SharingManager.eSharingType shareTo)
    {
        return mSharingManager.isInstalled(shareTo);
    }

    private void onScreenShotReady(Texture2D tex)
    {
        string pathForPersist = Application.temporaryCachePath + "/temp";
        if (!System.IO.Directory.Exists(pathForPersist))
            System.IO.Directory.CreateDirectory(pathForPersist);

        string imageName = pathForPersist + "/sharing_image.png";
        System.IO.File.WriteAllBytes(imageName, tex.EncodeToPNG());

        mSharingManager.share(mShareType, mActionType, imageName, (error)=> { if(null != mShareCompleteCallback) mShareCompleteCallback(); });
    }
}
