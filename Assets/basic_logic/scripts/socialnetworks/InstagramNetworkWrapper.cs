﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace SocialNetwork
{
	public class InstagramNetworkWrapper : MonoBehaviour, ISocialNetworkConnectorBasic
	{
		/*private InitCallback mInitCallback = null;
		private LogINCallback mLogInCallback = null;
		private LogOutCallback mLogOutCallback = null;
		private ShareCallback mShareCallback = null;

		private bool mIsInitted = true;*/

		/*===============================================================================================
        * 
        * MonoBehaviour implementation
        * 
        * =============================================================================================*/

		void Start () 
		{
            init((eErrorType error) => {});
		}


		void Update () 
		{

		}

		/*===============================================================================================
        * 
        * END OF BLOCK
        * 
        * =============================================================================================*/


		/*===============================================================================================
		* 
		* ISocialNetworkConnectorBasic implementation
		* 
		* =============================================================================================*/

		public void init(InitCallback callback)
		{
			/*mInitCallback = callback;

			InstagramAPI.Instance.OnLogin += logInSuccessCallback;
			InstagramAPI.Instance.OnLoginCanceled += logInCancelCallback;
			mIsInitted = true;
			mInitCallback (eErrorType.E_ERROR_NONE);
			mInitCallback = null;*/
		}

		public void logIN(LogINCallback callback)
		{
			/*mLogInCallback = callback;

			if (false == mIsInitted)
			{
				Debug.LogError ("[InstagramNetworkWrapper] not initialize(first you have to call \"init\" function)");
				mLogInCallback (eErrorType.E_ERROR_NOT_INITTED);
				mLogInCallback = null;
				return;
			}

			string[] permissions = new string[3];
			permissions[0] = "basic";
			permissions[1] = "public_content";
			permissions[2] = "comments";

			InstagramAPI.Instance.Auth ("http://3dlook.me", permissions);*/
		}

		public void logOut(LogOutCallback callback)
		{
			//callback (eErrorType.E_ERROR_NOT_IMPLEMENTED);
		}

		public void sharePicture(ShareCallback callback, string imagePath)
		{
            if (false == InstagramShareIOS.isInstalled())
                return;

			/*if (false == mIsInitted)
			{
				Debug.LogError ("[InstagramNetworkWrapper] not initialize(first you have to call \"init\" function)");
				return;
			}

			if (false == isAuthorized())
			{
				Debug.LogError ("[InstagramNetworkWrapper] not Authorized(first you have to call \"logIN\" function)");
				return;
			}*/

			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				InstagramShareIOS.PostToInstagramPicture("", imagePath);
			}
		}

		public void shareVideo(ShareCallback callback, string videoPath)
        {
            /*if (false == mIsInitted)
            {
                Debug.LogError ("[InstagramNetworkWrapper] not initialize(first you have to call \"init\" function)");
                return;
            }

            if (false == isAuthorized())
            {
                Debug.LogError ("[InstagramNetworkWrapper] not Authorized(first you have to call \"logIN\" function)");
                return;
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
				Debug.Log("[InstagramNetworkWrapper] Create screenshot");
				InstagramShareIOS.PostToInstagramVideo("", videoPath);
            }*/
        }

		public bool isInit()
		{
			return false;
		}

		public bool isAuthorized()
		{
			return false;//Instagram.InstagramAPI.Instance.IsLoggedIn;
		}

		public bool isInstalled()
		{
			return InstagramShareIOS.isInstalled ();
		}

		/*===============================================================================================
        * 
        * END OF BLOCK
        * 
        * =============================================================================================*/

		/*===============================================================================================
		* 
		* CALLBACKS
		* 
		* =============================================================================================*/
		/*private void logInSuccessCallback(InstagramAPI sender)
		{
			if (null != mLogInCallback) 
			{
				Debug.Log ("[InstagramNetworkWrapper] Authorization complete");
				mLogInCallback (eErrorType.E_ERROR_NONE);
				mLogInCallback = null;
			}
		}

		private void logInCancelCallback(InstagramAPI sender)
		{
			if (null != mLogInCallback) 
			{
				Debug.Log ("[InstagramNetworkWrapper] Authorization canceled");
				mLogInCallback (eErrorType.E_ERROR_NOT_AUTORIZED);
				mLogInCallback = null;
			}
		}*/
		/*===============================================================================================
		* 
		* END OF BLOCK
		* 
		* =============================================================================================*/
	}
}

