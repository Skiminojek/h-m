﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;

namespace SocialNetwork
{
    public class VKNetworkWrapper : MonoBehaviour, ISocialNetworkConnectorBasic
    {
        private InitCallback mExternalInitCallback = null;
        private LogINCallback mExternalLogInCallback = null;
        private LogOutCallback mExternalLogOutCallback = null;
        private ShareCallback mExternalShareCallback = null;

        private AccessToken mCurrentAccessToken = null;
        private List<string> mNeededpermissions;

        const string mPersistTokenAttribName = "FBAccessToken";

		[SerializeField]
		//private VK mVK;

        /*===============================================================================================
        * 
        * ISocialNetworkConnectorBasic implementation
        * 
        * =============================================================================================*/

        public void init (InitCallback callback)
        {
            /*Debug.Log("[FBNetworkWrapper] Request to init");

			mNeededpermissions = new List<string> () { "publish_actions" };
            mExternalInitCallback = callback;

            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback);
            } 
            else
            {
                Debug.Log("[FBNetworkWrapper] Already initialized, signal an app activation App Event");
                FB.ActivateApp();
                mExternalInitCallback(eErrorType.E_ERROR_NONE);
                mExternalInitCallback = null;
            }*/
        }
            
        public void logIN(LogINCallback callback)
        {
            /*Debug.Log("[FBNetworkWrapper] Request to logIn");
            mExternalLogInCallback = callback;

            if (false == FB.IsLoggedIn)
            {
				FB.LogInWithPublishPermissions(mNeededpermissions, AuthCallback);
            }
            else
            {
                Debug.Log("[FBNetworkWrapper] Already LogIn");
                mCurrentAccessToken = Facebook.Unity.AccessToken.CurrentAccessToken;
                mExternalLogInCallback(eErrorType.E_ERROR_NONE);
                mExternalLogInCallback = null;
            }*/
        }

        public void logOut(LogOutCallback callback)
        {
            ///FB.LogOut();
            //callback(eErrorType.E_ERROR_NONE);
        }

		public void sharePicture(ShareCallback callback, string picturePath)
		{
			//FBShareDialog.OpenFBShareDilaog (picturePath);
		}

		public void shareVideo(ShareCallback callback, string videoPath)
		{
			//FBShareDialog.OpenFBShareDilaogVideo (videoPath);
		}

		public bool isInit()
		{
			return true;
		}

		public bool isAuthorized()
		{
			return true;
		}

		private void FBCallback(IGraphResult result)
		{
			//Debug.Log ("[FBNetworkWrapper]FB SHARING COMPLETE. Result = " + result);
		}

		private void FBCallback(ILoginResult result)
		{
			//Debug.Log ("[FBNetworkWrapper]FB ILoginResult COMPLETE. Result = " + result);
		}

		public bool isInstalled()
		{
			return true;
		}

        /*===============================================================================================
        * 
        * END OF BLOCK
        * 
        * =============================================================================================*/


        /*===============================================================================================
        * 
        * Internal logic
        * 
        * =============================================================================================*/

        private void InitCallback ()
        {
            /*if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                Debug.Log("[FBNetworkWrapper] Init complete");
                FB.ActivateApp();
                mExternalInitCallback(eErrorType.E_ERROR_NONE);
            }
            else
            {
                Debug.Log("[FBNetworkWrapper] Failed to Initialize the Facebook SDK");
                mExternalInitCallback(eErrorType.E_ERROR_SOME_INTERNAL_ERROR);
            }

            mExternalInitCallback = null;*/
        }

        private void AuthCallback (ILoginResult result)
        {
			/*if (null == result.Error) 
            {
                // AccessToken class will have session details
                mCurrentAccessToken = Facebook.Unity.AccessToken.CurrentAccessToken;

                // Print current access token's User ID
                Debug.Log("[FBNetworkWrapper] User Id - " + mCurrentAccessToken.UserId);

                // Print current access token's granted permissions
                Debug.Log("[FBNetworkWrapper] access token's granted permissions");

                string permissions = mCurrentAccessToken.Permissions.ToCommaSeparateList();
                List<string> permList = new List<string>(permissions.Split(','));
                foreach (string perm in permList)
                {
                    Debug.Log("[FBNetworkWrapper] token - " + perm);
                }

                foreach (string neededPerm in mNeededpermissions)
                {
                    if (false == permList.Contains(neededPerm))
                    {
                        Debug.Log( string.Format("[FBNetworkWrapper] Permission {0} was not set", neededPerm));
                        mExternalLogInCallback(eErrorType.E_ERROR_NOT_ALL_PERMISSIONS);

                        return;
                    }
                }

                Debug.Log("[FBNetworkWrapper] Authorisation is done");

                mExternalLogInCallback(eErrorType.E_ERROR_NONE);
            } 
            else
            {
				Debug.Log("[FBNetworkWrapper] Auth callback " + result);
                mExternalLogInCallback(eErrorType.E_ERROR_INPUT_WAS_CANCELED);
            }

            mExternalLogInCallback = null;*/
        }

        private void shareCallback(IShareResult result)
        {
            /*if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
            {
                mExternalShareCallback(eErrorType.E_ERROR_SOME_INTERNAL_ERROR);
                Debug.Log("ShareLink Error: "+result.Error);
            } 
            else if (!string.IsNullOrEmpty(result.PostId)) 
            {
                // Print post identifier of the shared content
                mExternalShareCallback(eErrorType.E_ERROR_NONE);
                Debug.Log(result.PostId);
            } 
            else 
            {
                mExternalShareCallback(eErrorType.E_ERROR_NONE);
                Debug.Log("ShareLink success!");
            }
                
            mExternalShareCallback = null;*/
        }

        /*===============================================================================================
        * 
        * END OF BLOCK
        * 
        * =============================================================================================*/
    }
}