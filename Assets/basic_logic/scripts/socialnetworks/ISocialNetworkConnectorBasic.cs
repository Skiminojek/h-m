﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

namespace SocialNetwork
{
    public enum eErrorType
    {
        E_ERROR_NONE,
        E_ERROR_NO_CONNECTION,
        E_ERROR_WRONG_INPUT_DATA,
        E_ERROR_NOT_ALL_PERMISSIONS,
        E_ERROR_INPUT_WAS_CANCELED,
        E_ERROR_TIMEOUT,
        E_ERROR_SOME_INTERNAL_ERROR,
        E_ERROR_NOT_INITTED,
        E_ERROR_NOT_AUTORIZED,
		E_ERROR_NOT_IMPLEMENTED,
        E_ERROR_NOT_INSTALLED
    }

    public delegate void InitCallback(eErrorType errorCode);
    public delegate void LogINCallback(eErrorType errorCode);
    public delegate void LogOutCallback(eErrorType errorCode);
    public delegate void ShareCallback(eErrorType errorCode);

    public interface ISocialNetworkConnectorBasic 
    {
        void init(InitCallback callback);
        void logIN(LogINCallback callback);
        void logOut(LogOutCallback callback);
		void sharePicture(ShareCallback callback, string picturePath);
		void shareVideo(ShareCallback callback, string videoPath);
        bool isInit();
        bool isAuthorized();
		bool isInstalled();
    }
}
