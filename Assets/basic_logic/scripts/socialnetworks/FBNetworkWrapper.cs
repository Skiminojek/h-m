﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using Facebook.MiniJSON;
using System.Collections.Generic;

namespace SocialNetwork
{
    public class FBNetworkWrapper : MonoBehaviour, ISocialNetworkConnectorBasic
    {
        private InitCallback mExternalInitCallback = null;
        private LogINCallback mExternalLogInCallback = null;
        private LogOutCallback mExternalLogOutCallback = null;
        private ShareCallback mExternalShareCallback = null;

        private AccessToken mCurrentAccessToken = null;
        private List<string> mNeededpermissions;

        const string mPersistTokenAttribName = "FBAccessToken";

        /*===============================================================================================
        * 
        * ISocialNetworkConnectorBasic implementation
        * 
        * =============================================================================================*/

        public void init (InitCallback callback)
        {
            Debug.Log("[FBNetworkWrapper] Request to init");

            //mNeededpermissions = new List<string> () { "publish_actions", "email" };
            mNeededpermissions = new List<string>();// { DataAttributes.Shared.mEmailAttribute };
            mExternalInitCallback = callback;

            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback);
            } 
            else
            {
                Debug.Log("[FBNetworkWrapper] Already initialized, signal an app activation App Event");
                FB.ActivateApp();
                mExternalInitCallback(eErrorType.E_ERROR_NONE);
                mExternalInitCallback = null;
            }
        }
            
        public void logIN(LogINCallback callback)
        {
            Debug.Log("[FBNetworkWrapper] Request to logIn");
            mExternalLogInCallback = callback;

            if (false == FB.IsLoggedIn)
            {
				FB.LogInWithReadPermissions(mNeededpermissions, AuthCallback);
            }
            else
            {
                Debug.Log("[FBNetworkWrapper] Already LogIn");
                mCurrentAccessToken = Facebook.Unity.AccessToken.CurrentAccessToken;
                mExternalLogInCallback(eErrorType.E_ERROR_NONE);
                mExternalLogInCallback = null;
                
            }
        }

        public void logOut(LogOutCallback callback)
        {
            FB.LogOut();
            callback(eErrorType.E_ERROR_NONE);
        }

		public void sharePicture(ShareCallback callback, string picturePath)
		{
            if(Application.platform == RuntimePlatform.IPhonePlayer)
			    FBShareDialog.OpenFBShareDilaog (picturePath);
            else
                StartCoroutine(shareScreenshotAndroid(picturePath));
		}

		public void shareVideo(ShareCallback callback, string videoPath)
		{
			FBShareDialog.OpenFBShareDilaogVideo (videoPath);
		}

		public bool isInit()
		{
			return FB.IsInitialized;
		}

		public bool isAuthorized()
		{
			return FB.IsLoggedIn;
		}

		private void FBCallback(IGraphResult result)
		{
			Debug.Log ("[FBNetworkWrapper]FB SHARING COMPLETE. Result = " + result);
		}

		private void FBCallback(ILoginResult result)
		{   
			Debug.Log ("[FBNetworkWrapper]FB ILoginResult COMPLETE. Result = " + result);
            

        }

		public bool isInstalled()
		{
            if (Application.platform == RuntimePlatform.IPhonePlayer)
                return FBShareDialog.IsFBInstalled();
            else
                return true;// checkAndroidPackageAppIsPresent("com.facebook.katana");

		}

        /*===============================================================================================
        * 
        * END OF BLOCK
        * 
        * =============================================================================================*/


        /*===============================================================================================
        * 
        * Internal logic
        * 
        * =============================================================================================*/

        private IEnumerator shareScreenshotAndroid(string destination)
        {
            string ShareSubject = "SUBJECT";
            string shareLink = "LINK";
            string textToShare = "TEXT";

            Debug.Log(destination);


            if (!Application.isEditor)
            {

                AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
                AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
                AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);

                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), textToShare + shareLink);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), ShareSubject);
                intentObject.Call<AndroidJavaObject>("setType", "image/png");
                AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
                currentActivity.Call("startActivity", intentObject);
            }
            yield return null;
        }

        private bool checkAndroidPackageAppIsPresent(string package)
        {
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
            AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledPackages", 0);
            int num = appList.Call<int>("size");
            for (int i = 0; i < num; i++)
            {
                AndroidJavaObject appInfo = appList.Call<AndroidJavaObject>("get", i);
                string packageNew = appInfo.Get<string>("packageName");

                if (packageNew.CompareTo(package) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        private void InitCallback ()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                Debug.Log("[FBNetworkWrapper] Init complete");
                FB.ActivateApp();
                mExternalInitCallback(eErrorType.E_ERROR_NONE);
            }
            else
            {
                Debug.Log("[FBNetworkWrapper] Failed to Initialize the Facebook SDK");
                mExternalInitCallback(eErrorType.E_ERROR_SOME_INTERNAL_ERROR);
            }

            mExternalInitCallback = null;
        }

        private void AuthCallback (ILoginResult result)
        {
			if (null != result && null == result.Error) 
            {
				if (false == result.Cancelled) 
				{
					// AccessToken class will have session details
					mCurrentAccessToken = Facebook.Unity.AccessToken.CurrentAccessToken;

					if (null != mCurrentAccessToken) 
					{
						// Print current access token's User ID
						Debug.Log("[FBNetworkWrapper] User Id - " + mCurrentAccessToken.UserId);
					}

					// Print current access token's granted permissions
					Debug.Log("[FBNetworkWrapper] access token's granted permissions");

					string permissions = mCurrentAccessToken.Permissions.ToCommaSeparateList();
					List<string> permList = new List<string>(permissions.Split(','));
					foreach (string perm in permList)
					{
						Debug.Log("[FBNetworkWrapper] token - " + perm);
					}

					/*Debug.Log ("_FIRST");

					foreach (string neededPerm in mNeededpermissions)
					{
						Debug.Log ("_SECOND");
						if (false == permList.Contains(neededPerm))
						{
							Debug.Log ("_THIRD");
							Debug.Log( string.Format("[FBNetworkWrapper] Permission {0} was not set", neededPerm));
							mExternalLogInCallback(eErrorType.E_ERROR_NOT_ALL_PERMISSIONS);

							return;
						}
					}*/

					Debug.Log("[FBNetworkWrapper] Authorisation is done");

					mExternalLogInCallback(eErrorType.E_ERROR_NONE);
				} 
				else 
				{
					Debug.Log("[FBNetworkWrapper] Authorisation was canceled " + result);
					mExternalLogInCallback(eErrorType.E_ERROR_INPUT_WAS_CANCELED);
				}
            } 
            else
            {
				Debug.Log("[FBNetworkWrapper] Some internal error " + result);
				mExternalLogInCallback(eErrorType.E_ERROR_SOME_INTERNAL_ERROR);
            }

            mExternalLogInCallback = null;
        }

        private void shareCallback(IShareResult result)
        {
            if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
            {
                mExternalShareCallback(eErrorType.E_ERROR_SOME_INTERNAL_ERROR);
                Debug.Log("ShareLink Error: "+result.Error);
            } 
            else if (!string.IsNullOrEmpty(result.PostId)) 
            {
                // Print post identifier of the shared content
                mExternalShareCallback(eErrorType.E_ERROR_NONE);
                Debug.Log(result.PostId);
            } 
            else 
            {
                mExternalShareCallback(eErrorType.E_ERROR_NONE);
                Debug.Log("ShareLink success!");
            }
                
            mExternalShareCallback = null;
        }

        /*===============================================================================================
        * 
        * END OF BLOCK
        * 
        * =============================================================================================*/
    }
}