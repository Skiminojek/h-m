﻿using UnityEngine;
using System.Collections;
using SocialNetwork;
using System.Collections.Generic;

public class SharingManager : MonoBehaviour 
{
    [SerializeField]
    FBNetworkWrapper mFBNetworkWrapper;

    [SerializeField]
    InstagramNetworkWrapper mISNetworkWrapper;

	public enum eActionType
	{
		E_ACTION_PHOTO,
		E_ACTION_VIDEO
	}

	eActionType mActionType;

    string mShareContentPath;


    public enum eSharingType
    {
        E_SHARING_TO_FB,
        E_SHARING_TO_IS,
    }

    eSharingType mCurrentSharingType;

    private Dictionary<eSharingType, ISocialNetworkConnectorBasic> mNetworkWrappers = null;

	// Use this for initialization
	void Start () 
    {
        mISNetworkWrapper.init ((eErrorType error) =>{});
        mFBNetworkWrapper.init ((eErrorType error) =>{});

        if (null == mNetworkWrappers) 
        {
            mNetworkWrappers = new Dictionary<eSharingType, ISocialNetworkConnectorBasic> (3);
            mNetworkWrappers.Add (eSharingType.E_SHARING_TO_FB, mFBNetworkWrapper);
            mNetworkWrappers.Add (eSharingType.E_SHARING_TO_IS, mISNetworkWrapper);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

	public void share(eSharingType shareTo, eActionType actionType, string contentPath, ShareCallback callback)
    {
        if (false == mNetworkWrappers[shareTo].isInstalled())
        {
            Debug.Log(string.Format("{0} app not installed.", shareTo.ToString()));
            callback(eErrorType.E_ERROR_NOT_INSTALLED);
            return;
        }

        mCurrentSharingType = shareTo;
        mActionType = actionType;
        mShareContentPath = contentPath;

        NetworkHelper.checkConnection((result) =>
        {
            if (true == result)
                startSharing(callback);
            else
                new MobileNativeMessage(StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TITLE"), StringsManager.getString("NOT_CONNECTION_DIALOG_WINDOW_TEXT")).OnComplete += ()=> { callback(eErrorType.E_ERROR_NO_CONNECTION); };
        });
    }

    public bool isInstalled(SharingManager.eSharingType type)
    {
        if (false == mNetworkWrappers.ContainsKey(type))
        {
            return false;
        }

        return mNetworkWrappers[type].isInstalled();
    }

    private void startSharing(ShareCallback callback)
    {
        if (mActionType == eActionType.E_ACTION_PHOTO)
            mNetworkWrappers[mCurrentSharingType].sharePicture(callback, mShareContentPath);
        else if (mActionType == eActionType.E_ACTION_VIDEO)
            mNetworkWrappers[mCurrentSharingType].shareVideo(callback, mShareContentPath);

        callback(eErrorType.E_ERROR_NONE);
    }
}
